<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('listPeminjaman', 'Api\TransaksiController@getListPeminjaman');
Route::get('listPemesanan', 'Api\TransaksiController@getListPemesanan');
Route::post('laporanPemesanan', 'Api\TransaksiController@laporanPemesanan');
Route::post('laporanPeminjaman', 'Api\TransaksiController@laporanPeminjaman');
Route::post('laporanDenda', 'Api\TransaksiController@laporanDenda');
Route::get('getBahanPustakaYangHarusDiambil/{id}', 'Api\TransaksiController@getBahanPustakaYangHarusDiambil');
Route::get('getBahanPustakaYangDiPesan/{id}', 'Api\TransaksiController@getBahanPustakaYangDiPesan');
Route::get('getBahanPustakaYangDiPinjam/{id}', 'Api\TransaksiController@getBahanPustakaYangDiPinjam');
Route::get('perpanjanganPeminjaman/{id}', 'Api\TransaksiController@perpanjanganPeminjaman');
Route::put('pengembalianBuku/{id}', 'Api\TransaksiController@pengembalianBuku');
Route::get('setujuPemesanan/{id}', 'Api\TransaksiController@setujuPemesanan');
Route::get('tolakPemesanan/{id}', 'Api\TransaksiController@tolakPemesanan');
Route::get('ambilBuku/{id}', 'Api\TransaksiController@ambilBuku');
Route::get('getNotifikasi/{nomorInduk}', 'Api\TransaksiController@getNotifikasi');
Route::get('getTransaksiById/{id}', 'Api\TransaksiController@getTransaksiById');
Route::post('tambahPeminjaman', 'Api\TransaksiController@tambahPeminjaman');
Route::post('tambahPeminjamanAdmin', 'Api\TransaksiController@tambahPeminjamanAdmin');
