<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DataTransaksi;

class TransaksiController extends Controller
{
    public function getListPeminjaman(){
        $data = DataTransaksi::whereIn('status', [1, 2, 3, 5])->get();
        return json_encode($data);
    }

    public function getListPemesanan(){
        $data = DataTransaksi::where("status", 5)->get();
        return json_encode($data);
    }

    public function getTransaksiById($id){
        $data = DataTransaksi::find($id);
        return $data;
    }

    public function getBahanPustakaYangHarusDiambil($id){
        $data = DataTransaksi::where("no_identitas", $id)->where("status", 1)->get();
        return json_encode($data);
    }

    public function getBahanPustakaYangDiPesan($id){
        $data = DataTransaksi::where("no_identitas", $id)->where("status", 5)->get();
        return json_encode($data);
    }

    public function getBahanPustakaYangDiPinjam($id){
        $data = DataTransaksi::where("no_identitas", $id)->whereIn('status', [1, 2, 3])->get();
        return json_encode($data);
    }

    public function getNotifikasi($nomorInduk){
        $date = today()->format('Y-m-d');
        $pengembalianBuku = DataTransaksi::whereIn('status', [2, 3])->where("no_identitas", $nomorInduk)->where("tanggal_pengembalian", "<=", $date)->get();
        $pengambilanBuku = DataTransaksi::where('status', 1)->where("no_identitas", $nomorInduk)->get();
        $notif = array();
        $no = 1;
        foreach ($pengembalianBuku as $notifikasi) {
            array_push($notif, array("no" => $no, "notifikasi" => "Buku " . $notifikasi->judul_buku . " dikembalikan sekarang"));
            $no++;
        }
        foreach ($pengambilanBuku as $notifikasi) {
            
            array_push($notif, array("no" => $no, "notifikasi" => "Peminjaman buku " . $notifikasi->judul_buku . " telah disetujui"));
            $no++;
        }
        return json_encode($notif);

    }

    public function laporanPemesanan(Request $request){
        $from = date("Y-m-d", strtotime($request->input("tahun_awal") . "-" . $request->input("bulan_awal") . "-1"));
        $to = date("Y-m-t", strtotime($request->input("tahun_akhir") . "-" . $request->input("bulan_akhir") . "-1"));
        // $data1 = DataTransaksi::where("status", 5)->whereMonth("tanggal_peminjaman", $request->input("bulan_awal"))->whereYear("tanggal_peminjaman", $request->input("tahun_awal"))->get();
        // $data2 =DataTransaksi::where("status", 5)->whereMonth("tanggal_peminjaman", $request->input("bulan_akhir"))->whereYear("tanggal_peminjaman", $request->input("tahun_akhir"))->get();
        $data = DataTransaksi::whereBetween("tanggal_peminjaman", [$from, $to])->where("status", 5)->get();
        return response()->json([
                'data' => $data,
                'success' => true
            ], 200);
    }

    public function laporanPeminjaman(Request $request){
        $from = date("Y-m-d", strtotime($request->input("tahun_awal") . "-" . $request->input("bulan_awal") . "-1"));
        $to = date("Y-m-t", strtotime($request->input("tahun_akhir") . "-" . $request->input("bulan_akhir") . "-1"));
        // $data1 = DataTransaksi::whereIn("status", [1, 2, 3])->whereMonth("tanggal_peminjaman", $request->input("bulan_awal"))->whereYear("tanggal_peminjaman", $request->input("tahun_awal"))->get();
        // $data2 =DataTransaksi::whereIn("status", [1, 2, 3])->whereMonth("tanggal_peminjaman", $request->input("bulan_akhir"))->whereYear("tanggal_peminjaman", $request->input("tahun_akhir"))->get();
        $data = DataTransaksi::whereBetween("tanggal_peminjaman", [$from, $to])->whereIn("status", [1, 2, 3])->get();
        return response()->json([
                'data' => $data,
                'success' => true
            ], 200);
    }

    public function laporanDenda(Request $request){
        $from = date("Y-m-d", strtotime($request->input("tahun_awal") . "-" . $request->input("bulan_awal") . "-1"));
        $to = date("Y-m-t", strtotime($request->input("tahun_akhir") . "-" . $request->input("bulan_akhir") . "-1"));
        // $data1 = DataTransaksi::where("status", 4)->whereMonth("tanggal_peminjaman", $request->input("bulan_awal"))->whereYear("tanggal_peminjaman", $request->input("tahun_awal"))->get();
        // $data2 =DataTransaksi::where("status", 4)->whereMonth("tanggal_peminjaman", $request->input("bulan_akhir"))->whereYear("tanggal_peminjaman", $request->input("tahun_akhir"))->get();
        $data = DataTransaksi::whereBetween("tanggal_peminjaman", [$from, $to])->where("status", 4)->get();

        return response()->json([
                'from' => $from,
                'data' => $data,
                'success' => true
            ], 200);
    }
    
    public function tambahPeminjaman(Request $request){
        $date = date("Y/m/d");
        $dateNextWeek = date('Y/m/d', strtotime($date . "+7 days"));
        $post = DataTransaksi::create([
            "judul_buku" => $request->input("judul_buku"),
            "no_identitas" => $request->input("no_identitas"),
            "nama" => $request->input("nama"),
            "id_buku" => $request->input("id_create"),
            "tanggal_peminjaman" => $date,
            "tanggal_pengembalian" => $dateNextWeek,
            "status" => 5,
            "email" => $request->input("email"),
            "prodi" => $request->input("prodi"),
            "jabatan" => $request->input("jabatan")
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function tambahPeminjamanAdmin(Request $request){
        $date = date("Y/m/d");
        $dateNextWeek = date('Y/m/d', strtotime($date . "+7 days"));
        $success = true;
        $post = DataTransaksi::create([
            "judul_buku" => $request->input("judulPustaka1"),
            "no_identitas" => $request->input("no_identitas"),
            "nama" => $request->input("nama"),
            "id_buku" => $request->input("idBahanPustaka1"),
            "tanggal_peminjaman" => $date,
            "tanggal_pengembalian" => $dateNextWeek,
            "status" => 2,
            "email" => $request->input("email"),
            "prodi" => $request->input("prodi"),
            "jabatan" => $request->input("jabatan")
        ]);
        $success = $success && $post;

        if($request->input("idBahanPustaka2") != ""){
            $post2 = DataTransaksi::create([
                "judul_buku" => $request->input("judulPustaka2"),
                "no_identitas" => $request->input("no_identitas"),
                "nama" => $request->input("nama"),
                "id_buku" => $request->input("idBahanPustaka2"),
                "tanggal_peminjaman" => $date,
                "tanggal_pengembalian" => $dateNextWeek,
                "status" => 2,
                "email" => $request->input("email"),
                "prodi" => $request->input("prodi"),
                "jabatan" => $request->input("jabatan")
            ]);
            $success = $success && $post2;
        }

        if($request->input("idBahanPustaka3") != ""){
            $post3 = DataTransaksi::create([
                "judul_buku" => $request->input("judulPustaka3"),
                "no_identitas" => $request->input("no_identitas"),
                "nama" => $request->input("nama"),
                "id_buku" => $request->input("idBahanPustaka3"),
                "tanggal_peminjaman" => $date,
                "tanggal_pengembalian" => $dateNextWeek,
                "status" => 2,
                "email" => $request->input("email"),
                "prodi" => $request->input("prodi"),
                "jabatan" => $request->input("jabatan")
            ]);
            $success = $success && $post3;
        }
        
        if ($success) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

     public function setujuPemesanan(int $id){
        $date = date("Y/m/d");
        $dateNextWeek = date('Y/m/d', strtotime($date . "+7 days"));
        $post = DataTransaksi::find($id)->update([
            "status" => 1,
            "tanggal_peminjaman" => $date,
            "tanggal_pengembalian" => $dateNextWeek
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function tolakPemesanan(int $id){
        $post = DataTransaksi::find($id)->update([
            "status" => 3
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function ambilBuku(int $id){
        $post = DataTransaksi::find($id)->update([
            "status" => 2
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }    

    public function perpanjanganPeminjaman(int $id){
        $date = date("Y/m/d");
        $dateNextWeek = date('Y/m/d', strtotime($date . "+7 days"));
        $post = DataTransaksi::find($id)->update([
            "tanggal_pengembalian" => $dateNextWeek,
            "status" => 3
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function pengembalianBuku(Request $request, int $id){

        $post = DataTransaksi::find($id)->update([
            "status" => 4,
            "denda" => $request->input("denda")
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }
}
