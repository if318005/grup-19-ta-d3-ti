<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataTransaksi extends Model
{
    use HasFactory;
    protected $table = 'data_transaksi';
    protected $fillable = [
        "judul_buku",
        "no_identitas",
        "nama",
        "id_buku",
        "tanggal_peminjaman",
        "tanggal_pengembalian",
        "status",
        "email",
        "prodi",
        "jabatan",
        "denda",
    ];
    
}
