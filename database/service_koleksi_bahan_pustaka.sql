-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2021 at 06:13 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_koleksi_bahan_pustaka`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bahan_pustaka`
--

CREATE TABLE `bahan_pustaka` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_create` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `klasifikasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_pustaka` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lokasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subjek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edisi` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isbn` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul_pustaka` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tersedia` int(10) UNSIGNED NOT NULL,
  `gambar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kategori` enum('ta/kp/pa','buku','cd/dvd') COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pembimbing` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tahun` year(4) NOT NULL,
  `pengarang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahasa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penerbit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pembimbing2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penguji` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `penguji2` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `copyOriginal` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prodi` enum('D3 TEKNOLOGI INFORMASI','D3 TEKNLOGI KOMPUTER','D4 TEKNOLOGI REKAYASA PERANGKAT LUNAK','S1 INFORMATIKA','S1 SISTEM INFORMASI','S1 MANAJEMEN REKAYASA','S1 TEKNIK ELEKTRO','S1 BIOPROSES') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tingkat` enum('TINGKAT 1','TINGKAT 2','TINGKAT 3','TINGKAT 4') COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bahan_pustaka`
--

INSERT INTO `bahan_pustaka` (`id`, `id_create`, `klasifikasi`, `status_pustaka`, `lokasi`, `subjek`, `edisi`, `isbn`, `deskripsi`, `judul_pustaka`, `tersedia`, `gambar`, `created_at`, `updated_at`, `kategori`, `jenis`, `pembimbing`, `tahun`, `pengarang`, `bahasa`, `penerbit`, `pembimbing2`, `penguji`, `penguji2`, `sumber`, `copyOriginal`, `prodi`, `tingkat`) VALUES
(2, '21.00003', 'coba', 'tersedia', 'Lantai 1', 'Pemograman', NULL, NULL, 'Buku tentang pemograman Spring', 'Pemograman Spring coba', 1, NULL, '2021-05-11 00:17:48', '2021-08-06 16:30:53', 'cd/dvd', 'CD', NULL, 2017, NULL, NULL, NULL, NULL, NULL, NULL, 'coba', NULL, NULL, NULL),
(22, '21.00002', '005.133 MAH A', 'tersedia', 'Lantai 1', 'spesific programming language', NULL, NULL, 'Buku ini menjelaskan bagaimana sebuah sistem informasi dibuat, dimulai dari mengumpulkan informasi suatu instansi, memahami proses bisnis berjalan, menganalisa masalah yang dihadapi pada proses bisnis berjalan, membuat Fishbone untuk menganalisa permasalahan, membuat usulan dan menganalisa untuk memecahkan tiap masalah yang dihadapi.', 'analisa dan perancangan sistem informasi dengan codeigniter dan laravel', 1, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAEnwAABJ8BA8qoKAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABMFSURBVHic7Z15eBXV3cc/Z25WFtlkDSCoEFQEqWxqVZLBAmMFMcPVFmpdeGyfvvZ9q93R97Uoaq1breKOtX3a2sIFRGBA4EK1CBZxQRTEqiBLFiAJMQtkm/P+MSHe3MzcLTe5Se58nofnIefMnPO7M985+/kdIaXEJXlREm2AS2JxBZDkuAJIclwBJDmuAJIcVwBJjiuAJMcVQJLjCiDJcQWQ5LgCSHJcASQ5rgCSHFcASY4rgCTHFUCS4wogyXEFkOS4AkhyXAEkOa4AkhxXAEmOK4AkJyXRBgSjavo5wJ3ApYCI4JYaYCNwj9/w1bWmbZ2RdicA4DlAjfKeCUBX4CfxN6dzI9rTziBV07sC5UT25QdTC2T7Dd/++FrVuWlvbYBUYnv5p+/9TfxMSQ7aWwnQEygNDLtxrpe01NTGv8srTzb+f+d77/P5500+eBO40G/49rSupZ2H9tgGaELetVfTrWtXAKSUFB470Rg3KnskCxf9NvByBbgPyGtLGzsy7b4E6NmzB6KxVpCYZlN7y776yi6pCX7Dt7NVjOxktPsS4MSJslhuux+YFmdTOiXtrREYL76lavqURBvREeisAgCrFHAJQ7tvA/zqZz8ma9BA6w8pKT5RYXvvnr2fsNS3Mjj4Gr/hWxN/SzsP7b4NMHzYWZx79jCgeS8gkLOHD+PNrdsoLCwKDF6kavpav+FrPypvZ3SaKkBRFPTZs4KDxwLXJ8CcDkOnEQDApInjGTpkSHDwvaqmt/uSLlF0KgEIIfDq1wYHjwBuantrOgadSgAAF40dw4hzzwkO/j9V09MTYU97p816AaqmpwHjgMwQl3UDVgcG3DTvevr1PRMACZSVV4XN67PPv2DzljeCg+8HNkVucUTUAjv8hq82zum2GW0iAFXTzwL+BTSroDsBBcDP/Ybvr4k2JBbaqgp4kc758gEGAi+rmj490YbEQqsLQNX0PGBqa+eTYFKApaqmX5RoQ6KlVasAVdO7AHuBoafDlJQUPBnOzQATgdkRhm3MeuTJyuDQI8Bkv+E7nACLYqK1+8e/JuDlAwyf/R3O/MZk24trTDhQqdCORqedkZLK5c9S+58PA0OzgLWqpl/uN3y289TtjVarAlRNPxv4eWBYtyHDOHPcJMd7jp4SHePlAwhBl1m34BkwNDhmDLCsoww+tWYb4PfA131vIRg20wvCfslfRa2gsi7W5YCJQaSm03XOj1DO6B0c9S3g2QSYFDWtIgBV0zXgmsCwvt+YTNfBZ9leLyUcre5YL/80SrcedPX+FyI9IzjqVlXT70qETdEQdwE0jLg9ERjmSc9gyPRmEzWNlNQIas14W9J2ePoOouvs20Bp9jgXqZo+NxE2RUprlAA/Bc4NDMiaqpHarbvtxbUmFNd0zK8/kJTh59Fl+nftol5SNf3KtrYnUuIqAFXThwBNir3Mvv0ZcMkUx3uOVneghl8Y0sZeRsYlzZYipgErVU0flQCTwhLvluqjQJfAgO5nj6Tkow9sL642oaSD1v1OKP2yUHr0wSwrDgzuBRiqpk/2G76jCTLNlrgNBKmangv445JY5+UdYIrf8IWf0Woj4lIFNPR5n4xHWp2cCcDfVE1vN9Pw8TLkx8D5cUqrszMLeDzRRpymxVWAqukDgH3AGafDUrufQXrPZoMjjdSYgvpO0vCLBLOkCHmqWal/h9/w/T4R9gQSj0bgQwS8fIQg+8YfOg76VNXBoap2UwK2CfWFB6n4y2PI2urA4EdVTT/gN3yvJsouaGEVoGr6pcD3AsP6XhxixA9B0ankevkAngFD6TLrFhBNfruC1R6YmCCzGo2IiYaGzFME7Of3ZGQyZJrziF9ptTXjl4ykjhhD5lXe4OBMYLWq6cMTYBLQshLgB1hr/BoZrDqP+NWZguOdYMSvJaRffCXpE5utjekHrFM1vVcCTIpNAKqm9wEWBYZl9htA/0unON5ztJpOM+LXEjJzryM1e1xwcDbwaiJWLtv2AhpW8P4Sa1dNN5v7ugB9W9c0l1akHtgBfM+pF/Bd4N62s8eljfEAlwB/c6oCprSdLS4JJMtJADG55XDpcDzoVAU02YOdO+WbfEt1ntI+VV1L1clqx3iXxCAl/GPZCg4eOmQX/bDf8C2OSADpaWlMuLhZy7WR6ppaShwcN7gkjj//5RWnl/+03/D9Apy7gU28dBQWHQuZUYrHE4t9Lq3IP5atYMOmzXZRLwO3n/7DSQBNVjMUHQ0tAI9HQTis9nVpe159bQ2r166zi1oKzA/0mOIkgI8C/ygoLKLq5EmHSy1SUtxSoD2wbv1GfCtW2UWtBub5DV99YKCTAA4AjTtbpJR8su+zkBmnepJvkqe94d/yBn/9+1K7qE3AHLtt7LZvraGI2BUYtmfvvpCZuyVAYtn61nZe/rPtDvWtwCy/4bPtpoX6bJus5Ny77z8hDXAFkDh2vPMuzy95GZth/XeAq0OtQQwlgCa+dj/e8wl1dc4HcqSmdoitcJ2O9z/4kMXPvoBpNptn3w1MD7dJNZQAVmMdxwJAeUUlW7fvcE5ICNLTUh3jXeLPx3v28ofFz1JfXx8ctQ+4ym/4SsKl4SgAv+ErBZr0JdauD+1iJyPdFUBbse/Tz3jsicXU1jZr1+0HpvoNX5HNbc0I13Rv0qp4/4PdFBQ6p5uRnhZJni4tZP/+Azzy+B+orm7WrjsCqNE4qGgUgKrpdiM5qwnqDhqvO+/9UBTR6iKoOnmS/IJCTpwos2v0tCtM06SkpJTCwiJOnYrPXMmhw0d46JHfc7L5uMxRrC8/qjOTRO6MvMuwtnRNwBoCPtaQ2DZgA3AbcMPpG7p378aSZx6nT2/7FUx19fUcK46Pc4yqkyfZ+e777HjnXQqLiigtPdFE9R6PQo8ePejTuzcXjr6ASydPZMCA/nHJOxYOHjrE9rd3sGfvPkpKSjlR1lSkmZmZ9OrVk6xBA5k8aQLjxo4lLYp2U0FhIYseeNjukIxSrB1HH9rcFhKROyPvDeCKaG6acPFFPHjvXY7Dv2VfVVHVAsUfLy7mlX/4eO+9D6gN0fOwY/iws5h97TV846KxMecfLW/+6y3WGOvJLyiM6r6MjAwumTQBr34d3bvbLbz6mmPHjnPvAw9RWtrMWXY51pfv3EIPgWf4iPN/hrUwMWLyCwrp3r07540aYRufmuKJSQBSSjb6t/DEU8/w5ZeH7Lo2YTlxooztb++goLCI87KzSW/FKunY8WKeXPwc617fSHlF9LOhdXV1HPjyIG9ufYvevXsxZPBg2+tKSkq5/6FHKC5u1qivAjS/4dsedeYNeIaPOL8Yy41bMxcXodi1+yMuv3QyPXuc0SxOUQRCUaiuidyBZn5BIY8/sZgt/3yTurpm3ZqoOXz4SNgHGytSSjZs2swTTz1DQZRfvR01NTW8s/M9vvjiANnZI+iS+bUXtbKvvuKB3z5K0dFmm4qrgWv9hm9LS/IWUkpUTc8EJgODsBwfXgDMBJz3dwFDhwxm4d0/Z+iQLNv4r8qrqAyzUMQ0TdasXc/KVatDFfeFwCrgUyzPnAVYa+oHNfy7HMjBYafTuIvGcPON8+jt0G6Jhvz8Al546U/857PPnS4xgbeALVit8gKshnS/BlvPwXq2trtnMjIy+O4Nc8idcgUVlZXc/+DDHDp8JPiyOiDPb/hea+nvcdwbqGp6Kpazo0cAR+cGaWmp3DTveuZcNxOluYsUSssqOFVtXxIcPpLPcy/+kf37Dzgl/2+s1cn/8hu+kPWBqum9sbyCL8RmJXNmZiY3ePPInXJFTFPX9fUma9etZ+WrjkKtBX6HtdgiP1x6DTuC7sN6xs2YNHE8R48dt3s2JjDXb/j+Ho39ToTdHNqwVv1urBfh2GQdlX0uv7zzdoYOaVrcSikpLatsUh2Ypsnada+zYuVrTg/zJPC/wOPhXryNvUOxzh+2dd06Knsk82++MarewsFDh3n+xT9y4MuDTpfsBG7xG77d0djaYO/3gccIU9o2ILHm81+KNh8nIt4drGr6GGAJMN7pmpSUFM4fNZLRF4xi9PmjOP+8bLp3sw59LK+oYu+nn7Nt+w62bttu15o9zZtYPzL07FN4e+dhuarrExyXmpLCuHFjmTj+YsaMGd2kzj1NRWUlu3btZsfOd/lg12674VawhHoP8FjwPHuUtvbH8q8wJ8ylDwB3x/MInKi2h6ua7gHuwNozEMrtu5W4EAwdkkVdXT0FhUXhWvUVwK+witC4/EBV0/tiPdiQx8ZkZGTQq2dPunXrSnl5OaUnyuxG2YKJi1ADUTX9WuBprHaYE6ewhnv3N9iwxG/4jseaZ0z+AVRNPxd4gfjtH9gA3OY3fF/GKb0mqJo+E3gGqxHWUsqBXwDPtcZhVA0npz0C3BrhLR/7Dd/oWPOL2UFEw9DxbVgNn+Z9wcg4APzGb/j+FOP9EaNqeg+sBzu/BckYwA/awhm0qukq1kcWyc7hkbGWRPHwEHImVhF7A3AZ4Y9/r8DqJi0BVrSk7owFVdNHA3OB7+DQFQviGLAMeMVv+La2pm3BNHTPZ2LZOp1A17tfs9dv+GJ2zyNUbfZIUyoLBEwAUYiQ+QIKJHJ3usxcZRh/iXhgX9X0wVjOkgdjec7OwuqSSawFCpuA9/yGL+LxXfUabxb15mwQ50jkQCEYhKSfgHKJyJfCLEAqRxByw+a1vn9HYavA2t5+doCt/bFWRB9p+HcQ60iYiOxduHChsvXtj66oFzJHwEAhxCAp5UAEXaSkUAgKkCIfwSeiun7lpk0risOn2mhvD6z5msBnWwi84Dd8BZGmE4zInaGvAjnTIf4UAkOaLNm8zmfEmkm0LFy4UHlzx0ffl1LeKKx5ikhXnH4BvCJSa5/ctGpVRPPh8eCqb+tnm6b4MeAFGWk7o1YIscE0eWnzumUrWtO+UIjcGXn7gWFhL4Tl9UK5fcvapS0f+wxB7nT9AqGwBHD2Kx+eEom4Y7Ox7M/xsssOr9frKamo/4lE3EcEvSInpBTrhWL+0L92eas0gkMhVC3vTil5hPB1N0ApUt7pX7f85XgbMn78D1J79D2+ACEWYLlXbTECXhcpym0bX1vqOIITKznfvm60YiovYRXL8aAC5IIrJo1ZfM8997SZIx0hpWwowpiGEIMkcqAiuUBaX6CTKDYg5G3xUuzUGXkTpBAvAU7dmWrgDeALAQVIUWQizxAKA4HBSK7EeUazQgh+5TeWPy3jsILE6/WmFVfKBUi5AIeRUSnYLyTbgXwhKJBQJSUDBQxEMArJZThWa3K7wDN/k7F0T0ttjQTHXkCOljfYA9dLxF1Yvm6DqZSSBVdOvvCpWBV7qdebmVkh7wP5EyynBcHsFYj700hfHaox6vV6PcUVdVcKqfy3FDh5qXrLrGf+ltd9n8RiK4QVqgk8JxVlyeY1S98Nlc60adf1q1U8uiLk3dJ+0KcGKe8rO3bmQzt3PteqZxKG7QbmXO0doEj5JEjd/orYFJurea8UmC8S5Fq+gTohxcNponKhYRhRLSzIvVrPE5KngAE20dXAfWVH+/wumgebk3NzhsisWCiQP8VOqIJPEOJW/5pl26KxNWf27J5KtfIYiJttLxDskkK5NZygWkLkcwEz8mYLIRY7KhbxNKZ4xb9+qePKlNNfKlK5CcE87KuYDzHlzf71y9+L7Cc05/Jvz+2VZp561PHBwodCyj9QK18N1RWbOmtWf1mXOhvJHcBIm0vqETycLqt+E61Qm+Rz9XVXSak8j31jvB54XiL/umXdim3xqMYCiWogKGf27J5KjedxpPNhzBIOCfhICvIVKfOlEKmN9Z/V73ZyLlWNlIviWexdNV2fWu/heSEdR9PqBOyUgkNI8oXVezhTIAdJ67Szi3Gqq+P8dU6bdmPX+pSqB6Tkdqc8BRSYUuxShMyXUKBIsWnjumX/bEm+Mc4FzJkB8nmsQYmWI9mmeMz5G9es2BuX9AKYOXNml8r6tEVI/of4OMeOu1ADmTrDe4kU5otE7Hxb/tFvLL8l1vxiHgrWtHlnVMuTjyJES8bWK4BfXzHpwqdbu+ujTvdORDGX4NzTiAC5XVHkra0h1EC8Xm/a8QrzLmGduxhu2XDNyW5Kz21Ll4bev+9Ai+cCpk6fc7lUzJtBzCKyRQ0AnyLEK6Y0X9xiLG+zUzYbxhrmSkXMFZIc7HsewdSAWC8w/3b5pDHL2rKPnjvDmy2EOR9rnsW2tBXw9ibDd0msecTtxJCcnJyUlPQ+U6SHMRIGY5KFIEtCLcgjoBwRmIel4tnWmq3aSJk6a1Z/WZMyAzgLIbKAwQjZBymKgCNIeUQqfCHTTGPLypWOq1faAiGEUKfp3zSFHC+EyAIzS8JgBB8ryMc2rV3xacxpt/fdNS6ti+vWI8lxBZDkuAJIclwBJDmuAJIcVwBJjiuAJMcVQJLjCiDJcQWQ5LgCSHJcASQ5rgCSHFcASY4rgCTHFUCS4wogyXEFkOS4AkhyXAEkOa4AkhxXAEmOK4Ak5/8BosS8C/itLnEAAAAASUVORK5CYII=', '2021-08-09 23:51:38', '2021-08-12 18:33:54', 'buku', 'Buku Bacaan', NULL, 2018, 'Meilan Anastashya', 'Indonesia', 'LOKOMEDIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, '21.00001', '005.133 Abd m Kp.1', 'tersedia', 'Lantai 1', 'Computer Programming Language', '-', '717051371', 'Aplikasi POS banyak dibutuhkan oleh pelaku usaha. Laravel merupakan framework yang paling popular saat ini. Mempelajari keduanya sekaligus, yaitu membuat aplikasi POS dengan Laravel, tentu akan menjadi pembelajaran yang sangat menarik. Apalagi disertai dengan penggunaan AJAX yang dapat membuat aplikasi berbasis web terasa seperti aplikasi desktop. Itulah yang akan dibahas di dalam buku ini. Pembahasan dimulai dari dasar-dasar penggunaan Laravel dilengkapi dengan trik Laravel untuk aplikasi POS, seperti ekspor dan impor file Excel, membuat file PDF, cetak barcode, direct printing ke printer Dot Matrix, pembuatan helper angka terbilang, dan sebagainya. Dibahas juga langkah demi langkah pembuatan operasi CRUD, dengan dan tanpa AJAX. Di bagian akhir dibahas langkah demi langkah membuat aplikasi POS dengan Laravel dan AJAX. Aplikasi POS yang dibuat ini memiliki fitur menarik seperti cetak barcode, cetak kartu member, pilihan cetak nota menggunakan PDF atau direct printing ke printer Dot Matrix, tampilan tabel dengan DataTable, serta desain menggunakan template AdminLTE. Pembahasan selengkapnya mencakup: ? Memahami berbagai fitur Laravel ? Operasi CRUD pada Laravel, dengan dan tanpa AJAX ? Trik-trik Laravel untuk aplikasi POS ? Proses membuat aplikasi POS ? Menerapkan template AdminLTE pada Laravel ? Trik direct printing ke printer Dot Matrix pada aplikasi PHP Keterampilan: Pemula, Menengah Kelompok: Pemrograman Jenis buku: Referensi, Tutorial', 'Membuat Aplikasi Point of Sale dengan Laravel dan AJAX', 1, NULL, '2021-08-10 01:14:37', '2021-08-12 18:37:30', 'buku', 'Buku Bacaan', NULL, 2017, 'Abdulloh, Rohi', 'Indonesia', 'Elex Media Komputindo', NULL, NULL, NULL, NULL, 'Original', NULL, NULL),
(26, '21.00006', '005.133 Abd m Kp.1', 'tersedia', 'Lantai 1', 'Pengembahan tanaman', '2', '9786026231185', 'Buku berisi tentang bagaimana cara mengembangkan tanaman hias', 'PENGEMBANGAN TANAMAN HIAS', 1, NULL, '2021-08-11 11:05:31', '2021-08-11 11:05:31', 'buku', 'Buku Bacaan', NULL, 2018, 'Sutono', 'Indonesia', 'erlangga', NULL, NULL, NULL, NULL, 'Copy', NULL, NULL),
(27, '21.00008', 'F 809..3 Toe a', 'tersedia', 'Lantai 1', 'Fiction', 'Pada buku Anak Semua Bangsa dikisahkan pengenalan si tokoh dengan lingkungan sendiri dan dunia, sejauh pikirannya dapat menjangkaunya.', '9798659139', 'Pada buku Anak Semua Bangsa dikisahkan pengenalan si tokoh dengan lingkungan sendiri dan dunia, sejauh pikirannya dapat menjangkaunya.', 'Anak semua bangsa', 1, 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAEnwAABJ8BA8qoKAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABMFSURBVHic7Z15eBXV3cc/Z25WFtlkDSCoEFQEqWxqVZLBAmMFMcPVFmpdeGyfvvZ9q93R97Uoaq1breKOtX3a2sIFRGBA4EK1CBZxQRTEqiBLFiAJMQtkm/P+MSHe3MzcLTe5Se58nofnIefMnPO7M985+/kdIaXEJXlREm2AS2JxBZDkuAJIclwBJDmuAJIcVwBJjiuAJMcVQJLjCiDJcQWQ5LgCSHJcASQ5rgCSHFcASY4rgCTHFUCS4wogyXEFkOS4AkhyXAEkOa4AkhxXAEmOK4AkJyXRBgSjavo5wJ3ApYCI4JYaYCNwj9/w1bWmbZ2RdicA4DlAjfKeCUBX4CfxN6dzI9rTziBV07sC5UT25QdTC2T7Dd/++FrVuWlvbYBUYnv5p+/9TfxMSQ7aWwnQEygNDLtxrpe01NTGv8srTzb+f+d77/P5500+eBO40G/49rSupZ2H9tgGaELetVfTrWtXAKSUFB470Rg3KnskCxf9NvByBbgPyGtLGzsy7b4E6NmzB6KxVpCYZlN7y776yi6pCX7Dt7NVjOxktPsS4MSJslhuux+YFmdTOiXtrREYL76lavqURBvREeisAgCrFHAJQ7tvA/zqZz8ma9BA6w8pKT5RYXvvnr2fsNS3Mjj4Gr/hWxN/SzsP7b4NMHzYWZx79jCgeS8gkLOHD+PNrdsoLCwKDF6kavpav+FrPypvZ3SaKkBRFPTZs4KDxwLXJ8CcDkOnEQDApInjGTpkSHDwvaqmt/uSLlF0KgEIIfDq1wYHjwBuantrOgadSgAAF40dw4hzzwkO/j9V09MTYU97p816AaqmpwHjgMwQl3UDVgcG3DTvevr1PRMACZSVV4XN67PPv2DzljeCg+8HNkVucUTUAjv8hq82zum2GW0iAFXTzwL+BTSroDsBBcDP/Ybvr4k2JBbaqgp4kc758gEGAi+rmj490YbEQqsLQNX0PGBqa+eTYFKApaqmX5RoQ6KlVasAVdO7AHuBoafDlJQUPBnOzQATgdkRhm3MeuTJyuDQI8Bkv+E7nACLYqK1+8e/JuDlAwyf/R3O/MZk24trTDhQqdCORqedkZLK5c9S+58PA0OzgLWqpl/uN3y289TtjVarAlRNPxv4eWBYtyHDOHPcJMd7jp4SHePlAwhBl1m34BkwNDhmDLCsoww+tWYb4PfA131vIRg20wvCfslfRa2gsi7W5YCJQaSm03XOj1DO6B0c9S3g2QSYFDWtIgBV0zXgmsCwvt+YTNfBZ9leLyUcre5YL/80SrcedPX+FyI9IzjqVlXT70qETdEQdwE0jLg9ERjmSc9gyPRmEzWNlNQIas14W9J2ePoOouvs20Bp9jgXqZo+NxE2RUprlAA/Bc4NDMiaqpHarbvtxbUmFNd0zK8/kJTh59Fl+nftol5SNf3KtrYnUuIqAFXThwBNir3Mvv0ZcMkUx3uOVneghl8Y0sZeRsYlzZYipgErVU0flQCTwhLvluqjQJfAgO5nj6Tkow9sL642oaSD1v1OKP2yUHr0wSwrDgzuBRiqpk/2G76jCTLNlrgNBKmangv445JY5+UdYIrf8IWf0Woj4lIFNPR5n4xHWp2cCcDfVE1vN9Pw8TLkx8D5cUqrszMLeDzRRpymxVWAqukDgH3AGafDUrufQXrPZoMjjdSYgvpO0vCLBLOkCHmqWal/h9/w/T4R9gQSj0bgQwS8fIQg+8YfOg76VNXBoap2UwK2CfWFB6n4y2PI2urA4EdVTT/gN3yvJsouaGEVoGr6pcD3AsP6XhxixA9B0ankevkAngFD6TLrFhBNfruC1R6YmCCzGo2IiYaGzFME7Of3ZGQyZJrziF9ptTXjl4ykjhhD5lXe4OBMYLWq6cMTYBLQshLgB1hr/BoZrDqP+NWZguOdYMSvJaRffCXpE5utjekHrFM1vVcCTIpNAKqm9wEWBYZl9htA/0unON5ztJpOM+LXEjJzryM1e1xwcDbwaiJWLtv2AhpW8P4Sa1dNN5v7ugB9W9c0l1akHtgBfM+pF/Bd4N62s8eljfEAlwB/c6oCprSdLS4JJMtJADG55XDpcDzoVAU02YOdO+WbfEt1ntI+VV1L1clqx3iXxCAl/GPZCg4eOmQX/bDf8C2OSADpaWlMuLhZy7WR6ppaShwcN7gkjj//5RWnl/+03/D9Apy7gU28dBQWHQuZUYrHE4t9Lq3IP5atYMOmzXZRLwO3n/7DSQBNVjMUHQ0tAI9HQTis9nVpe159bQ2r166zi1oKzA/0mOIkgI8C/ygoLKLq5EmHSy1SUtxSoD2wbv1GfCtW2UWtBub5DV99YKCTAA4AjTtbpJR8su+zkBmnepJvkqe94d/yBn/9+1K7qE3AHLtt7LZvraGI2BUYtmfvvpCZuyVAYtn61nZe/rPtDvWtwCy/4bPtpoX6bJus5Ny77z8hDXAFkDh2vPMuzy95GZth/XeAq0OtQQwlgCa+dj/e8wl1dc4HcqSmdoitcJ2O9z/4kMXPvoBpNptn3w1MD7dJNZQAVmMdxwJAeUUlW7fvcE5ICNLTUh3jXeLPx3v28ofFz1JfXx8ctQ+4ym/4SsKl4SgAv+ErBZr0JdauD+1iJyPdFUBbse/Tz3jsicXU1jZr1+0HpvoNX5HNbc0I13Rv0qp4/4PdFBQ6p5uRnhZJni4tZP/+Azzy+B+orm7WrjsCqNE4qGgUgKrpdiM5qwnqDhqvO+/9UBTR6iKoOnmS/IJCTpwos2v0tCtM06SkpJTCwiJOnYrPXMmhw0d46JHfc7L5uMxRrC8/qjOTRO6MvMuwtnRNwBoCPtaQ2DZgA3AbcMPpG7p378aSZx6nT2/7FUx19fUcK46Pc4yqkyfZ+e777HjnXQqLiigtPdFE9R6PQo8ePejTuzcXjr6ASydPZMCA/nHJOxYOHjrE9rd3sGfvPkpKSjlR1lSkmZmZ9OrVk6xBA5k8aQLjxo4lLYp2U0FhIYseeNjukIxSrB1HH9rcFhKROyPvDeCKaG6acPFFPHjvXY7Dv2VfVVHVAsUfLy7mlX/4eO+9D6gN0fOwY/iws5h97TV846KxMecfLW/+6y3WGOvJLyiM6r6MjAwumTQBr34d3bvbLbz6mmPHjnPvAw9RWtrMWXY51pfv3EIPgWf4iPN/hrUwMWLyCwrp3r07540aYRufmuKJSQBSSjb6t/DEU8/w5ZeH7Lo2YTlxooztb++goLCI87KzSW/FKunY8WKeXPwc617fSHlF9LOhdXV1HPjyIG9ufYvevXsxZPBg2+tKSkq5/6FHKC5u1qivAjS/4dsedeYNeIaPOL8Yy41bMxcXodi1+yMuv3QyPXuc0SxOUQRCUaiuidyBZn5BIY8/sZgt/3yTurpm3ZqoOXz4SNgHGytSSjZs2swTTz1DQZRfvR01NTW8s/M9vvjiANnZI+iS+bUXtbKvvuKB3z5K0dFmm4qrgWv9hm9LS/IWUkpUTc8EJgODsBwfXgDMBJz3dwFDhwxm4d0/Z+iQLNv4r8qrqAyzUMQ0TdasXc/KVatDFfeFwCrgUyzPnAVYa+oHNfy7HMjBYafTuIvGcPON8+jt0G6Jhvz8Al546U/857PPnS4xgbeALVit8gKshnS/BlvPwXq2trtnMjIy+O4Nc8idcgUVlZXc/+DDHDp8JPiyOiDPb/hea+nvcdwbqGp6Kpazo0cAR+cGaWmp3DTveuZcNxOluYsUSssqOFVtXxIcPpLPcy/+kf37Dzgl/2+s1cn/8hu+kPWBqum9sbyCL8RmJXNmZiY3ePPInXJFTFPX9fUma9etZ+WrjkKtBX6HtdgiP1x6DTuC7sN6xs2YNHE8R48dt3s2JjDXb/j+Ho39ToTdHNqwVv1urBfh2GQdlX0uv7zzdoYOaVrcSikpLatsUh2Ypsnada+zYuVrTg/zJPC/wOPhXryNvUOxzh+2dd06Knsk82++MarewsFDh3n+xT9y4MuDTpfsBG7xG77d0djaYO/3gccIU9o2ILHm81+KNh8nIt4drGr6GGAJMN7pmpSUFM4fNZLRF4xi9PmjOP+8bLp3sw59LK+oYu+nn7Nt+w62bttu15o9zZtYPzL07FN4e+dhuarrExyXmpLCuHFjmTj+YsaMGd2kzj1NRWUlu3btZsfOd/lg12674VawhHoP8FjwPHuUtvbH8q8wJ8ylDwB3x/MInKi2h6ua7gHuwNozEMrtu5W4EAwdkkVdXT0FhUXhWvUVwK+witC4/EBV0/tiPdiQx8ZkZGTQq2dPunXrSnl5OaUnyuxG2YKJi1ADUTX9WuBprHaYE6ewhnv3N9iwxG/4jseaZ0z+AVRNPxd4gfjtH9gA3OY3fF/GKb0mqJo+E3gGqxHWUsqBXwDPtcZhVA0npz0C3BrhLR/7Dd/oWPOL2UFEw9DxbVgNn+Z9wcg4APzGb/j+FOP9EaNqeg+sBzu/BckYwA/awhm0qukq1kcWyc7hkbGWRPHwEHImVhF7A3AZ4Y9/r8DqJi0BVrSk7owFVdNHA3OB7+DQFQviGLAMeMVv+La2pm3BNHTPZ2LZOp1A17tfs9dv+GJ2zyNUbfZIUyoLBEwAUYiQ+QIKJHJ3usxcZRh/iXhgX9X0wVjOkgdjec7OwuqSSawFCpuA9/yGL+LxXfUabxb15mwQ50jkQCEYhKSfgHKJyJfCLEAqRxByw+a1vn9HYavA2t5+doCt/bFWRB9p+HcQ60iYiOxduHChsvXtj66oFzJHwEAhxCAp5UAEXaSkUAgKkCIfwSeiun7lpk0risOn2mhvD6z5msBnWwi84Dd8BZGmE4zInaGvAjnTIf4UAkOaLNm8zmfEmkm0LFy4UHlzx0ffl1LeKKx5ikhXnH4BvCJSa5/ctGpVRPPh8eCqb+tnm6b4MeAFGWk7o1YIscE0eWnzumUrWtO+UIjcGXn7gWFhL4Tl9UK5fcvapS0f+wxB7nT9AqGwBHD2Kx+eEom4Y7Ox7M/xsssOr9frKamo/4lE3EcEvSInpBTrhWL+0L92eas0gkMhVC3vTil5hPB1N0ApUt7pX7f85XgbMn78D1J79D2+ACEWYLlXbTECXhcpym0bX1vqOIITKznfvm60YiovYRXL8aAC5IIrJo1ZfM8997SZIx0hpWwowpiGEIMkcqAiuUBaX6CTKDYg5G3xUuzUGXkTpBAvAU7dmWrgDeALAQVIUWQizxAKA4HBSK7EeUazQgh+5TeWPy3jsILE6/WmFVfKBUi5AIeRUSnYLyTbgXwhKJBQJSUDBQxEMArJZThWa3K7wDN/k7F0T0ttjQTHXkCOljfYA9dLxF1Yvm6DqZSSBVdOvvCpWBV7qdebmVkh7wP5EyynBcHsFYj700hfHaox6vV6PcUVdVcKqfy3FDh5qXrLrGf+ltd9n8RiK4QVqgk8JxVlyeY1S98Nlc60adf1q1U8uiLk3dJ+0KcGKe8rO3bmQzt3PteqZxKG7QbmXO0doEj5JEjd/orYFJurea8UmC8S5Fq+gTohxcNponKhYRhRLSzIvVrPE5KngAE20dXAfWVH+/wumgebk3NzhsisWCiQP8VOqIJPEOJW/5pl26KxNWf27J5KtfIYiJttLxDskkK5NZygWkLkcwEz8mYLIRY7KhbxNKZ4xb9+qePKlNNfKlK5CcE87KuYDzHlzf71y9+L7Cc05/Jvz+2VZp561PHBwodCyj9QK18N1RWbOmtWf1mXOhvJHcBIm0vqETycLqt+E61Qm+Rz9XVXSak8j31jvB54XiL/umXdim3xqMYCiWogKGf27J5KjedxpPNhzBIOCfhICvIVKfOlEKmN9Z/V73ZyLlWNlIviWexdNV2fWu/heSEdR9PqBOyUgkNI8oXVezhTIAdJ67Szi3Gqq+P8dU6bdmPX+pSqB6Tkdqc8BRSYUuxShMyXUKBIsWnjumX/bEm+Mc4FzJkB8nmsQYmWI9mmeMz5G9es2BuX9AKYOXNml8r6tEVI/of4OMeOu1ADmTrDe4kU5otE7Hxb/tFvLL8l1vxiHgrWtHlnVMuTjyJES8bWK4BfXzHpwqdbu+ujTvdORDGX4NzTiAC5XVHkra0h1EC8Xm/a8QrzLmGduxhu2XDNyW5Kz21Ll4bev+9Ai+cCpk6fc7lUzJtBzCKyRQ0AnyLEK6Y0X9xiLG+zUzYbxhrmSkXMFZIc7HsewdSAWC8w/3b5pDHL2rKPnjvDmy2EOR9rnsW2tBXw9ibDd0msecTtxJCcnJyUlPQ+U6SHMRIGY5KFIEtCLcgjoBwRmIel4tnWmq3aSJk6a1Z/WZMyAzgLIbKAwQjZBymKgCNIeUQqfCHTTGPLypWOq1faAiGEUKfp3zSFHC+EyAIzS8JgBB8ryMc2rV3xacxpt/fdNS6ti+vWI8lxBZDkuAJIclwBJDmuAJIcVwBJjiuAJMcVQJLjCiDJcQWQ5LgCSHJcASQ5rgCSHFcASY4rgCTHFUCS4wogyXEFkOS4AkhyXAEkOa4AkhxXAEmOK4Ak5/8BosS8C/itLnEAAAAASUVORK5CYII=', '2021-08-12 20:47:01', '2021-08-12 20:47:01', 'buku', 'Buku Bacaan', NULL, 2018, 'Pramoedya Ananta Toer', 'Indonesia', 'Hasta Mitra', NULL, NULL, NULL, NULL, 'Copy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `id_pustaka`
--

CREATE TABLE `id_pustaka` (
  `id` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `id_pustaka`
--

INSERT INTO `id_pustaka` (`id`, `tahun`, `jumlah`) VALUES
(1, 21, 9);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_26_061608_create_bahan_pustaka_table', 1),
(7, '2021_06_27_003049_alter_table_bahan_pustaka', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bahan_pustaka`
--
ALTER TABLE `bahan_pustaka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `id_pustaka`
--
ALTER TABLE `id_pustaka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bahan_pustaka`
--
ALTER TABLE `bahan_pustaka`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `id_pustaka`
--
ALTER TABLE `id_pustaka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
