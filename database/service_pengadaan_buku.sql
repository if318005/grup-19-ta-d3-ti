-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2021 at 06:14 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `service_pengadaan_buku`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_pengajuan`
--

CREATE TABLE `data_pengajuan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pengarang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `judul_buku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_pengajuan`
--

INSERT INTO `data_pengajuan` (`id`, `pengarang`, `status`, `judul_buku`, `id_user`, `created_at`, `updated_at`) VALUES
(1, 'erlangga', 2, 'Pemograman Spring', 1, '2021-04-06 21:35:30', '2021-07-21 20:26:35'),
(2, 'erlangga', 3, 'Pemograman Spring', 1, '2021-04-07 23:57:46', '2021-08-11 02:49:05'),
(3, 'erlangga', 2, 'Pemograman Laravel 8', 9, '2021-04-29 17:16:14', '2021-08-11 02:49:18'),
(4, 'erlangga', 3, 'Pemograman Spring', 1, '2021-05-17 00:56:33', '2021-05-17 00:56:33'),
(30, 'coba', 3, 'coba', 1, '2021-07-19 23:12:56', '2021-08-11 02:51:32'),
(31, 'asdf', 3, 'asdf', 1, '2021-07-20 02:30:28', '2021-08-11 02:52:02'),
(32, 'asdf', 3, 'asdf', 1, '2021-07-20 02:33:04', '2021-08-11 02:52:37'),
(33, 'asdf', 1, 'asdf', 1, '2021-07-20 02:42:46', '2021-08-11 02:52:49'),
(34, 'asdf', 0, 'asdf', 1, '2021-07-20 09:47:14', '2021-07-20 09:47:14'),
(35, 'douwes', 0, 'Penjajahan', 1, '2021-07-21 20:28:29', '2021-07-21 20:28:29'),
(36, 'douwes', 0, 'Penjajahan', 1, '2021-07-23 01:41:08', '2021-07-23 01:41:08'),
(37, 'coba1', 0, 'coba1', 1, '2021-07-26 20:50:15', '2021-07-26 20:50:15'),
(38, 'coba2', 0, 'coba2', 1, '2021-07-26 20:50:59', '2021-07-26 20:50:59'),
(39, 'coba2', 0, 'coba2', 1, '2021-07-26 20:51:59', '2021-07-26 20:51:59'),
(40, 'coba3', 0, 'coba3', 1, '2021-07-26 20:52:06', '2021-07-26 20:52:06'),
(41, 'buku12', 0, 'buku12', 1, '2021-07-26 21:17:52', '2021-07-26 21:17:52'),
(42, 'buku12', 0, 'buku12', 1, '2021-07-26 21:18:46', '2021-07-26 21:18:46'),
(43, 'buku33', 0, 'buku33', 1, '2021-07-26 21:21:53', '2021-07-26 21:21:53'),
(44, 'douwes', 0, 'Penjajahan', 1, '2021-08-02 07:16:03', '2021-08-02 07:16:03'),
(45, 'douwes', 0, 'Penjajahan', 1, '2021-08-02 07:17:35', '2021-08-02 07:17:35'),
(46, 'douwes', 0, 'Penjajahan', 1, '2021-08-02 07:17:48', '2021-08-02 07:17:48');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_04_06_133205_create_bahan_pustaka_table', 1),
(5, '2021_04_06_134457_create_data_pengajuan_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_pengajuan`
--
ALTER TABLE `data_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_pengajuan`
--
ALTER TABLE `data_pengajuan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
