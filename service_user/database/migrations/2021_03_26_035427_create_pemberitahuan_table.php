<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePemberitahuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemberitahuan', function (Blueprint $table) {
            $table->id();
            $table->text("Isi");
            $table->bigInteger("id_user")->unsigned();
            $table->foreign("id_user")->references("id")->on("users");
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger("id_role")->unsigned();
            $table->foreign("id_role")->references("id")->on("role");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemberitahuan');
    }
}
