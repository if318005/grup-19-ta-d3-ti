<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::get('getUser/{username}', 'Api\UserController@getUser');
Route::get('getUserByNomorInduk/{nomorInduk}', 'Api\UserController@getUserByNomorInduk');
Route::get('getAllUser', 'Api\UserController@getAllUser');
Route::get('hapusUser/{id}', 'Api\UserController@hapusUser');
Route::put('gantiPassword/{username}', 'Api\UserController@gantiPassword');
Route::put('editUser/{id}', 'Api\UserController@editUser');

// Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user/detail', 'Api\UserController@details');
    Route::post('logout', 'Api\UserController@logout');
// }); 
