<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class UserController extends Controller
{

    public $successStatus = 200;

    // public function login(Request $request){
        // if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        //     $user = Auth::user();
        //     $success['token'] =  $user->createToken('myApp')->accessToken;
        //     return response()->json(['success' => $success], $this->successStatus);
        // }
        // else{
        //     return response()->json(['error'=>'Unauthorised'], 401);
        // }
        // $data = json_decode(file_get_contents($request), true);
        // $data = $request->input("name");
        // $data1 = json_decode($data, true);
        // return json_encode($data);
    // }

    public function login(Request $request){

        if(Auth::attempt(['username' => request('username'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('myApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function getAllUser(){
        $data = User::all();
        return response()->json($data);
    }

    public function getUser($username){
        $user = User::where("username", $username)->get()->first();
        if($user != null){
            $password = User::where("username", $username)->get()->first()->password;
            $user["pass"] = $password;
            return response()->json($user);
        }else{
            return response()->json(null);
        }
        return $user;
    }

    public function getUserByNomorInduk($nomorInduk){
        $user = User::where("nomorInduk", $nomorInduk)->get()->first();
        if($user != null){
            return response()->json($user);
        }else{
            return response()->json(null);
        }
        return $user;
    }

    // public function register(Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'username' => 'required',
    //         'password' => 'required',

    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json(['error'=>$validator->errors()], 401);            
    //     }

    //     $input = $request->all();
    //     $input['password'] = bcrypt($input['password']);
    //     $user = User::create($input);
    //     $success['token'] =  $user->createToken('myApp')->accessToken;
    //     $success['name'] =  $user->name;

    //     return response()->json(['success'=>$success], $this->successStatus);
    // }

    public function register(Request $request){
        $post = User::create([
            "name" => $request->input("name"),
            "nomorInduk" => $request->input("nomorInduk"),
            "email" => $request->input("email"),
            "username" => $request->input("username"),
            "id_role" => $request->input("id_role"),
            "password" => bcrypt($request->input("password")),
            "Prodi" => $request->input("prodi"),
            "alamat" => $request->input("alamat"),
            "noHP" => $request->input("noHP")
        ]);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function hapusUser($id){
        $post = User::find($id)->delete();
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil dihapus!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal dihapus!',
            ], 400);
        }
    }

    public function logout(Request $request)
    {
        $logout = $request->user()->token();
        $logout->revoke();
        if($logout){
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }
    }

    public function gantiPassword(Request $request, $username){
        $user = User::where("username", $username)->first();
        $o_pass = $user->password;

        $l_pass = $request->input("password");
        $n_pass = $request->input("newPassword");

        if(\Hash::check($l_pass, $o_pass)){
            if(!\Hash::check($n_pass, $o_pass)){
                $post = $user->update(["password" => bcrypt($n_pass)]);
                if($post){
                    return response()->json([
                        'success' => true,
                        'message' => 'Berhasil!',
                    ], 200);
                }else{
                    return response()->json([
                        'success' => false,
                        'message' => 'Kata sandi gagal di ubah!'
                    ], 200);
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'kata sandi lama tidak bisa sama dengan kata sandi baru!',
                ], 200);
            }
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Kata sandi lama tidak tepat!',
            ], 200);
        }
    }

    public function editUser(Request $request, $id){
        $user = User::find($id);
        $post = $user->update([
            "name" => $request->input("nama"),
            "email" => $request->input("email"),
            "Prodi" => $request->input("jurusan"),
            "alamat" => $request->input("alamat"),
            "noHP" => $request->input("noHP"),
            "id_role" => $request->input("jabatan")
        ]);
        if($post){
            return response()->json([
                'success' => true,
                'message' => 'Berhasil!',
            ], 200);
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Kata sandi gagal di ubah!'
            ], 200);
        }
    }

    public function details()
    {
        $user = Auth::user();
        if($user){
            return response()->json(['success' => $user], $this->successStatus);
        }else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
} 
