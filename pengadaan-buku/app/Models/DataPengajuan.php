<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPengajuan extends Model
{
    use HasFactory;
    protected $table = 'data_pengajuan';
    protected $fillable = [
        "pengarang",
        "status",
        "judul_buku",
        "id_user",
    ];
}
