<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DataPengajuan;

class PengajuanController extends Controller
{
    public function getListPengajuan(){
        $data = DataPengajuan::all();
        return json_encode($data);
    }

    public function getPengajuan(int $id){
        $data = DataPengajuan::find($id);
        return json_encode($data);
    }

    public function tambahPengajuan(Request $request){
        $pengarang = $request->input("pengarang");
        // $tahun_terbit = $request->input("tahun_terbit");
        $judul_buku = $request->input("judul_buku");
        // $id_user = $request->input("id_user");
        $id_user = 1;

        $post = DataPengajuan::create([
            "pengarang" => $pengarang,
            "status" => 0,
            // "tahun_terbit" => $tahun_terbit,
            "judul_buku" => $judul_buku,
            "id_user" => $id_user
        ]);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function setujuPengajuan(int $id){
        $data = DataPengajuan::find($id);
        $data->update([
            'status' => 1
        ]);
        if ($data) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function tolakPengajuan(int $id){
        $data = DataPengajuan::find($id);
        $data->update([
            'status' => 3
        ]);
        if ($data) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function pengadaanBuku(int $id){
        $data = DataPengajuan::find($id);
        $data->update([
            'status' => 2
        ]);
        if ($data) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
                'data' => $data
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }
}
