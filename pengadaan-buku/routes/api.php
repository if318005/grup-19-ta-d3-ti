<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('listPengajuan', 'Api\PengajuanController@getListPengajuan');
Route::get('getPengajuan/{id}', 'Api\PengajuanController@getPengajuan');
Route::post('tambahPengajuan', 'Api\PengajuanController@tambahPengajuan');
Route::put('setujuPengajuan/{id}', 'Api\PengajuanController@setujuPengajuan');
Route::put('tolakPengajuan/{id}', 'Api\PengajuanController@tolakPengajuan');
Route::put('pengadaanBuku/{id}', 'Api\PengajuanController@pengadaanBuku');
Route::get('coba', function(Request $request){
    $name = $request->input('name');
    $umur = $request->input('umur');
    return json_encode($umur*10);
});
