import Home from './ADMIN/Home';
import BerandaUser from './USERS/BerandaUser';
import SignUp from './ADMIN/Components/pages/SignUp';
import Buku from './ADMIN/BahanPustaka/Buku';
import CD from './ADMIN/BahanPustaka/CD';
import TA from './ADMIN/BahanPustaka/TA';
import BahanPustakaBaru from './ADMIN/KelolaData/BahanPustakaBaru';
import DataPengguna from './ADMIN/KelolaData/DataPengguna';
import Laporan from './ADMIN/KelolaData/Laporan';
import PanduanPengguna from './ADMIN/TentangPerpus/PanduanPengguna';
import PanduanPesanPinjam from './ADMIN/TentangPerpus/PanduanPesanPinjam';
import PeraturanPerpustakaan from './ADMIN/TentangPerpus/PeraturanPerpustakaan';
import Pencarian from './ADMIN/Components/pages/Pencarian';
import Peminjaman from './ADMIN/Pemberitahuan/Peminjaman';
import Pemesanan from './ADMIN/Pemberitahuan/Pemesanan';
import SejarahPeminjaman from './ADMIN/Pemberitahuan/SejarahPeminjaman';
import Anggota from './ADMIN/Pemberitahuan/Anggota';
import RatingBuku from './ADMIN/Pemberitahuan/RatingBuku';
import BukuUser from './USERS/BahanPustaka/BukuUser';
import CDUser from './USERS/BahanPustaka/CDUser';
import TAUser from './USERS/BahanPustaka/TAUser';
import BahanPustakaBaruUser from './USERS/Pemberitahuan/BahanPustakaBaru';
import Artikel from './USERS/Pemberitahuan/Artikel';
import RatingBukuUser from './USERS/Pemberitahuan/RatingBuku';
import PanduanPenggunaUser from './USERS/TentangPerpus/PanduanPengguna';
import PanduanPesanPinjamUser from './USERS/TentangPerpus/PanduanPesanPinjam';
import PeraturanPerpustakaanUser from './USERS/TentangPerpus/PeraturanPerpustakaan';
import GantiPassword from './USERS/InformasiAkun/GantiPassword';
import InformasiBahanPustaka from './USERS/InformasiAkun/InformasiBahanPustaka';
import NotifikasiAdmin from './USERS/InformasiAkun/NotifikasiAdmin';
import PencarianUser from './USERS/Components/pages/Pencarian';
import DetailBuku from './ADMIN/BahanPustaka/Detail_Buku';
import DetailCD from './USERS/BahanPustaka/DetailCD';
import LoginForm from './forms/LoginForm';
import SejarahPerpus from './ADMIN/TentangPerpus/SejarahPerpus';
import SejarahPerpusUser from './USERS/TentangPerpus/SejarahPerpus';
import Galeri from './ADMIN/TentangPerpus/Galeri';
import GaleriUser from './USERS/TentangPerpus/Galeri';
import Request from './USERS/BahanPustaka/Request';
import DetailTA from './ADMIN/BahanPustaka/DetailTA';
import PengadaanBuku from './ADMIN/Pemberitahuan/PengadaanBuku';

const sisperRoutes = [
  {
    path: "/beranda",
    component: Home,
  },
  {
    path: "/login",
    component: LoginForm,
  },
  {
    path: "/user",
    component: BerandaUser,
  },
  {
    path: "/pencarian",
    component: Pencarian,
  },
  {
    path: "/buku",
    component: Buku,
  },
  {
    path: "/cd",
    component: CD,
  },
  {
    path: "/ta",
    component: TA,
  },
  {
    path: "/detail-buku/:id",
    component: DetailBuku,
  },
  {
    path: "/bahan-pustaka-baru",
    component: BahanPustakaBaru,
  },
  {
    path: "/data-pengguna",
    component: DataPengguna,
  },
  {
    path: "/laporan",
    component: Laporan,
  },
  {
    path: "/panduan-pengguna",
    component: PanduanPengguna,
  },
  {
    path: "/panduan-pesan-pinjam",
    component: PanduanPesanPinjam,
  },
  {
    path: "/peraturan-perpustakaan",
    component: PeraturanPerpustakaan,
  },
  {
    path: "/sejarah-Perpustakaan",
    component: SejarahPerpus,
  },
  {
    path: "/sejarah-perpus-User",
    component: SejarahPerpusUser,
  },
  {
    path: "/galeri",
    component: Galeri,
  },
  {
    path: "/galeri-user",
    component: GaleriUser,
  },
  {
    path: "/peminjaman",
    component: Peminjaman,
  },
  {
    path: "/pemesanan",
    component: Pemesanan,
  },
  {
    path: "/pengadaan",
    component: PengadaanBuku,
  },
  {
    path: "/sejarah-peminjaman",
    component: SejarahPeminjaman,
  },
  {
    path: "/anggota",
    component: Anggota,
  },
  {
    path: "/rating-buku",
    component: RatingBuku,
  },
  {
    path: "/sign-up",
    component: SignUp,
  },
  {
    path: "/pencarian",
    component: PencarianUser,
  },
  {
    path: "/buku-user",
    component: BukuUser,
  },
  {
    path: "/cd-user",
    component: CDUser,
  },
  {
    path: "/ta-user",
    component: TAUser,
  },
  {
    path: "/bahan-pustaka-baru-user",
    component: BahanPustakaBaruUser,
  },
  {
    path: "/artikel",
    component: Artikel,
  },
  {
    path: "/rating-buku-user",
    component: RatingBukuUser,
  },
  {
    path: "/panduan-pengguna-user",
    component: PanduanPenggunaUser,
  },
  {
    path: "/panduan-pesan-pinjam-user",
    component: PanduanPesanPinjamUser,
  },
  {
    path: "/peraturan-perpustakaan-user",
    component: PeraturanPerpustakaanUser,
  },
  {
    path: "/ganti-password",
    component: GantiPassword,
  },
  {
    path: "/informasi-bahan-pustaka",
    component: InformasiBahanPustaka,
  },
  {
    path: "/notifikasi-admin",
    component: NotifikasiAdmin,
  },
  {
    path: "/request",
    component: Request,
  },
  {
    path: "/detail-CD",
    component: DetailCD,
  },
  {
    path: "/detail-TA",
    component: DetailTA,
  }
]

export default sisperRoutes;