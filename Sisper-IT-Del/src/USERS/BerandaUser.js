import React from 'react';
import '../App.css';
import Header from './Components/Header/Header';
import NavbarUser from './Components/Navbar/NavbarUser';
import Pencarian from './Components/pages/Pencarian';
import Footer from './Components/Footer/Footer';
import mainBanner from './Components/Images/Main Banner.png';
//import InformasiBahanPustaka from './InformasiAkun/InformasiBahanPustaka';

export default function BerandaUser() {
  return (
    <>
    <Header />
    <div className='beranda' style={{ backgroundImage: `url(${mainBanner})` }}>
      <NavbarUser />
      <Pencarian />
      {/* <InformasiBahanPustaka /> */}
    </div>
    <Footer />
    </>
  );
}
