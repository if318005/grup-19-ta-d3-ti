import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import TableFooter from '@material-ui/core/TableFooter';
import NavbarUser from '../Components/Navbar/NavbarUser';
import LastPageIcon from '@material-ui/icons/LastPage';
import GambarBuku from '../Components/Images/Buku.png';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Table from '@material-ui/core/Table';
import {Button, Dialog} from '@material-ui/core';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Axios from 'axios';
import Swal from 'sweetalert2';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  button: {
    backgroundColor: '#061D47',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));



function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const BukuUser=() => {

  const [openDetail, setOpenDetail] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.search(value) !== -1;
  });


  setFilteredData(result);
  }
  useEffect(()=> {
    const getBuku = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
        setAllData(response.data);
        setFilteredData(response.data);
        })
        .catch(error => {
            console.log('Error getting fake data: ' + error);
        })
    };
    getBuku();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])
  
  //Detail Buku
  const [detailBahasa, setDetailBahasa] = useState('')
  const [detailSubjek, setDetailSubjek] = useState('')
  const [detailJudulBuku, setDetailJudulBuku] = useState('')
  const [detailPengarang, setDetailPengarang] = useState('')
  const [detailPenerbit, setDetailPenerbit] = useState('')
  const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
  const [detailTahun, setDetailTahun] = useState('')
  const [detailJenis, setDetailJenis] = useState('')
  const [detailLokasi, setDetailLokasi] = useState('')
  const [detailStok, setDetailStok] = useState('')
  const [detailDeskripsi, setDetailDeskripsi] = useState('')
  const [detailGambar, setDetailGambar] = useState('')
  const [detailId, setDetailId] = useState('')
  const [detailIdCreate, setDetailIdCreate] = useState('')


  const handleClickOpenDetail = (val) => {
    setDetailBahasa(val.bahasa)
    setDetailSubjek(val.subjek)
    setDetailJudulBuku(val.judul_pustaka)
    setDetailPengarang(val.pengarang)
    setDetailPenerbit(val.penerbit)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailTahun(val.tahun)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailStok(val.tersedia)
    setDetailDeskripsi(val.deskripsi)
    setDetailGambar(val.gambar)
    setDetailIdCreate(val.id_create)
    setDetailId(val.id)
    setOpenDetail(true);
  }

  const handleClose = () => {
    setOpenDetail(false);
  };

  const pinjamBuku = () => {
    console.log(detailId)
    Axios.post("http://localhost:8082/api/v1/transaksiBuku/tambahPeminjaman", {
        username: localStorage.getItem("username"),
        id_buku: detailId,
        judul_buku: detailJudulBuku,
        id_create: detailIdCreate
    },
    {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Berhasil!",
          text: "Menunggu konfirmasi peminjaman",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Gagal meminjam buku",
          type: "failed"
        });
      }
    });
  };


  return (
    <>
    <Header />
    <NavbarUser />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
    <br /><h1>Daftar Buku</h1>
    <br /><div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Pengarang, Penerbit" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Cover Buku</StyledTableCell>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell >Pengarang</StyledTableCell>
            <StyledTableCell >Status</StyledTableCell>
            <StyledTableCell >Klasifikasi</StyledTableCell>
            <StyledTableCell >Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
            <StyledTableCell>
              {val.gambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={val.gambar} alt="Buku"/>}
            </StyledTableCell>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell>{val.pengarang}</StyledTableCell>
              <StyledTableCell >{val.status_pustaka}</StyledTableCell>
              <StyledTableCell >{val.klasifikasi}</StyledTableCell>
              <StyledTableCell >
              <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faInfoCircle" style={{color: 'blue'}} /></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    <br />
    </div>
    <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail Buku</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Cover Buku</label>
            </div>
            <div class="form-row">
              <label class="col-md-4">
              {detailGambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={detailGambar} alt="."/>}
              </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-4">: {detailIdCreate}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul Buku</label>
              <label class="col-md-4">: {detailJudulBuku}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Bahasa</label>
              <label class="col-md-4">: {detailBahasa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-4">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pengarang</label>
              <label class="col-md-4">: {detailPengarang}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penerbit</label>
              <label class="col-md-4">: {detailPenerbit}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Deskripsi</label>
              <label class="col-md-4">: {detailDeskripsi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-4">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Edisi</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-4">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Stok</label>
              <label class="col-md-4">: {detailStok}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-4">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Copy/Original</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-4">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              {detailStok > 0 ?
                <Link to="/buku-user">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={pinjamBuku}
                  type="submit"
                >
                  Pinjam
                </Button>
              </Link> :
                <button class="col-md-2" disabled>Pinjam</button>
            }
              <button class="col-md-2" onClick={handleClose} className={classes.button}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    <Footer />
    </>
  );
}

export default BukuUser;
