// import React, {useState, useEffect, Fragment} from 'react';
// import Navbar from '../Components/Navbar/Navbar';
// import Pencarian from '../Components/pages/Pencarian';
// import { Grid, InputLabel, Card} from '@material-ui/core';
// import CardActionArea from '@material-ui/core/CardActionArea';
// import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
// import Typography from '@material-ui/core/Typography';
// import Axios from 'axios';
// import '../KelolaData/style.css';

// function DetailTA() {
//     const [bukuList, setBukuList] = useState([])

//     useEffect(()=> {
//         Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka').then((response)=> {
//             setBukuList(response.data);
//             console.log(bukuList);
//         });
//       }, [bukuList]);


//     return (
//         <>
//         <Navbar />
//         <Pencarian />
//       <div class="container_beranda">
//         <Fragment>
//             <Grid container spacing={2} justify="flex-end" className="mb-4">
//                 <Grid item lg={12} md={12} sm={6} xs={12}>
//                     <InputLabel class="kas">Buku Kas</InputLabel>
//                 </Grid>
//             </Grid>

//             {bukuList.map((val)=> {
//                 return (
//                     <Grid container spacing={3} className="mb-3">
//                         <Grid item xs={12}>
//                             <Card>
//                                 <CardActionArea>
//                                     <CardMedia title="Contemplative Reptile"/>
//                                         <CardContent className="left">
//                                             <image src={val.gambar}></image>
//                                         </CardContent>
//                                         <CardContent className="right">
//                                                 <Typography variant="h5" color="textSecondary" component="h1">
//                                                     {val.judul_pustaka}
//                                                 </Typography>
//                                                 <Typography variant="body3" color="textSecondary"  component='p'>
//                                                     {(val.deskripsi)}
//                                                 </Typography>

                                            
//                                     </CardContent>
//                                 </CardActionArea>
//                                 </Card>
//                         </Grid>
//                     </Grid>
//                 )
//             })}

//         </Fragment> 
//       </div>
//       </>
//     )
// }
// export default DetailTA;

import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import NavbarUser from '../Components/Navbar/NavbarUser';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import GambarBuku from '../Components/Images/Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";
import { Dialog} from '@material-ui/core';
import {Link} from 'react-router-dom';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  button: {
    backgroundColor: '#061D47',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

const DetailTA=() => {

  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.search(value) !== -1;
  });


  setFilteredData(result);
  }
  useEffect(()=> {
    const getBuku = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
        setAllData(response.data);
        setFilteredData(response.data);
        })
        .catch(error => {
            console.log('Error getting fake data: ' + error);
        })
    };
    getBuku();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  return (
    <>
    <Header />
    <NavbarUser />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
    <br /><h1>Daftar Buku</h1>
    <br /><div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Pengarang, Penerbit" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Cover Buku</StyledTableCell>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell >Pengarang</StyledTableCell>
            <StyledTableCell >Status</StyledTableCell>
            <StyledTableCell >Klasifikasi</StyledTableCell>
            <StyledTableCell align="right">Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {filteredData.map((val) => (
            <StyledTableRow key={val.id}>
            <StyledTableCell>
              {val.gambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={val.gambar} alt="Buku"/>}
            </StyledTableCell>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell>{val.pengarang}</StyledTableCell>
              <StyledTableCell >{val.status_pustaka}</StyledTableCell>
              <StyledTableCell >{val.klasifikasi}</StyledTableCell>
              <StyledTableCell align="right">
              <Link to="#" ><FontAwesomeIcon icon={faEye} size="2x" className="faPen" style={{color: 'blue'}} /></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    <br />
    </div>
    
    <Dialog>
      <div className="Detail-buku">
            <div class="form-row">
              <label class="col-md-2">Judul Buku</label>
              <label class="col-md-2"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2">Pengarang</label>
              <label class="col-md-2"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2">Penerbit</label>
              <label class="col-md-2"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2">Klasifikasi</label>
              <label class="col-md-2"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2">Tahun</label>
              <label class="col-md-2"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2">Bahasa</label>
              <label class="col-md-2"></label>
            </div>
          </div>
    </Dialog>
    <Footer />
    </>
  );
}

export default DetailTA;
