// import React, {useState, useEffect, Fragment} from 'react';
// import Navbar from '../Components/Navbar/Navbar';
// import Pencarian from '../Components/pages/Pencarian';
// import { Grid, InputLabel, Card} from '@material-ui/core';
// import CardActionArea from '@material-ui/core/CardActionArea';
// import CardContent from '@material-ui/core/CardContent';
// import CardMedia from '@material-ui/core/CardMedia';
// import Typography from '@material-ui/core/Typography';
// import Axios from 'axios';
// import '../KelolaData/style.css';

// function DetailBuku() {
//     const [bukuList, setBukuList] = useState([])

//     useEffect(()=> {
//         Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka').then((response)=> {
//             setBukuList(response.data);
//             console.log(bukuList);
//         });
//       }, [bukuList]);


//     return (
//         <>
//         <Navbar />
//         <Pencarian />
//       <div class="container_beranda">
//         <Fragment>
//             <Grid container spacing={2} justify="flex-end" className="mb-4">
//                 <Grid item lg={12} md={12} sm={6} xs={12}>
//                     <InputLabel class="kas">Buku Kas</InputLabel>
//                 </Grid>
//             </Grid>

//             {bukuList.map((val)=> {
//                 return (
//                     <Grid container spacing={3} className="mb-3">
//                         <Grid item xs={12}>
//                             <Card>
//                                 <CardActionArea>
//                                     <CardMedia title="Contemplative Reptile"/>
//                                         <CardContent className="left">
//                                             <image src={val.gambar}></image>
//                                         </CardContent>
//                                         <CardContent className="right">
//                                                 <Typography variant="h5" color="textSecondary" component="h1">
//                                                     {val.judul_pustaka}
//                                                 </Typography>
//                                                 <Typography variant="body3" color="textSecondary"  component='p'>
//                                                     {(val.deskripsi)}
//                                                 </Typography>

                                            
//                                     </CardContent>
//                                 </CardActionArea>
//                                 </Card>
//                         </Grid>
//                     </Grid>
//                 )
//             })}

//         </Fragment> 
//       </div>
//       </>
//     )
// }
// export default DetailBuku;

import React, {useState, useEffect, Fragment} from 'react';
import Navbar from '../Components/Navbar/NavbarUser';
import { Grid, InputLabel, Card} from '@material-ui/core';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Header from '../Components/Header/Header';
import Axios from 'axios';

function DetailBuku() {
    const [buku, setBuku] = useState([])

    useEffect(()=> {
        Axios.get(`http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka/$id`).then((response)=> {
            setBuku(response.data);
            console.log(buku);
        });
      }, [buku]);


    return (
        <>
        <Header />
        <Navbar />
      <div class="container_beranda">
        <Fragment>
            <Grid container spacing={2} justify="flex-end" className="mb-4">
                <Grid item lg={12} md={12} sm={6} xs={12}>
                    <InputLabel class="kas">Buku Kas</InputLabel>
                </Grid>
            </Grid>
                    <Grid container spacing={3} className="mb-3">
                        <Grid item xs={12}>
                            <Card>
                                <CardActionArea>
                                    <CardMedia title="Contemplative Reptile"/>
                                        <CardContent className="left">
                                            <image src={buku.gambar}></image>
                                        </CardContent>
                                        <CardContent className="right">
                                                <Typography variant="h5" color="textSecondary" component="h1">
                                                    {buku.judul_pustaka}
                                                </Typography>
                                                <Typography variant="body3" color="textSecondary"  component='p'>
                                                    {(buku.deskripsi)}
                                                </Typography>

                                            
                                    </CardContent>
                                </CardActionArea>
                                </Card>
                        </Grid>
                    </Grid>

        </Fragment> 
      </div>
      </>
    )
}
export default DetailBuku;