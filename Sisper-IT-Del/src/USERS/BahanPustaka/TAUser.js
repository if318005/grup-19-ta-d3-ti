import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import TablePagination from '@material-ui/core/TablePagination';
import TableFooter from '@material-ui/core/TableFooter';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import NavbarUser from '../Components/Navbar/NavbarUser';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { Dialog} from '@material-ui/core';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Axios from 'axios';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  button: {
    backgroundColor: '#061D47',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const TAUser=() => {

  const [openDetail, setOpenDetail] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.search(value) !== -1;
  });

  setFilteredData(result);
  }
  useEffect(()=> {
    const getTA = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getTaKpPa', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setTAList(response.data);
        console.log(taList);
        setAllData(response.data);
        setFilteredData(response.data);
        })
        .catch(error => {
            console.log('Error getting fake data: ' + error);
        })
    };
    getTA();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  //Detail TA
  const [detailJenis, setDetailJenis] = useState('')
  const [detailSubjek, setDetailSubjek] = useState('')
  const [detailJudulTaKpPa, setDetailJudulTaKpPa] = useState('')
  const [detailAbstrak, setDetailAbstrak] = useState('')
  const [detailPenulis, setDetailPenulis] = useState('')
  const [detailLokasi, setDetailLokasi] = useState('')
  const [detailTahun, setDetailTahun] = useState('')
  const [detailPenguji, setDetailPenguji] = useState('')
  const [detailPembimbing, setDetailPembimbing] = useState('')
  const [detailPembimbing2, setDetailPembimbing2] = useState('')
  const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
  const [detailTingkat, setDetailTingkat] = useState('')
  const [detailStatus, setDetailStatus] = useState('')
  const [detailId, setDetailId] = useState('')

  const handleClickOpenDetail = (val) => {
    setDetailId(val.id_create)
    setDetailSubjek(val.subjek)
    setDetailJudulTaKpPa(val.judul_pustaka)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailAbstrak(val.deskripsi)
    setDetailPenulis(val.pengarang)
    setDetailTahun(val.tahun)
    setDetailPenguji(val.penguji)
    setDetailPembimbing(val.pembimbing)
    setDetailPembimbing2(val.pembimbing2)
    setDetailTingkat(val.tingkat)
    setDetailStatus(val.status)
    setOpenDetail(true);
  }

  const handleClose = () => {
    setOpenDetail(false);
  };

  const classes = useStyles();
  const [taList, setTAList] = useState([])

  return (
    <>
    <Header />
    <NavbarUser />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h1>Daftar TA/KP/PA</h1>
      <br /><div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Penguji" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell>Jenis</StyledTableCell>
            <StyledTableCell>Status</StyledTableCell>
            <StyledTableCell>Pembimbing</StyledTableCell>
            <StyledTableCell>Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell>{val.jenis}</StyledTableCell>
              <StyledTableCell>{val.status_pustaka}</StyledTableCell>
              <StyledTableCell >{val.pembimbing}</StyledTableCell>
              <StyledTableCell >
              <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue'}} /></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={5}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    <br />
    </div>
      <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail TA/KP/PA</h2></center><br/>
          <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-9">: {detailId}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul</label>
              <label class="col-md-9">: {detailJudulTaKpPa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-9">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Abstrak</label>
              <label class="col-md-9">: {detailAbstrak}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penulis</label>
              <label class="col-md-9">: {detailPenulis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-9">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-9">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penguji</label>
              <label class="col-md-9">: {detailPenguji}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 1</label>
              <label class="col-md-9">: {detailPembimbing}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 2</label>
              <label class="col-md-9">: {detailPembimbing2}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-9">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tingkat</label>
              <label class="col-md-9">: {detailTingkat}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Status</label>
              <label class="col-md-9">: {detailStatus}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <button class="col-md-2" onClick={handleClose} className={classes.button}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    <Footer />
    </>
  );
}

export default TAUser;
