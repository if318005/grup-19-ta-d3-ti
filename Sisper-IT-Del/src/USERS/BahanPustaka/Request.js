import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Header from '../Components/Header/Header';
import Navbar from '../Components/Navbar/NavbarUser';
import Axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../Components/Footer/Footer';
import Swal from 'sweetalert2';


const useStyles = makeStyles({
  table: {
    marginLeft: '100px',
    marginRight: 'auto',
  },
  button: {
    backgroundColor: '#061D47',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

function Request() {
  const classes = useStyles();

  const [judul_buku, setJudul] = useState('')
  const [pengarang, setPengarang] = useState('')
  const [penerbit, setPenerbit] = useState('')
  const [edisi, setEdisi] = useState('')
  const [harga, setHarga] = useState('')
  const submitPengadaan = (e) => {
    e.preventDefault()
    Axios.post('http://localhost:8082/api/v1/pengadaanBuku/addPengajuan', {
      judul_buku: judul_buku, 
      pengarang: pengarang, 
      penerbit: penerbit, 
      edisi: edisi, 
      harga: harga,
    },{
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) =>{
      Swal.fire(
        {
            title: "Berhasil",
            text: "Buku berhasil direquest",
            timer: 2000,
            icon: "success",
        })
    }
      );

    };

  return (
    <>
    <Header />
    <Navbar />
        <div>
            <br />
            <center><h3>Request Buku Baru</h3></center>
            <form className={classes.table}>
            <div class="form-row">
                <label class="col-md-2">Judul Buku</label>
                <input 
                  type="text" 
                  name="judul_buku" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setJudul(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">Pengarang</label>
                <input 
                  type="text" 
                  name="pengarang" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPengarang(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">Penerbit</label>
                <input 
                  type="text" 
                  name="penerbit" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPenerbit(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">Edisi</label>
                <input 
                  type="text" 
                  name="edisi" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setEdisi(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">Harga</label>
                <input 
                  type="number" 
                  name="harga" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setHarga(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <text class="col-md-2"/>
                <button class="col-md-1" className={classes.button} onClick={submitPengadaan}>Request</button>
            </div>
            </form>
        </div>
      <Footer />
    </>
  );
}

export default Request;
