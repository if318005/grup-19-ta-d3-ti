import React, {useState, useEffect} from 'react';
import './Pencarian.css';
import mainBanner from '../Images/Background-Buku.png';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { faInfoCircle} from "@fortawesome/free-solid-svg-icons";
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import TableFooter from '@material-ui/core/TableFooter';
import LastPageIcon from '@material-ui/icons/LastPage';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {Link} from 'react-router-dom';
import GambarBuku from '../Images/Buku.png';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import Axios from 'axios';
import { Button, Dialog} from '@material-ui/core';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  formTambah: {
    display: 'flex',
    flexDirection: 'column',
    alignItem: 'center',
    justifyContent: 'center',
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1000,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  form: {
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'120px',
    height: '35px',
    borderRadius: '50px',
  },
  gambar: {
    width: '75%',
    height: '75%'
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

function Pencarian() {
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };
  const [openDetailCD, setOpenDetailCD] = React.useState(false);
  const [openDetailBuku, setOpenDetailBuku] = React.useState(false);
  const [openDetailTA, setOpenDetailTA] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [filteredData2,setFilteredData2] = useState(allData);
  const [filteredData3,setFilteredData3] = useState(allData);
    
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.search(value) !== -1;
  });

  setFilteredData(result);
  }

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getBahanPustaka = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          })
          .catch(error => {
              console.log('Error getting data: ' + error);
              })
    };
    const getTA = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getTaKpPa', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData2(response.data);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    const getCD = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getCdDvd', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
        setAllData(response.data);
        setFilteredData3(response.data);
        })
        .catch(error => {
            console.log('Error getting fake data: ' + error);
        })
    };
    getCD();
    getTA();
    getBahanPustaka();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const pinjamCD = () => {
    Axios.post("http://localhost:8082/api/v1/transaksiBuku/tambahPeminjaman", {
        username: localStorage.getItem("username"),
        id_buku: detailId,
        judul_buku: detailJudulCdDvd
    },
    {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Berhasil!",
          text: "Menunggu konfirmasi peminjaman",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Gagal meminjam buku",
          type: "failed"
        });
      }
    });
  };

  //Detail CD/DVD
  const [detailJenis, setDetailJenis] = useState('')
  const [detailSubjek, setDetailSubjek] = useState('')
  const [detailJudulCdDvd, setDetailJudulCdDvd] = useState('')
  const [detailDeskripsi, setDetailDeskripsi] = useState('')
  const [detailSumber, setDetailSumber] = useState('')
  const [detailProdiPemilik, setDetailProdiPemilik] = useState('')
  const [detailLokasi, setDetailLokasi] = useState('')
  const [detailISBN, setDetailISBN] = useState('')
  const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
  const [detailTanggalMasuk, setDetailTanggalMasuk] = useState('')
  //const [detailIdMasterCd, setDetailIdMasterCd] = useState('')
  const [detailId, setDetailId] = useState('')
  const [detailIdCreate, setDetailIdCreate] = useState('')
  const [detailStok, setDetailStok] = useState('')

  //Detail Buku
  const [detailBahasa, setDetailBahasa] = useState('')
  const [detailJudulBuku, setDetailJudulBuku] = useState('')
  const [detailPengarang, setDetailPengarang] = useState('')
  const [detailPenerbit, setDetailPenerbit] = useState('')
  const [detailTahun, setDetailTahun] = useState('')
  const [detailGambar, setDetailGambar] = useState('')
  
  //Detail TA/KP/PA
  const [detailJudulTaKpPa, setDetailJudulTaKpPa] = useState('')
  const [detailPenulis, setDetailPenulis] = useState('')
  const [detailAbstrak, setDetailAbstrak] = useState('')
  const [detailPenguji, setDetailPenguji] = useState('')
  const [detailPenguji2, setDetailPenguji2] = useState('')
  const [detailPembimbing, setDetailPembimbing] = useState('')
  const [detailPembimbing2, setDetailPembimbing2] = useState('')
  const [detailTingkat, setDetailTingkat] = useState('')
  const [detailStatus, setDetailStatus] = useState('')

  const handleClickOpenDetailCD = (val) => {
    setDetailIdCreate(val.id_create)
    setDetailSubjek(val.subjek)
    setDetailJudulCdDvd(val.judul_pustaka)
    setDetailSumber(val.penerbit)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailDeskripsi(val.deskripsi)
    setDetailId(val.id)
    setDetailStok(val.tersedia)
    setDetailProdiPemilik(val.prodi)
    setDetailISBN(val.isbn)
    setDetailTanggalMasuk(val.created_at)
    setOpenDetailCD(true);
  }

  const handleClickOpenDetailBuku = (val) => {
    setDetailId(val.id_create)
    setDetailBahasa(val.bahasa)
    setDetailSubjek(val.subjek)
    setDetailJudulBuku(val.judul_pustaka)
    setDetailPengarang(val.pengarang)
    setDetailPenerbit(val.penerbit)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailTahun(val.tahun)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailStok(val.tersedia)
    setDetailDeskripsi(val.deskripsi)
    setDetailGambar(val.gambar)
    setOpenDetailBuku(true);
  }

  const handleClickOpenDetailTA = (val) => {
    setDetailId(val.id_create)
    setDetailSubjek(val.subjek)
    setDetailJudulTaKpPa(val.judul_pustaka)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailAbstrak(val.deskripsi)
    setDetailPenulis(val.pengarang)
    setDetailTahun(val.tahun)
    setDetailPenguji(val.penguji)
    setDetailPenguji2(val.penguji2)
    setDetailPembimbing(val.pembimbing)
    setDetailPembimbing2(val.pembimbing2)
    setDetailTingkat(val.tingkat)
    setDetailStatus(val.status)
    setOpenDetailTA(true);
  }

  const handleClose = () => {
    setOpenDetailCD(false);
    setOpenDetailTA(false);
    setOpenDetailBuku(false);
  };

  
  const pinjamBuku = () => {
    console.log(detailId)
    Axios.post("http://localhost:8082/api/v1/transaksiBuku/tambahPeminjaman", {
        username: localStorage.getItem("username"),
        id_buku: detailId,
        judul_buku: detailJudulBuku,
        id_create: detailIdCreate
    },
    {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Berhasil!",
          text: "Menunggu konfirmasi peminjaman",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Gagal meminjam buku",
          type: "failed"
        });
      }
    });
  }

  return (
    <>
    <div className="container_pencarian" style={{ backgroundImage: `url(${mainBanner})` }}>
      <div className="bloc-tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          BUKU
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          TA/KP
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          CD/DVD
        </button>
      </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input 
              class="search_search" 
              type="search" 
              placeholder=" Judul, Pengarang, ID, Penerbit" 
              onChange={(event) =>handleSearch(event)}
            />        
            <input 
              class="search_button" 
              type="submit" 
              value="Cari"
            />
          </div>
          <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Cover Buku</StyledTableCell>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell >Pengarang</StyledTableCell>
            <StyledTableCell >Klasifikasi</StyledTableCell>
            <StyledTableCell >Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
            <StyledTableCell>
              {val.gambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={val.gambar} alt="Buku"/>}
            </StyledTableCell>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell>{val.pengarang}</StyledTableCell>
              <StyledTableCell >{val.klasifikasi}</StyledTableCell>
              <StyledTableCell >
              <Link to="#" onClick={() => handleClickOpenDetailBuku(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faInfoCircle" style={{color: 'blue'}} /></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
        </div>

        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input 
              class="search_search" 
              type="search" 
              placeholder=" Judul" 
            />        
            <input 
              class="search_button" 
              type="submit" 
              value="Cari"
            />
          </div>
          <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell>Jenis</StyledTableCell>
            <StyledTableCell>Pembimbing</StyledTableCell>
            <StyledTableCell>Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData2.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData2
          ).map((val) => (
            <StyledTableRow key={val.id}>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell>{val.jenis}</StyledTableCell>  
              <StyledTableCell >{val.pembimbing}</StyledTableCell>
              <StyledTableCell >
              <Link to="#" onClick={() => handleClickOpenDetailTA(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue'}} /></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={5}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
        </div>

        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input 
              class="search_search" 
              type="search" 
              placeholder=" Judul, Pengarang, ID, Penerbit" 
            />        
            <input 
              class="search_button" 
              type="submit" 
              value="Cari"
            />
          </div>
          <Table className={classes.table} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Judul</StyledTableCell>
                <StyledTableCell >Jenis</StyledTableCell>
                <StyledTableCell >Klasifikasi</StyledTableCell>
                <StyledTableCell >Alat</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(rowsPerPage > 0
                ? filteredData3.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                : filteredData3
              ).map((val) => (
                <StyledTableRow key={val.id}>
                  <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
                  <StyledTableCell >{val.jenis}</StyledTableCell>
                  <StyledTableCell >{val.klasifikasi}</StyledTableCell>
                  <StyledTableCell>
                  <Link to="#" onClick={() => handleClickOpenDetailCD(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faInfoCircle" style={{color: 'blue'}} /></Link>
                  </StyledTableCell>
                </StyledTableRow>
              ))}
              {emptyRows > 0 && (
                <TableRow style={{ height: 53 * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                  colSpan={5}
                  count={filteredData.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </div>        
      </div>
    </div>
    <Dialog open={openDetailCD} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail CD/DVD</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Cover CD/DVD</label>
            </div>
            {/* <div class="form-row">
              <label class="col-md-4">
              {detailGambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={detailGambar} alt="."/>}
              </label>
            </div> */}
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-9">: {detailIdCreate}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul</label>
              <label class="col-md-9">: {detailJudulCdDvd}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-9">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Deskripsi</label>
              <label class="col-md-9">: {detailDeskripsi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Sumber</label>
              <label class="col-md-9">: {detailSumber}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Prodi Pemilik</label>
              <label class="col-md-9">: {detailProdiPemilik}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-9">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ISBN</label>
              <label class="col-md-9">: {detailISBN}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-9">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tanggal Masuk</label>
              <label class="col-md-9">: {detailTanggalMasuk}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Status</label>
              <label class="col-md-9">: </label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              {detailStok > 0 ?
                <Link to="/buku-user">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={pinjamCD}
                  type="submit"
                >
                  Pinjam
                </Button>
              </Link> :
                <button class="col-md-2" disabled>Pinjam</button>
            }
              <button class="col-md-2" onClick={handleClose} className={classes.button}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    
    <Dialog open={openDetailTA} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail TA/KP/PA</h2></center><br/>
          <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-9">: {detailId}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul</label>
              <label class="col-md-9">: {detailJudulTaKpPa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-9">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Abstrak</label>
              <label class="col-md-9">: {detailAbstrak}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penulis</label>
              <label class="col-md-9">: {detailPenulis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-9">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-9">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penguji</label>
              <label class="col-md-9">: {detailPenguji}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penguji 2</label>
              <label class="col-md-9">: {detailPenguji2}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 1</label>
              <label class="col-md-9">: {detailPembimbing}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 2</label>
              <label class="col-md-9">: {detailPembimbing2}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-9">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tingkat</label>
              <label class="col-md-9">: {detailTingkat}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Status</label>
              <label class="col-md-9">: {detailStatus}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <button class="col-md-2" onClick={handleClose} className={classes.button}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    
    <Dialog open={openDetailBuku} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail Buku</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Cover Buku</label>
            </div>
            <div class="form-row">
              <label class="col-md-4">
              {detailGambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={detailGambar} alt="."/>}
              </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-4">: {detailIdCreate}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul Buku</label>
              <label class="col-md-4">: {detailJudulBuku}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Bahasa</label>
              <label class="col-md-4">: {detailBahasa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-4">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pengarang</label>
              <label class="col-md-4">: {detailPengarang}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penerbit</label>
              <label class="col-md-4">: {detailPenerbit}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Deskripsi</label>
              <label class="col-md-4">: {detailDeskripsi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-4">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Edisi</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-4">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Stok</label>
              <label class="col-md-4">: {detailStok}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-4">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Copy/Original</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-4">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              {detailStok > 0 ?
                <Link to="/buku-user">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={pinjamBuku}
                  type="submit"
                >
                  Pinjam
                </Button>
              </Link> :
                <button class="col-md-2" disabled>Pinjam</button>
            }
              <button class="col-md-2" onClick={handleClose} className={classes.button}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    </>
  );
}

export default Pencarian;