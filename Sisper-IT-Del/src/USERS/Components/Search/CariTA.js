import React, {useState} from 'react';
import './Cari.css'

function CariTA() {
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <div className="container_cari">
      <div className="bloc-tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          CARI TA/KP/PA
        </button>
      </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input class="search_search" type="search" placeholder=" Judul, Pengarang, ID, Penerbit" />        
            <input class="search_button" type="submit" value="Cari"/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CariTA;