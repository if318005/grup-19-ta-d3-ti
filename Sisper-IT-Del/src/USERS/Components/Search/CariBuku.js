import React, {useState, useEffect } from 'react';
import Axios from 'axios';
import './Cari.css'

function CariBuku() {
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);

  const handleSearch = (event) =>{
  }
  useEffect(() => {
    Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka').then(response => {
        console.log(response.data)
        setAllData(response.data);
        setFilteredData(response.data);
        })
        .catch(error => {
            console.log('Error getting fake data: ' + error);
            })
  }, []);

  const styles = {
    display:'inline',
    width:'30%',
    height:50,
    float:'left',
    padding:5,
    border:'0.5px solid black',
    marginBottom:10,
    marginRight:10
    }

  return (
      <>
    <div className="container_cari">
      <div className="bloc-tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          CARI BUKU
        </button>
      </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input 
                class="search_search" 
                type="search" 
                placeholder=" Judul, Pengarang, ID, Penerbit" 
                onChange={(event) =>handleSearch(event)}
            />        
            <input 
                class="search_button" 
                type="submit" 
                value="Cari"
                
            />
          </div>
        </div>
      </div>
    </div>
    <div>
        <div style={{ margin: '0 auto', marginTop: '10%' }}>
            <label>Search:</label>
            <input type="text" onChange={(event) =>handleSearch(event)} />
        </div>
        <div style={{padding:10}}>
            {filteredData.map((value,index)=>{
                return(
                    <div>
                        <div style={styles} key={value.id}>
                        {value.title}
                        </div>
                    </div>
                )
            })}
        </div>
    </div>
    </>
  );
}

export default CariBuku;