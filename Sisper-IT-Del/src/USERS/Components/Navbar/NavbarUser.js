import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './NavbarUser.css';
import {DBahanPustakaUser, DTentangPerpusUser, DPemberitahuanUser, DInformasiAkun} from '../Dropdown/DropdownUser';

function NavbarUser() {
  const [click, setClick] = useState(false);
  const [dropdown, setDropdown] = useState(false);
  const [dropdown2, setDropdown2] = useState(false);
  const [dropdown3, setDropdown3] = useState(false);
  const [dropdown4, setDropdown4] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const onMouseEnter = () => {if (window.innerWidth < 960) {setDropdown(false);} else {setDropdown(true);}};
  const onMouseEnter2 = () => {if (window.innerWidth < 960) {setDropdown2(false);} else {setDropdown2(true);}};
  const onMouseEnter3 = () => {if (window.innerWidth < 960) {setDropdown3(false);} else {setDropdown3(true);}};
  const onMouseEnter4 = () => {if (window.innerWidth < 960) {setDropdown4(false);} else {setDropdown4(true);}};

  const onMouseLeave = () => {if (window.innerWidth < 960) {setDropdown(false);} else {setDropdown(false);}};
  const onMouseLeave2 = () => {if (window.innerWidth < 960) {setDropdown2(false);} else {setDropdown2(false);}};
  const onMouseLeave3 = () => {if (window.innerWidth < 960) {setDropdown3(false);} else {setDropdown3(false);}};
  const onMouseLeave4 = () => {if (window.innerWidth < 960) {setDropdown4(false);} else {setDropdown4(false);}};

  return (
    <>
      <nav className='navbar'>
        <div className='menu-icon' onClick={handleClick}>
          <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
        </div>
        <ul className={click ? 'nav-menu active' : 'nav-menu'}>
          <li className='nav-item'>
            <Link to='/beranda-user' className='nav-links' onClick={closeMobileMenu}>
              BERANDA
            </Link>
          </li>
          <li
            className='nav-item'
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
          >
            <Link
              to='/user'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              PEMBERITAHUAN <i className='fas fa-caret-down' />
            </Link>
            {dropdown && <DPemberitahuanUser />}
          </li>
          <li 
            className='nav-item'
            onMouseEnter={onMouseEnter2}
            onMouseLeave={onMouseLeave2}
          >
            <Link
              to='/user'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              BAHAN PUSTAKA <i className='fas fa-caret-down' />
            {dropdown2 && <DBahanPustakaUser />}
            </Link>
          </li>
          <li 
            className='nav-item'
            onMouseEnter={onMouseEnter3}
            onMouseLeave={onMouseLeave3}
          >
            <Link
              to='/user'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              TENTANG PERPUS <i className='fas fa-caret-down' />
            {dropdown3 && <DTentangPerpusUser />}
            </Link>
          </li>
          <li className='nav-item'
            onMouseEnter={onMouseEnter4}
            onMouseLeave={onMouseLeave4}>
            <Link
              to='/user'
              className='nav-links'
              onClick={closeMobileMenu}
            >
              INFORMASI AKUN ANDA <i className='fas fa-caret-down' />
            {dropdown4 && <DInformasiAkun />}
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
}

export default NavbarUser;
