import React from 'react';
import { makeStyles} from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import jwt_decode from "jwt-decode";

const useStyles = makeStyles({
  header: {
    fontSize: '30px',
    fontWeight: 'medium',
    marginLeft: '60px',
  },
  img: {
    fontSize: '22px',
    imageResolution: '100%',
    height: '100px',
    width: '100px',
    marginRight: 'auto',
    marginLeft: '60px',
  },
  button: {
    margin: '0 0 0 60px',
    backgroundColor: '#67BCF3',
    color: 'black',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
    marginLeft: '60px',
  }
});

function AuthVerify(){
  if(localStorage.getItem("token")){
    var decodedToken = jwt_decode(localStorage.getItem("token"));
    if (Date.now() >= decodedToken.exp*1000){
      
      return false
    }
    return true
  }
  return false
}



function Header() {
  
  const logout = () =>{
    localStorage.removeItem("token");
    localStorage.setItem("login", false);
    localStorage.removeItem("username");
    localStorage.removeItem("role")
  }

  const classes = useStyles();
  

  return (
    <>
      <div className='Header' style={{maxHeight:'100px'}}>
          <div class="row">
            <div class="col-sm-1 my-auto"><Link to="/beranda"><img src="images/Logo-Sisper-ITDel.png" className={classes.img} alt="." /></Link></div>
            <div class="col-sm-8 my-auto"><h4 className={classes.header}>
            Sistem Informasi Perpustakaan IT Del
          </h4></div>
            {AuthVerify && localStorage.getItem("login")? 
            <div class="col-sm-3 my-auto"><Link to="/login" onClick={logout}><button className={classes.button}>Logout</button></Link></div> :
            <div class="col-sm-3 my-auto"><Link to="/login"><button className={classes.button}>Login</button></Link></div>  
          }
          </div>
      </div>
    </>
  );
}

export default Header;
