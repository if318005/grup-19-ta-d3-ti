import * as React from "react";
import './KoleksiBuku.css'

function KoleksiBuku() {


    return (
        <div className="wrapper" style={{backgroundColor: "#eee1d4"}}>
      <Card
        img="images/Buku.png"
        title="Manajemen Sistem Informasi"
      />

      <Card
        img="images/Data.png"
        title="Analisis dan Disain Informasi : pendekatan ters"
      />

      <Card
        img="images/Laporan.png"
        title="Baked Cod with Vegetables"
      />

        <Card
            img="images/Buku.png"
            title="Baked Cod with Vegetables"
        />

        <Card
        img="images/Data.png"
        title="Simple Risotto"
      />
        </div>
        )
    }
function Card(props) {
    return (
      <div className="card">
        <div className="card__body">
          <img src={props.img} class="card__image" />
          <h6 className="card__title">{props.title}</h6>
        </div>
        <button className="card__btn">Detail buku</button>
      </div>
    );
  }
export default KoleksiBuku;