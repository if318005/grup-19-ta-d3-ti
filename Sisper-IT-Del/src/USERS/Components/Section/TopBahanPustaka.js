import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
//import mainBanner from '../Images/Background-Buku.png';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: '1000px',
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
  },
});

const TopBahanPustaka=() => {
  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getBahanPustaka = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response)=> {
          setBukuList(response.data);
          console.log(bukuList);
      });
    };
    getBahanPustaka();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

    const [cardGambar, setCardGambar] = useState('')
    const [cardJudul, setCardJudul] = useState('')

    const handleClickOpenDetail = (val) => {
      setCardGambar(val.gambar)
      setCardJudul(val.judul_pustaka)
    }

  return (
    <>
    <div>
      <hr />
      <center><h4><b>10 bahan pustaka yang sering dipinjam</b></h4></center>
      <hr />
        <Card
          
        />
      </div>
    </>
  );
}
function Card(props) {
  return (
    <div className="card">
      <div className="card__body">
        <img src={props.img} class="card__image" alt="."/>
        <h5 className="card__title">{props.title}</h5>
        <p className="card__description">{props.description}</p>
      </div>
    </div>
  );
}

export default TopBahanPustaka;