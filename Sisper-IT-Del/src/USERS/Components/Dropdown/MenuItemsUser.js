export const Pemberitahuan = [
  {
    title: 'Bahan Pustaka Baru',
    path: '/bahan-pustaka-baru-user',
    cName: 'dropdown-link'
  },
  // {
  //   title: 'Artikel',
  //   path: '/artikel-user',
  //   cName: 'dropdown-link'
  // },
  // {
  //   title: 'Rating Buku',
  //   path: '/rating-buku-user',
  //   cName: 'dropdown-link'
  // }
];

export const BahanPustaka = [
  {
    title: 'Buku',
    path: '/buku-user',
    cName: 'dropdown-link'
  },
  {
    title: 'CD/DVD',
    path: '/cd-user',
    cName: 'dropdown-link'
  },
  {
    title: 'TA/KP/PA',
    path: '/ta-user',
    cName: 'dropdown-link'
  }
];

export const TentangPerpus = [{
  title: 'Tentang Perpustakaan',
  path: '/sejarah-perpustakaan-user',
  cName: 'dropdown-link'
  },
  {
    title: 'Panduan Pengguna',
    path: '/panduan-pengguna-user',
    cName: 'dropdown-link'
  },
  {
    title: 'Panduan Pesan Pinjam',
    path: '/panduan-pesan-pinjam-user',
    cName: 'dropdown-link'
  },
  {
    title: 'Peraturan Perpustakaan',
    path: '/peraturan-perpustakaan-user',
    cName: 'dropdown-link'
  },
  {
    title: 'Galeri',
    path: '/galeri-user',
    cName: 'dropdown-link'
  }
];

export const InformasiAkun = [
  {
    title: 'Selamat Datang ' + localStorage.getItem("username"),
    cName: 'dropdown-link'
  },
  {
    title: 'Informasi Bahan Pustaka',
    path: '/informasi-bahan-pustaka',
    cName: 'dropdown-link'
  },
  {
    title: 'Notifikasi dari Admin',
    path: '/notifikasi-admin',
    cName: 'dropdown-link'
  },
  {
    title: 'Ganti Password',
    path: '/ganti-password',
    cName: 'dropdown-link'
  }
];