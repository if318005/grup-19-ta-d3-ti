import React, {useState, useEffect} from 'react';
import NavbarUser from '../Components/Navbar/NavbarUser';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import { Link} from 'react-router-dom';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import mainBanner from '../Components/Images/Background-Buku.png';
import './style.css'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#061D47',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  const useStyles = makeStyles({
    table: {
      maxWidth: 1200,
      marginLeft: 'auto',
      marginRight: 'auto',
      border: '1px solid #c6c6c6',
    },
    button: {
      backgroundColor: '#229644;',
      color: 'white',
      width:'250px',
      height: '50px',
      borderRadius: '10px',
      marginLeft: '75px',
    }
  });
  

function BahanPustakaBaru() {
    const [toggleState, setToggleState] = useState(1);

    const toggleTab = (index) => {
      setToggleState(index);
    };
    const classes = useStyles();
    const [bukuList, setBukuList] = useState([])
    const [cdList, setCdList] = useState([])
    const [taList, setTaList] = useState([])

    useEffect(()=> {
      const getBuku = () => {
        Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBukuBaru', {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setBukuList(response.data);
              console.log(bukuList);
          });
      };
      const getCD = () => {
        Axios.get('http://localhost:8082/api/v1/bahanPustaka/getCdBaru', {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setCdList(response.data);
              console.log(cdList);
          });
      };
      const getTA = () => {
        Axios.get('http://localhost:8082/api/v1/bahanPustaka/getTaBaru').then((response)=> {
              setTaList(response.data);
              console.log(taList);
          });
      };
      getBuku();
      getCD();
      getTA();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
        <Header />
        <NavbarUser />
          <div style={{ backgroundImage: `url(${mainBanner})` }}>
            <br/><h1>Bahan Pustaka Baru</h1>
            <Link to="/request"><button className={classes.button}>Request Bahan Pustaka</button></Link>
            <div className="bahanPustakaBaru">
              <div className="bloc-tabs">
                <button
                  className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(1)}
                >
                  Buku
                </button>
                <button
                  className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(2)}
                >
                  CD/DVD
                </button>
                <button
                  className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(3)}
                >
                  TA/KP/PA
                </button>
              </div>

              <div>
                <div
                  className={toggleState === 1 ? "content  active-content" : "content"}
                >
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                    <TableRow>
                        <StyledTableCell>Judul</StyledTableCell>
                        <StyledTableCell>Pengarang</StyledTableCell>
                        <StyledTableCell>Jenis</StyledTableCell>
                        <StyledTableCell>Status</StyledTableCell>
                        <StyledTableCell>Klasifikasi</StyledTableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {bukuList.map((val) => (
                        <StyledTableRow key={val.id}>
                        <StyledTableCell component="th" scope="row">
                          {val.judul_pustaka}
                        </StyledTableCell>
                        <StyledTableCell>{val.pengarang}</StyledTableCell>
                        <StyledTableCell>{val.jenis_pustaka}</StyledTableCell>
                        <StyledTableCell>{val.status_pustaka}</StyledTableCell>
                        <StyledTableCell>{val.klasifikasi}</StyledTableCell>
                        </StyledTableRow>
                    ))}
                    </TableBody>
                </Table>
                </div>
              
                  <div
                    className={toggleState === 2 ? "content  active-content" : "content"}
                  >
                        <Table className={classes.table} aria-label="customized table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell>Judul</StyledTableCell>
                                <StyledTableCell>Jenis</StyledTableCell>
                                <StyledTableCell>Status</StyledTableCell>
                                <StyledTableCell>Prodi</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {cdList.map((val) => (
                                <StyledTableRow key={val.id}>
                                <StyledTableCell component="th" scope="row">
                                  {val.judul_pustaka}
                                </StyledTableCell>
                                <StyledTableCell>{val.jenis_pustaka}</StyledTableCell>
                                <StyledTableCell>{val.status_pustaka}</StyledTableCell>
                                <StyledTableCell>{val.prodi}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                  </div>
                
                  <div
                    className={toggleState === 3 ? "content  active-content" : "content"}
                  >
                        <Table className={classes.table} aria-label="customized table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell>Judul</StyledTableCell>
                                <StyledTableCell>Jenis</StyledTableCell>
                                <StyledTableCell>Pembimbing</StyledTableCell>
                                <StyledTableCell>Tahun</StyledTableCell>
                                <StyledTableCell>Status</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {taList.map((val) => (
                                <StyledTableRow key={val.id}>
                                <StyledTableCell component="th" scope="row">
                                  {val.judul_pustaka}
                                </StyledTableCell>
                                <StyledTableCell>{val.jenis_pustaka}</StyledTableCell>
                                <StyledTableCell>{val.pembimbing}</StyledTableCell>
                                <StyledTableCell>{val.tahun}</StyledTableCell>
                                <StyledTableCell>{val.status}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                  </div>
              </div>
            </div>
          </div>
        <Footer />
        </>
  );
}

export default BahanPustakaBaru;
