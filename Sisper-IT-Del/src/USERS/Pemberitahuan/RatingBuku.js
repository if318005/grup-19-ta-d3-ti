import React from 'react';
import NavbarUser from '../Components/Navbar/NavbarUser';
import mainBanner from '../Components/Images/Background-Buku.png';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';

function RatingBuku() {

  return (
    <>
    <Header />
    <NavbarUser />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <h1>Rating Buku</h1>
    </div>
    <Footer />
    </>
  );
}

export default RatingBuku;
