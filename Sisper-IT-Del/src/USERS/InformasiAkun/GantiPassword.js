import React from 'react';
import GantiSandi from '../../forms/GantiSandi';
import NavbarUser from '../Components/Navbar/NavbarUser';

function GantiPassword() {

  return (
    <>
    <NavbarUser />
    <GantiSandi />
    </>
  );
}

export default GantiPassword;
