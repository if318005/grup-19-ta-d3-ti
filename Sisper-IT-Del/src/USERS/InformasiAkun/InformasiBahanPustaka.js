import React, {useState, useEffect} from 'react';
import NavbarUser from '../Components/Navbar/NavbarUser';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import mainBanner from '../Components/Images/Background-Buku.png';
import '../Components/pages/Pencarian.css'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#061D47',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  const useStyles = makeStyles({
    table: {
      maxWidth: 1200,
      marginLeft: 'auto',
      marginRight: 'auto',
      border: '1px solid #c6c6c6',
    },
    button: {
      backgroundColor: '#229644;',
      color: 'white',
      width:'250px',
      height: '50px',
      borderRadius: '10px',
      marginLeft: '75px',
    }
  });
  

function InformasiBahanPustaka() {
    const [toggleState, setToggleState] = useState(1);

    const toggleTab = (index) => {
      setToggleState(index);
    };
    const classes = useStyles();
    const [bukuList, setBukuList] = useState([])
    const [cdList, setCdList] = useState([])
    const [taList, setTaList] = useState([])

    useEffect(()=> {
      const getPinjam = () => {
        Axios.get('http://localhost:8082/api/v1/transaksiBuku/bahanPustakaDiPinjam/' + localStorage.getItem("username"), {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setBukuList(response.data);
              console.log(bukuList);
          });
      };
      const getPesan = () => {
        Axios.get('http://localhost:8082/api/v1/transaksiBuku/bahanPustakaDiPesan/' + localStorage.getItem("username"), {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setCdList(response.data);
              console.log(cdList);
          });
      };
      const getAmbil = () => {
        Axios.get('http://localhost:8082/api/v1/transaksiBuku/bahanPustakaHarusAmbil/' + localStorage.getItem("username"),{
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setTaList(response.data);
              console.log(taList);
          });
      };
      getPinjam();
      getPesan();
      getAmbil();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
        <Header />
        <NavbarUser />
          <div style={{ backgroundImage: `url(${mainBanner})` }}>
            <br/><h1>Informasi Bahan Pustaka</h1>
            <div className="pustaka-baru">
              <div className="bloc-tabs">
                <button
                  className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(1)}
                >
                  Bahan Pustaka yang dipinjam
                </button>
                <button
                  className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(2)}
                >
                  Bahan Pustaka yang dipesan
                </button>
                <button
                  className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
                  onClick={() => toggleTab(3)}
                >
                  Bahan Pustaka yang harus diambil
                </button>
              </div>

              <div>
                <div
                  className={toggleState === 1 ? "content  active-content" : "content"}
                >
                <Table className={classes.table} aria-label="customized table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell>ID Bahan Pustaka</StyledTableCell>
                                <StyledTableCell>Judul</StyledTableCell>
                                <StyledTableCell>Waktu peminjaman</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {taList.map((val) => (
                                <StyledTableRow key={val.id}>
                                <StyledTableCell>{val.id_buku}</StyledTableCell>
                                <StyledTableCell>
                                  {val.judul_buku}
                                </StyledTableCell>
                                <StyledTableCell>{val.tanggal_peminjaman}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                </div>
              
                  <div
                    className={toggleState === 2 ? "content  active-content" : "content"}
                  >
                        <Table className={classes.table} aria-label="customized table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell>ID Bahan Pustaka</StyledTableCell>
                                <StyledTableCell>Judul</StyledTableCell>
                                <StyledTableCell>Waktu pemesanan</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {taList.map((val) => (
                                <StyledTableRow key={val.id}>
                                <StyledTableCell>{val.id_buku}</StyledTableCell>
                                <StyledTableCell>
                                  {val.judul_buku}
                                </StyledTableCell>
                                <StyledTableCell>{val.created_at}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                  </div>
                
                  <div
                    className={toggleState === 3 ? "content  active-content" : "content"}
                  >
                        <Table className={classes.table} aria-label="customized table">
                            <TableHead>
                            <TableRow>
                                <StyledTableCell>ID Bahan Pustaka</StyledTableCell>
                                <StyledTableCell>Judul</StyledTableCell>
                                <StyledTableCell>Waktu pemesanan</StyledTableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {taList.map((val) => (
                                <StyledTableRow key={val.id}>
                                <StyledTableCell>{val.id_buku}</StyledTableCell>
                                <StyledTableCell>
                                  {val.judul_buku}
                                </StyledTableCell>
                                <StyledTableCell>{val.created_at}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                            </TableBody>
                        </Table>
                  </div>
              </div>
            </div>
          </div>
        <Footer />
        </>
  );
}

export default InformasiBahanPustaka