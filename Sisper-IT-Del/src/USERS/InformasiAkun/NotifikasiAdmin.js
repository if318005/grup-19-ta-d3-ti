import React, {useState, useEffect} from 'react';
import NavbarUser from '../Components/Navbar/NavbarUser';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import mainBanner from '../Components/Images/Background-Buku.png';
import '../Components/pages/Pencarian.css'

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#061D47',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    }
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);
  
  const useStyles = makeStyles({
    table: {
      maxWidth: 1200,
      minHeight: 300,
      marginLeft: 'auto',
      marginRight: 'auto',
      border: '1px solid #c6c6c6',
      backgroundColor: 'white'
    },
    button: {
      backgroundColor: '#229644;',
      color: 'white',
      width:'250px',
      height: '50px',
      borderRadius: '10px',
      marginLeft: '75px',
    }
  });
  

function NotifikasiAdmin() {
    const classes = useStyles();
    const [bukuList, setBukuList] = useState([])

    useEffect(()=> {
      const getBuku = () => {
        Axios.get('http://localhost:8082/api/v1/transaksiBuku/getNotifikasi/' + localStorage.getItem("username"), {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
              setBukuList(response.data);
              console.log(bukuList);
          });
      };
      getBuku();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    return (
        <>
        <Header />
        <NavbarUser />
          <div style={{ backgroundImage: `url(${mainBanner})` }}>
            <br/><h1>Notifikasi dari Admin</h1>
            <div className="pustaka-baru">
              <Table className={classes.table} aria-label="customized table">
                <TableBody>
                  {bukuList.map((val) => (
                    <StyledTableRow>
                      <StyledTableCell>o {val.notifikasi}</StyledTableCell>
                    </StyledTableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </div>
    <Footer />
    </>
  );
}

export default NotifikasiAdmin