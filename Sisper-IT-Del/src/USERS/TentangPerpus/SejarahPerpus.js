import React, {useState} from 'react';
import Navbar from '../Components/Navbar/NavbarUser';
import Footer from '../Components/Footer/Footer';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Header from '../Components/Header/Header';
import Perpus1 from '../Components/Images/Perpus1.PNG';
import Perpus2 from '../Components/Images/SIPP.PNG';
import Perpus5 from '../Components/Images/Perpus5.PNG';
import Penghargaan from '../Components/Images/Penghargaan.PNG';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrophy } from "@fortawesome/free-solid-svg-icons";
import './style.css'

const useStyles = makeStyles({
  panduanDetails: {
    flex: 1,
    background: 'white',
    padding: '20px',
  },
});

function SejarahPerpus() {
    const [toggleState, setToggleState] = useState(1);

    const toggleTab = (index) => {
        setToggleState(index);
    };
    
  const classes = useStyles();

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /> <br />
      <div className="tentang-perpus">
        <div className="bloc-tabs">
          <button
            className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(1)}
          >
            Sejarah Perpustakaan
          </button>
          <button
            className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(2)}
          >
            Layanan Perpustakaan
          </button>
          <button
            className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(3)}
          >
            Pencapaian Perpustakaan
          </button>
        </div>

        <div className="content-tabs">
          <div
            className={toggleState === 1 ? "content  active-content" : "content"}
          >
              <Grid item xs={12} md={12}>
                  <h3><center>Sejarah Perpustakaan IT Del</center></h3>
                  <div className={classes.panduanDetails}>
                      <Typography variant="subtitle1" paragraph>
                          
                      <center><img src={Perpus5} alt="Perpus5" width="absolute" height="400px"/></center>
                      
                      <text>
                      Perpustakaan Politeknik Informatika Del (PI Del) berdiri sejak tanggal 9 Januari 2002 dengan sistem pelayanan  open acces (terbuka). 
                      Selanjutnya berubah nama seiring dengan berubahnya status Politeknik menjadi Institut sejak tahun 2013. 
                      Perpustakaan berganti nama menjadi Perpustakaan Institut Teknologi Del. 
                      Perpustakaan Institut Teknologi Del memiliki fungsi utama yaitu fungsi pendidikan. 
                      Pengadaan koleksi dan pengembangan minat baca internal dan eksternal Institut Teknologi Del dilakukan sebagai bagian dari kegiatan diseminasi kegiatan peningkatan  minat baca.<br/><br/>

                      Perpustakaan IT Del memiliki visi yaitu “Sebagai pusat informasi terkemuka yang menyediakan layanan informasi cetak maupun cetak dan juga penyedia layanan informasi berteknologi tinggi. 
                      Untuk mencapai visi dimaksud, misi yang diemban oleh Perpustakaan IT Del adalah sebagai berikut:<br/>
        
                      1. Menyediakan layanan pustaka yang mendukung program-program akademik dan non akademik.<br/>

                      2. Memberikan layanan informasi cetak maupun non cetak bagi seluruh civitas akademika PI Del maupun kepada masyarakat umum.<br/>

                      3. Meningkatkan cara dan proses diseminasi koleksi lebih efisien dan efektif.<br/>

                      4. Meningkatkan keragaman koleksi demi peningkatan mutu layanan<br/><br />

                      Pada awal berdiri, perpustakaan IT Del menempati 1 ruangan yang berada di Lt. 2 Gedung administrasi dan Ruangan direktur IT Del. 
                      Namun demikian perpustakaan banyak dikunjungi oleh mahasiswa/i yang saat itu masih sekitar 60 mahasiswa/i angkatan I PI-Del. 
                      Sangat membanggakan dengan jumlah hanya 138 buku dan baru berdiri setelah 3 bulan penerimaan mahasiwa/i baru, 
                      hal ini yang menyebabkan perpustakaan menjadi tempat yang ramai dikunjungi.<br /><br />

                      Pada tahun 2003 gedung perpustakaan yang baru telah selesai dibangun, letaknya berada di samping gedung auditorium. 
                      Gedung baru terdiri dari 2 lantai, 2 unit ruang diskusi, 1 unit ruang referensi, 1 unit ruang baca tenang, 
                      unit ruang pengolahan buku dan 1 unit ruang kepala perpustakaan.<br /><br />

                      <center><img src={Perpus1} alt="Perpus1" width="absolute" height="400px"/></center><br /><br />

                      Perpustakaan IT Del mendapat dukungan yang sangat baik dari Yayasan Del dan juga pimpinan di IT Del. 
                      Oleh sebab itu pada tahun 2012 koleksi buku mencapai 3936 judul dan 7347 eksemplar. 
                      Selain buku, di perpustakaan IT Del disediakan koleksi pustaka periodical yang uptodate seperti: 
                      reader’s digest, national geographic, the economist, Koran kompas, Koran SIB, The Jakarta Pos, Tabloid Bola, 
                      Harian Analisa, Majalah Tempo, CHIP, INfokomputer, Infolinux, Weekender dan berbagai koleksi lain diantaranya jurnal dan prosiding. 
                      Koleksi unggulan IT Del adalah koleksi buku-buku bidang Teknologi Informasi terkini dan uptodate. 
                      Koleksi dimaksud diperoleh dari toko buku online yang tepercaya dan memasarkan buku-buku terbaru, bahkan buku yang belum terbit dapat dipesan. 
                      Selain buku-buku IT terkini, di perpustakaan juga disediakan buku-buku non IT terkini yang berguna untuk pemerkayaan pengetahuan, 
                      personal improvement, dan buku-buku yang sifatnya buku hiburan.<br /><br />

                      Layanan yang diberikan di perpustakaaan IT Del bukan hanya untuk kalangan tertentu saja. 
                      Layanan transaksi bahan pustaka dapat diperoleh oleh segenap staf dan karyawan yang berada di bawah naungan Yayasan Del. 
                      Selain staf dan karyawan, keluarga staf dan karyawan juga dapat memperoleh layanan di Perpustakaan IT Del.<br /><br />

                      Perpustakaan juga dilengkapi dengan koleksi CD/DVD untuk membantu mahasiswa belajar program-program komputer dan juga Bahasa Inggris melalui CD/DVD, terutama untuk belajar-sendiri. 
                      Penyediaan sumber perpustakan ini ditujukan untuk membantu mahasiswa meningkatkan kemampuan dalam pemrograman dan menggunakan Bahasa Inggris.<br /><br />

                      Sesuai dengan perkembangan informasi, maka perpustakaan IT Del memiliki sistem informasi yang disebut dengan sistem informasi perpustakaan IT Del (SIPP). 
                      Sistem informasi senantiasa uptodate dengan bantuan tim PSDI IT Del. 
                      Sistem informasi dimaksud dapat diakses melalui HP Android.<br /><br />

                      <center><img src={Perpus2} alt="Perpus2" width="absolute" height="400px"/></center><br /><br />

                          </text>
                      </Typography>
                  </div>
              </Grid>
          </div>
          <div
            className={toggleState === 3 ? "content  active-content" : "content"}
          >
              <Grid item xs={12} md={12}>
                <h3><center>Penghargaan yang Pernah diraih Perpustakaan IT Del</center></h3>
                  <div className={classes.panduanDetails}>
                      <center><img src={Penghargaan} alt="Penghargaan" width="absolute" height="400px"/></center><br/>
                      <Typography variant="subtitle1" paragraph>
                          <text>
                          <FontAwesomeIcon icon={faTrophy} size="1x" className="faTrophy" style={{color: 'orange'}} /> Perpustakaaan Terbaik 1 (pertama) se-Sumut tahun 2012<br/>
                          <FontAwesomeIcon icon={faTrophy} size="1x" className="faTrophy" style={{color: 'orange'}} /> Pustakawan Terbaik 1 (pertama) se-Sumut tahun 2012<br/>
                          <FontAwesomeIcon icon={faTrophy} size="1x" className="faTrophy" style={{color: 'orange'}} /> Perpustakaan Terbaik ke 2 (kedua) se-Sumut tahun 2017<br/>
                          <FontAwesomeIcon icon={faTrophy} size="1x" className="faTrophy" style={{color: 'orange'}} /> Pustakawan Terbaik ke 3 (tiga) se-Sumut tahun 2017<br/>
                          <FontAwesomeIcon icon={faTrophy} size="1x" className="faTrophy" style={{color: 'orange'}} /> Perpustakaan Terbaik 1 (pertama) se-sumut tahun 2019<br/>

                          </text>
                      </Typography>
                  </div>
              </Grid>
          </div>
          <div
            className={toggleState === 2 ? "content  active-content" : "content"}
          >
              <Grid item xs={12} md={12}>
                  <h3><center>Jenis Layanan Perpustakaan IT Del</center></h3>
                    <div className="wrapper">
                          <Card
                            img="images/TransaksiBuku.jpg"
                            title="Layanan Transaksi Buku"
                          />
                          <Card
                            img="images/Referensi.jpg"
                            title="Layanan Referensi"
                          />
                          <Card
                            img="images/Jurnal.jpg"
                            title="Layanan Jurnal"
                          />
                          <Card
                            img="images/Print.jpg"
                            title="Layanan Cetak Dokumen"
                          />
                          <Card
                            img="images/Scan.jpg"
                            title="Layanan Scan Dokumen"
                          />
                          <Card
                            img="images/E-Banking.jpg"
                            title="Transaksi e-banking (BRI)"
                          />
                          <Card
                            img="images/TerimaKaryaIlmiah.jpg"
                            title="Layanan Bebas Pustaka dan Berita Acara Serah terima karya Ilmiah"
                          />
                          <Card
                            img="images/Turnitin.jpg"
                            title="Layanan Pengecekan Plagiarisme dengan Software Turnitin"
                          />
                          </div>
              </Grid>
          </div>
        </div>
      </div>
          <br/>
    </div>
    <Footer />
    </>
  );
}

function Card(props) {
  return (
    <div className="card">
      <div className="card__body">
        <img src={props.img} class="card__image" alt="."/>
        <h5 className="card__title">{props.title}</h5>
        <p className="card__description">{props.description}</p>
      </div>
    </div>
  );
}

export default SejarahPerpus;
