import React from 'react';
import Navbar from '../Components/Navbar/NavbarUser';
import Footer from '../Components/Footer/Footer';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';

const useStyles = makeStyles({
  panduanDetails: {
    flex: 1,
    background: 'white',
    margin: '10px 60px 10px 60px',
    padding: '20px',
  },
});

function PanduanPesanPinjam() {
  const classes = useStyles();

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h3><center>Panduan Pesan Pinjam</center></h3> <br/>
      <Grid item xs={12} md={12}>
        <div className={classes.panduanDetails}>
          <Typography variant="subtitle1" paragraph>
            <text>
              Sistem Informasi Perpustakaan IT Del ini dibangun untuk mempermudah peminjaman barang perpustakaan.<br/>
              Ada beberapa langkah yang harus diikuti dalam menjalankan Sistem Informasi Perpustakaan IT Del ini. Cara penggunaan Sistem Informasi Perpustakaan IT Del ini adalah sebagai berikut:<br/><br/>

              1. Anggota dapat melihat barang-barang yang dipinjam, dipesan dan yang harus diambil dari perpustakaan di menu informasi akun anda<br/>
              2. Untuk melihat barang yang harus diambil hari ini anggota dapat memilih "bahan pustaka yang harus diambil hari ini"<br/>
              3. Untuk melihat barang yang harus dipesan anggota dapat memilih "bahan pustaka yang di pesan"<br/>
              4. Untuk melihat barang yang harus dipinjam anggota dapat memilih "bahan pustaka yang dipinjam"<br/>
              5. Jika admin menyetujui pesanan maka akan ada pemberitahuan kepada anggota pada notifikasi dari admin<br/>
              6. Untuk keamanan akun anggota, selesai menggunakan Sistem Informasi Perpustakaan IT Del anggota harus keluar dengan memilih menu keluar<br/>
              7. Untuk memastikan anggota telah keluar dari Sistem Informasi Perpustakaan IT Del maka akan muncul notifikasi logout<br/>
            </text>
          </Typography>
        </div>
      </Grid>
    </div>
    <Footer />
    </>
  );
}

export default PanduanPesanPinjam;
