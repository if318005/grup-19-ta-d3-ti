import React from 'react';
import Navbar from '../Components/Navbar/NavbarUser';
import Footer from '../Components/Footer/Footer';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Header from '../Components/Header/Header';
import Perpus2 from '../Components/Images/Perpus2.PNG';
import Perpus3 from '../Components/Images/Perpus3.PNG';
import Perpus4 from '../Components/Images/Perpus4.PNG';
import Perpusling from '../Components/Images/Perpusling.PNG';
import mainBanner from '../Components/Images/Background-Buku.png';

const useStyles = makeStyles({
  panduanDetails: {
    flex: 1,
    background: 'white',
    margin: '10px 60px 10px 60px',
    padding: '20px',
  },
});

function Galeri() {
  const classes = useStyles();

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h3><center>Pengabdian masyarakat yang dilakukan oleh Perpustakaan IT Del </center></h3><br />
      <Grid item xs={12} md={12}>
              <div className={classes.panduanDetails}>
                  <Typography variant="subtitle1" paragraph>
                  <text>
                        <center><b>Story telling untuk keluarga staf/karyawan IT Del<br/></b></center>

                        <center><img src={Perpus2} alt="Perpus2" width="absolute" height="400px"/></center><br /><br />

                        <center><b>Story telling ini dijadwalkan dilakukan 2 kali dalam seminggu.</b></center><br/>

                        <center><img src={Perpus3} alt="Perpus3" width="absolute" height="400px"/></center><br /><br />

                        <center><b>Ceramah minat baca untuk sekolah di sekitar IT Del</b></center><br/>

                        <center><img src={Perpus4} alt="Perpus4" width="absolute" height="400px"/></center><br /><br />

                        <center><b>Perpusling</b></center><br/>

                        <center><img src={Perpusling} alt="Perpusling" width="absolute" height="400px"/></center><br /><br />

                      </text>
                  </Typography>
              </div>
        </Grid>
      </div>
      <Footer />
    </>
  );
}

export default Galeri;
