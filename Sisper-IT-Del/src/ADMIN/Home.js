import React from 'react';
import '../App.css';
import Footer from './Components/Footer/Footer';
import Navbar from './Components/Navbar/Navbar';
import Cards from './Components/Section/Cards';
import Header from '../ADMIN/Components/Header/Header';

export default function Home() {
  return (
    <>
    <div className='beranda'>
      <Header />
      <Navbar />
      <Cards />
      <Footer />
     </div>
    </>
  );
}