import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme  } from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { faTrash, faPen, faInfoCircle} from "@fortawesome/free-solid-svg-icons";
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import TableFooter from '@material-ui/core/TableFooter';
import LastPageIcon from '@material-ui/icons/LastPage';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Button, Dialog} from '@material-ui/core';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Table from '@material-ui/core/Table';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import Axios from 'axios';

  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#061D47',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

  const useStyles = makeStyles({
    table: {
      maxWidth: 1200,
      marginLeft: 'auto',
      marginRight: 'auto',
      border: '1px solid #c6c6c6',
      backgroundColor: 'white',
    },
    cari: {
      marginLeft: '80px',
      marginBottom: '30px',

    },
    button: {
      backgroundColor: '#061D47',
      marginRight: '10px',
      color: 'white',
      width:'150px',
      height: '35px',
      borderRadius: '50px',
    }
  });

  const useStyles1 = makeStyles((theme) => ({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
    },
  }));

  function TablePaginationActions(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;
  
    const handleFirstPageButtonClick = (event) => {
      onChangePage(event, 0);
    };
  
    const handleBackButtonClick = (event) => {
      onChangePage(event, page - 1);
    };
  
    const handleNextButtonClick = (event) => {
      onChangePage(event, page + 1);
    };
  
    const handleLastPageButtonClick = (event) => {
      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };
  
    return (
      <div className={classes.root}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }

  TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
  };

const CD=() => {
    const [openEdit, setOpenEdit] = React.useState(false);
    const [openDetail, setOpenDetail] = React.useState(false);

    const [allData,setAllData] = useState([]);
    const [filteredData,setFilteredData] = useState(allData);
    
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const handleSearch = (event) => {
      let value = event.target.value.toLowerCase();
      let result = [];
      console.log(value);
      result = allData.filter((data) => {
      return data.judul_pustaka.toLowerCase().search(value) !== -1;
    });

    setFilteredData(result);
    }
    const classes = useStyles();
    const [bukuList, setBukuList] = useState([])

    useEffect(()=> {
      const getCD = () => {
        Axios.get('http://localhost:8082/api/v1/bahanPustaka/getCdDvd', {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then(response => {
          setBukuList(response.data);
          console.log(bukuList);
            setAllData(response.data);
            setFilteredData(response.data);
            })
            .catch(error => {
                console.log('Error getting data: ' + error);
                })
      };
      getCD();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    const hapusCD = (id) =>{
      console.log("ini : " + id);
      const newList = bukuList.filter((val) => val.id !== id);
      setBukuList(newList);
      Swal.fire({
        title: 'Apakah anda ingin menghapus data ini?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Ya`,
        denyButtonText: `Tidak`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const newList = bukuList.filter((val) => val.id !== id);
          setBukuList(newList);
          Axios.delete(`http://localhost:8082/api/v1/bahanPustaka/deleteBahanPustaka/${id}`, {
            headers: {
              Authorization: 'Bearer ' + localStorage.getItem("token")
            }
          }).then((response) => {
            Swal.fire('Berhasil menghapus data', '', 'success').then(
              window.location.reload()
            );
            
          });
          
        } else if (result.isDenied) {
          Swal.fire('Data tidak jadi di hapus', '', 'info')
        }
      })
    };

    //Detail CD
  const [detailJenis, setDetailJenis] = useState('')
  const [detailSubjek, setDetailSubjek] = useState('')
  const [detailJudulCdDvd, setDetailJudulCdDvd] = useState('')
  const [detailDeskripsi, setDetailDeskripsi] = useState('')
  const [detailSumber, setDetailSumber] = useState('')
  const [detailId, setDetailId] = useState('')
  //const [detailProdiPemilik, setDetailProdiPemilik] = useState('')
  const [detailLokasi, setDetailLokasi] = useState('')
  //const [detailISBN, setDetailISBN] = useState('')
  //const [detailJenis, setDetailJenis] = useState('')
  //const [detailRating, setDetailRating] = useState('')
  const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
  //const [detailTanggalMasuk, setDetailTanggalMasuk] = useState('')
  //const [detailStatus, setDetailStatus] = useState('')
  //const [detailIdMasterCd, setDetailIdMasterCd] = useState('')

  const handleClickOpenDetail = (val) => {
    setDetailId(val.id_create)
    setDetailSubjek(val.subjek)
    setDetailJudulCdDvd(val.judul_pustaka)
    setDetailSumber(val.penerbit)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailDeskripsi(val.deskripsi)
    setOpenDetail(true);
  }

    //UPDATE CD
    const [newSumber, setNewSumber] = useState('')
    const [newSubjek, setNewSubjek] = useState('')
    const [newJudulCD, setNewJudulCD] = useState('')
    const [newKlasifikasi, setNewKlasifikasi] = useState('')
    const [newTahun, setNewTahun] = useState('')
    const [newJenis, setNewJenis] = useState('')
    const [newLokasi, setNewLokasi] = useState('')
    const [idUpdate, setIdUpdate] = useState('')

    const handleClickOpenEdit = (val) => {
      setOpenEdit(true);
      setNewJudulCD(val.judul_pustaka)
      setNewSumber(val.sumber)
      setNewTahun(val.tahun)
      setNewKlasifikasi(val.klasifikasi)
      setNewSubjek(val.subjek)
      setNewJenis(val.jenis)
      setNewLokasi(val.lokasi)
      setIdUpdate(val.id)
      console.log(val.lokasi)
    };

    const handleClose = () => {
      setOpenEdit(false);
      setOpenDetail(false);
    };

    const editCD = () => {
      Axios.put("http://localhost:8082/api/v1/bahanPustaka/editCdDvd/" + idUpdate, {
          judul_pustaka: newJudulCD,
          subjek: newSubjek,
          klasifikasi: newKlasifikasi,
          tahun: newTahun,
          lokasi: newLokasi,
          jenis: newJenis,
          sumber: newSumber
          
      }, {
        headers : {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response)=>{
        if(response.data.success === true){
          handleClose();
          Swal.fire({
            title: "Behasil!",
            text: "Data buku telah di ubah",
            type: "success"
          }).then(function() {
              window.location.reload();
          });
        }else{
          handleClose();
          Swal.fire({
            title: "Gagal",
            text: "Data buku gagal di ubah",
            type: "failed"
          });
        }
      });
      setNewJudulCD("")
      setNewSumber("")
      setNewTahun("")
      setNewKlasifikasi("")
      setNewSubjek("")
      setNewJenis("")
      setNewLokasi("")
    };

  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <br/><h1>Daftar CD/DVD</h1>
      <div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul,  Jenis" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell style={{ width: 460 }}>Judul</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Jenis</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Status</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Prodi</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Klasifikasi</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
              <StyledTableCell style={{ height: 100 }}>
                {val.judul_pustaka}
              </StyledTableCell>
              <StyledTableCell >{val.jenis}</StyledTableCell>
              <StyledTableCell >{val.status_pustaka}</StyledTableCell>
              <StyledTableCell >{val.prodi}</StyledTableCell>
              <StyledTableCell >{val.klasifikasi}</StyledTableCell>
              <StyledTableCell >
                <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick={() => handleClickOpenEdit(val)}><FontAwesomeIcon icon={faPen} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick = {() => hapusCD(val.id)}><FontAwesomeIcon icon={faTrash} size="2x" className="faTrash" style={{color: 'red'}}/></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    <br />
    </div>
    <Dialog open={openEdit} onClose={handleClose}>
      <form>
        <div className="Edit-cd">
          <br/><center><h2>Edit Detail CD</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2">Judul CD</label>
              <input 
                type="text" 
                name="judul_pustaka"
                id="judul_pustaka" 
                class="form-control col-md-6"
                value={newJudulCD}
                onChange={(e)=> {
                  setNewJudulCD(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Klasifikasi</label>
              <input 
                type="text" 
                name="klasifikasi"
                id="klasifikasi" 
                value = {newKlasifikasi}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewKlasifikasi(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Tahun</label>
              <input 
                type="text" 
                name="tahun"
                id="tahun" 
                value={newTahun}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewTahun(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Subjek</label>
              <input 
                type="text" 
                name="subjek"
                id="subjek" 
                value={newSubjek}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewSubjek(e.target.value)}}
              />
            </div>
            <div class="form-row">
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  id="lokasi"
                  value={newLokasi}
                  onChange={(e)=> {
                    setNewLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  id="jenis"
                  value={newJenis}
                  onChange={(e)=> {
                    setNewJenis(e.target.value)}}
                >
                  <option></option>
                  <option>CD</option>
                  <option>DVD</option>
                </select>
            </div>
            <div class="form-row">
              <label class="col-md-2">Sumber</label>
              <input 
                type="text" 
                name="sumber"
                id="sumber" 
                value={newSumber}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewSumber(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <div class="col-md-2"></div>
              <div class="col-md-3">
              <Link to="/cd">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={editCD}
                >
                  Simpan
                </Button>
              </Link>
              </div>
              <div class="col-md-3">
                <Link to="/cd">
                    <Button variant="contained" className={classes.button}>Batal</Button>
                </Link>
              </div>
            </div>
          </div>
        </form>
      </Dialog>
    <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail CD/DVD</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Cover CD/DVD</label>
            </div>
            {/* <div class="form-row">
              <label class="col-md-4">
              {detailGambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={detailGambar} alt="."/>}
              </label>
            </div> */}
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-9">: {detailId}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul</label>
              <label class="col-md-9">: {detailJudulCdDvd}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-9">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Deskripsi</label>
              <label class="col-md-9">: {detailDeskripsi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Sumber</label>
              <label class="col-md-9">: {detailSumber}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Prodi Pemilik</label>
              <label class="col-md-9">: </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-9">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ISBN</label>
              <label class="col-md-9">: </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Rating</label>
              <label class="col-md-9"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-9">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tanggal Masuk</label>
              <label class="col-md-9">: </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Status</label>
              <label class="col-md-9">: </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID Master CD</label>
              <label class="col-md-9"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <button class="col-md-2" onClick={handleClose}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    
    <Footer />
    </>
  );
}

export default CD;
