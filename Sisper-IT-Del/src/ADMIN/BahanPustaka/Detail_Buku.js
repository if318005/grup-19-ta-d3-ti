import React, {useState, useEffect, Fragment} from 'react';
import Navbar from '../Components/Navbar/Navbar';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import '../KelolaData/style.css';
import Header from '../Components/Header/Header';

function DetailBuku({match}) {
    console.log(match);
    const [buku, setBuku] = useState([])
    const [open, setOpen] = React.useState(false);

    useEffect(()=> {
        getBuku(match);
      }, [match]);

      const getBuku = (match) => {
        Axios.get(`http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka/${match.params.id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response)=> {
            setBuku(response.data);
          });
      };
      
    return (
        <>
        <Header />
        <Navbar />
        
        <div className="row">
        <div class="col-md-4">
            <img src={buku.gambar} class="card__image" alt="."/>
            </div>
            <div class="col-md-8">
            <h5 className="card__title">{buku.judul_pustaka}</h5>
            <p className="card__description">{buku.deskripsi}</p>
            </div>
        </div>
        <div>
        <TableRow>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
        </TableCell>
        <TableCell align="right">{buku.judul_pustaka}</TableCell>
        <TableCell align="right">{buku.status}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Detail Buku
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell>ID Buku</TableCell>
                    <TableCell>Judul Buku</TableCell>
                    <TableCell align="right">Pengarang</TableCell>
                    <TableCell align="right">Status</TableCell>
                    <TableCell align="right">Bahasa</TableCell>
                    <TableCell align="right">Penerbit</TableCell>
                    <TableCell align="right">Lokasi</TableCell>
                    <TableCell align="right">Tahun</TableCell>
                    <TableCell align="right">Alat</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow key={buku.id}>
                        <TableCell component="th" scope="row">{buku.id}</TableCell>
                        <TableCell>{buku.judul_pustaka}</TableCell>
                        <TableCell align="right">{buku.pengarang}</TableCell>
                        <TableCell align="right">{buku.status_pustaka}</TableCell>
                        <TableCell align="right">{buku.bahasa}</TableCell>
                        <TableCell align="right">{buku.penerbit}</TableCell>
                        <TableCell align="right">{buku.lokasi}</TableCell>
                        <TableCell align="right">{buku.tahun}</TableCell>
                        <TableCell align="right"><button>Pinjam</button></TableCell>
                    </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
</div>
      </>
    )
}
export default DetailBuku;