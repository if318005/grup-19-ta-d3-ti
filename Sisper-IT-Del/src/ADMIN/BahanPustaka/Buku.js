import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import TableHead from '@material-ui/core/TableHead';
import Axios from 'axios';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import GambarBuku from '../Components/Images/Buku.png'
import Swal from 'sweetalert2';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPen, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Dialog} from '@material-ui/core';
import {Link} from 'react-router-dom';
import './style.css';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import PropTypes from 'prop-types';

  const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: '#061D47',
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    formTambah: {
      display: 'flex',
      flexDirection: 'column',
      alignItem: 'center',
      justifyContent: 'center',
    }
  }))(TableCell);

  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);

  const useStyles = makeStyles({
    table: {
      maxWidth: 1200,
      marginLeft: 'auto',
      marginRight: 'auto',
      border: '1px solid #c6c6c6',
      backgroundColor: 'white',
    },
    cari: {
      marginLeft: '80px',
      marginBottom: '30px',

    },
    form: {
      width: '600px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center'
    },
    button: {
      backgroundColor: '#061D47',
      marginRight: '10px',
      color: 'white',
      width:'150px',
      height: '35px',
      borderRadius: '50px',
    },
    gambar: {
      width: '75%',
      height: '75%'
    }
  });
  
  const useStyles1 = makeStyles((theme) => ({ 
    root: { 
      flexShrink: 0, 
      marginLeft: theme.spacing(2.5)
    }
  }));

  function TablePaginationActions(props) {
    const classes = useStyles1();
    const theme = useTheme();
    const { count, page, rowsPerPage, onChangePage } = props;
  
    const handleFirstPageButtonClick = (event) => {
      onChangePage(event, 0);
    };
  
    const handleBackButtonClick = (event) => {
      onChangePage(event, page - 1);
    };
  
    const handleNextButtonClick = (event) => {
      onChangePage(event, page + 1);
    };
  
    const handleLastPageButtonClick = (event) => {
      onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };
  
    return (
      <div className={classes.root}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label="first page"
        >
          {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
        </IconButton>
        <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="next page"
        >
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label="last page"
        >
          {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
        </IconButton>
      </div>
    );
  }
  
  TablePaginationActions.propTypes = {
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
  };

const Buku=() => {
    const [openEdit, setOpenEdit] = React.useState(false);
    const [openDetail, setOpenDetail] = React.useState(false);

    const [allData,setAllData] = useState([]);
    const [filteredData,setFilteredData] = useState(allData);
    
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

    const handleChangePage = (event, newPage) => {
      setPage(newPage);
    };
    
    const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(parseInt(event.target.value, 10));
      setPage(0);
    };

    const handleSearch = (event) => {
      let value = event.target.value.toLowerCase();
      let result = [];
      console.log(value);
      result = allData.filter((data) => {
      return data.judul_pustaka.search(value) !== -1;
    });

    setFilteredData(result);
    }

    const classes = useStyles();
    const [bukuList, setBukuList] = useState([])

    useEffect(()=> {
      const getBuku = () => {
        Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then(response => {
          setBukuList(response.data);
          console.log(bukuList);
            setAllData(response.data);
            setFilteredData(response.data);
            })
            .catch(error => {
                console.log('Error getting fake data: ' + error);
                })
      };
      getBuku();
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    //UPDATE BUKU
    const [newBahasa, setNewBahasa] = useState('')
    const [newSubjek, setNewSubjek] = useState('')
    const [newJudulBuku, setNewJudulBuku] = useState('')
    const [newPengarang, setNewPengarang] = useState('')
    const [newPenerbit, setNewPenerbit] = useState('')
    const [newKlasifikasi, setNewKlasifikasi] = useState('')
    const [newTahun, setNewTahun] = useState('')
    const [newJenis, setNewJenis] = useState('')
    const [newLokasi, setNewLokasi] = useState('')
    const [newStok, setNewStok] = useState('')
    const [newDeskripsi, setNewDeskripsi] = useState('')
    const [idUpdate, setIdUpdate] = useState('')

    //Detail Buku
    const [detailId, setDetailId] = useState('')
    const [detailBahasa, setDetailBahasa] = useState('')
    const [detailSubjek, setDetailSubjek] = useState('')
    const [detailJudulBuku, setDetailJudulBuku] = useState('')
    const [detailPengarang, setDetailPengarang] = useState('')
    const [detailPenerbit, setDetailPenerbit] = useState('')
    const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
    const [detailTahun, setDetailTahun] = useState('')
    const [detailJenis, setDetailJenis] = useState('')
    const [detailLokasi, setDetailLokasi] = useState('')
    const [detailStok, setDetailStok] = useState('')
    const [detailDeskripsi, setDetailDeskripsi] = useState('')
    const [detailGambar, setDetailGambar] = useState('')

    const handleClickOpenDetail = (val) => {
      setDetailId(val.id_create)
      setDetailBahasa(val.bahasa)
      setDetailSubjek(val.subjek)
      setDetailJudulBuku(val.judul_pustaka)
      setDetailPengarang(val.pengarang)
      setDetailPenerbit(val.penerbit)
      setDetailKlasifikasi(val.klasifikasi)
      setDetailTahun(val.tahun)
      setDetailJenis(val.jenis)
      setDetailLokasi(val.lokasi)
      setDetailStok(val.tersedia)
      setDetailDeskripsi(val.deskripsi)
      setDetailGambar(val.gambar)
      setOpenDetail(true);
    }

    const handleClickOpenEdit = (val) => {
      console.log(val);
      setNewJudulBuku(val.judul_pustaka)
      setNewPengarang(val.pengarang)
      setNewPenerbit(val.penerbit)
      setNewKlasifikasi(val.klasifikasi)
      setNewTahun(val.tahun)
      setNewBahasa(val.bahasa)
      setNewSubjek(val.subjek)
      setNewStok(val.tersedia)
      setNewJenis(val.jenis)
      setNewLokasi(val.lokasi)
      setNewDeskripsi(val.deskripsi)
      setIdUpdate(val.id)
      // console.log("adalah ",newDeskripsi)
      console.log("id adalah" ,idUpdate)
      setOpenEdit(true);
    };

    const handleClose = () => {
      setOpenEdit(false);
      setOpenDetail(false);
    };
    const hapusBuku = (id) =>{
      // console.log("ini : " + id);
      Swal.fire({
        title: 'Apakah anda ingin menghapus data ini?',
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: `Ya`,
        denyButtonText: `Tidak`,
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
          const newList = bukuList.filter((val) => val.id !== id);
          setBukuList(newList);
          Axios.delete(`http://localhost:8082/api/v1/bahanPustaka/deleteBahanPustaka/${id}`, {
            headers: {
              Authorization: 'Bearer ' + localStorage.getItem("token")
            }
          }).then((response) => {
            Swal.fire('Berhasil menghapus data', '', 'success').then(
              window.location.reload()
            );
            
          });
          
        } else if (result.isDenied) {
          Swal.fire('Data tidak jadi di hapus', '', 'info')
        }
      })
      
    };

    function editBuku() {
      var updateurl = "http://localhost:8082/api/v1/bahanPustaka/editBuku/"+ idUpdate;
      console.log("Asdf   : ", updateurl);
      Axios.put(updateurl, {
          judul_pustaka: newJudulBuku,
          pengarang: newPengarang,
          bahasa: newBahasa,
          klasifikasi: newKlasifikasi,
          penerbit: newPenerbit,
          lokasi: newLokasi,
          tahun: newTahun,
          subjek: newSubjek,
          jenis: newJenis,
          deskripsi: newDeskripsi,
         tersedia: newStok
          
      },
      {
        headers : {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response) => {
        if(response.data.success === true){
          handleClose();
          Swal.fire({
            title: "Berhasil!",
            text: "Data buku telah di ubah",
            type: "success"
          }).then(function() {
              window.location.reload();
          });
        }else{
          handleClose();
          Swal.fire({
            title: "Gagal",
            text: "Data buku gagal di ubah",
            type: "failed"
          });
        }
      });
      // window.location.reload();
      setNewJudulBuku("")
      setNewPengarang("")
      setNewPenerbit("")
      setNewKlasifikasi("")
      setNewTahun("")
      setNewBahasa("")
      setNewSubjek("")
      setNewStok("")
      setNewJenis("")
      setNewLokasi("")
      setNewDeskripsi("")
      // handleClose()
      // console.log(newJudulBuku);
  };

  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <br/><h1>Daftar Buku</h1>
      <div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Pengarang, Penerbit" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell style={{ width: 420 }}>Cover Buku</StyledTableCell>
            <StyledTableCell style={{ width: 460 }}>Judul Buku</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Pengarang</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Jenis</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Klasifikasi</StyledTableCell>
            <StyledTableCell style={{ width: 240 }}>Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
              <StyledTableCell style={{ height: 160 }}>
                {val.gambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={val.gambar} alt="Buku"/>}
              </StyledTableCell>
              <StyledTableCell>{val.judul_pustaka}</StyledTableCell>
              <StyledTableCell >{val.pengarang}</StyledTableCell>
              <StyledTableCell>{val.jenis}</StyledTableCell>
              <StyledTableCell>{val.klasifikasi}</StyledTableCell>
              <StyledTableCell>
                <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick={() => handleClickOpenEdit(val)}><FontAwesomeIcon icon={faPen} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick = {() => hapusBuku(val.id)}><FontAwesomeIcon icon={faTrash} size="2x" className="faTrash" style={{color: 'red'}}/></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    <br />
    </div>
    <Dialog open={openEdit} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Edit Detail Buku</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2">Judul Buku</label>
              <input 
                type="text" 
                name="judul_pustaka"
                id="judul_pustaka" 
                value={newJudulBuku}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewJudulBuku(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Pengarang</label>
              <input 
                type="text" 
                name="pengarang"
                id="pengarang"
                value={newPengarang}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPengarang(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Penerbit</label>
              <input 
                type="text" 
                name="penerbit"
                id="penerbit" 
                value={newPenerbit}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPenerbit(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Klasifikasi</label>
              <input 
                type="text" 
                name="klasifikasi"
                id="klasifikasi" 
                value={newKlasifikasi}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewKlasifikasi(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Tahun</label>
              <input 
                type="text" 
                name="tahun"
                id="tahun" 
                value={newTahun}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewTahun(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Bahasa</label>
              <input 
                type="text" 
                name="bahasa"
                id="bahasa" 
                value={newBahasa}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewBahasa(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Subjek</label>
              <input 
                type="text" 
                name="subjek"
                id="subjek" 
                value={newSubjek}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewSubjek(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Stok</label>
              <input 
                type="number" 
                name="stok"
                id="stok"
                value={newStok}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewStok(e.target.value)}}
              />
            </div>
            <div class="form-row">
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  id="jenis"
                  value={newJenis}
                  onChange={(e)=> {
                    setNewJenis(e.target.value)}}
                >
                  <option></option>
                  <option>Buku Bacaan</option>
                  <option>Tandon</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  id="lokasi"
                  value={newLokasi}
                  onChange={(e)=> {
                    setNewLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>

            <div class="form-row">
                <label class="col-md-2">Deskripsi</label>
                <textarea 
                  class="form-control col-md-6"
                  name="deskripsi"  
                  id="deskripsi"
                  value={newDeskripsi}
                  onChange={(e)=> {
                    setNewDeskripsi(e.target.value)}}
                />
            </div>
            <div class="form-row">
              <div class="col-md-2"></div>
              <div class="col-md-3">
              <Link to="/buku">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={editBuku}
                  type="submit"
                >
                  Simpan
                </Button>
              </Link>
              </div>
              <div class="col-md-3">
              <Link to="/buku">
                  <Button variant="contained" className={classes.button}>Batal</Button>
              </Link>
              </div>
            </div>
          </div>
        </form>
      </Dialog>
    <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail Buku</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Cover Buku</label>
            </div>
            <div class="form-row">
              <label class="col-md-4">
              {detailGambar == null ? <img className={classes.gambar} src={GambarBuku} alt="."/> : <img className={classes.gambar} src={detailGambar} alt="."/>}
              </label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-4">: {detailId}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul Buku</label>
              <label class="col-md-4">: {detailJudulBuku}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Bahasa</label>
              <label class="col-md-4">: {detailBahasa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-4">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pengarang</label>
              <label class="col-md-4">: {detailPengarang}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penerbit</label>
              <label class="col-md-4">: {detailPenerbit}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Deskripsi</label>
              <label class="col-md-4">: {detailDeskripsi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-4">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Edisi</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-4">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Stok</label>
              <label class="col-md-4">: {detailStok}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-4">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Copy/Original</label>
              <label class="col-md-4"></label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-4">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <button class="col-md-2" onClick={handleClose}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    <Footer />
    </>
  );
}

export default Buku;

