import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { faTrash, faPen, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import TableFooter from '@material-ui/core/TableFooter';
import LastPageIcon from '@material-ui/icons/LastPage';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Button, Dialog} from '@material-ui/core';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Table from '@material-ui/core/Table';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import Axios from 'axios';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});
const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const TA=() => {
  
  const [openEdit, setOpenEdit] = React.useState(false);
  const [openDetail, setOpenDetail] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.toLowerCase().search(value) !== -1;
  });

  setFilteredData(result);
  }
  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getTA = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getTaKpPa', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    getTA();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  //Detail TA
  const [detailJenis, setDetailJenis] = useState('')
  const [detailSubjek, setDetailSubjek] = useState('')
  const [detailJudulTaKpPa, setDetailJudulTaKpPa] = useState('')
  const [detailAbstrak, setDetailAbstrak] = useState('')
  const [detailPenulis, setDetailPenulis] = useState('')
  const [detailLokasi, setDetailLokasi] = useState('')
  const [detailTahun, setDetailTahun] = useState('')
  const [detailPenguji, setDetailPenguji] = useState('')
  const [detailPembimbing, setDetailPembimbing] = useState('')
  const [detailPembimbing2, setDetailPembimbing2] = useState('')
  const [detailKlasifikasi, setDetailKlasifikasi] = useState('')
  const [detailTingkat, setDetailTingkat] = useState('')
  const [detailStatus, setDetailStatus] = useState('')
  const [detailId, setDetailId] = useState('')

  const handleClickOpenDetail = (val) => {
    setDetailId(val.id_create)
    setDetailSubjek(val.subjek)
    setDetailJudulTaKpPa(val.judul_pustaka)
    setDetailKlasifikasi(val.klasifikasi)
    setDetailJenis(val.jenis)
    setDetailLokasi(val.lokasi)
    setDetailAbstrak(val.deskripsi)
    setDetailPenulis(val.pengarang)
    setDetailTahun(val.tahun)
    setDetailPenguji(val.penguji)
    setDetailPembimbing(val.pembimbing)
    setDetailPembimbing2(val.pembimbing2)
    setDetailTingkat(val.tingkat)
    setDetailStatus(val.status)
    setOpenDetail(true);
  }

  //UPDATE TA
  const [newPenulis, setNewPenulis] = useState('')
  const [newSubjek, setNewSubjek] = useState('')
  const [newJudulTA, setNewJudulTA] = useState('')
  const [newKlasifikasi, setNewKlasifikasi] = useState('')
  const [newTahun, setNewTahun] = useState('')
  const [newJenis, setNewJenis] = useState('')
  const [newLokasi, setNewLokasi] = useState('')
  const [idUpdate, setIdUpdate] = useState('')
  const [newPembimbing, setNewPembimbing] = useState('')
  const [newPembimbing2, setNewPembimbing2] = useState('')
  const [newPenguji, setNewPenguji] = useState('')
  const [newDeskripsi, setNewDeskripsi] = useState('')

  const handleClickOpenEdit = (val) => {
    setOpenEdit(true);
    setNewJudulTA(val.judul_pustaka)
    setNewTahun(val.tahun)
    setNewKlasifikasi(val.klasifikasi)
    setNewSubjek(val.subjek)
    setNewJenis(val.jenis)
    setNewLokasi(val.lokasi)
    setNewPembimbing(val.pembimbing)
    setNewPembimbing2(val.pembimbing2)
    setNewPenguji(val.penguji)
    setIdUpdate(val.id)
    console.log(val.lokasi)
  };

  const handleClose = () => {
    setOpenEdit(false);
    setOpenDetail(false);
  };

  const editTA = () => {
    Axios.put("http://localhost:8082/api/v1/bahanPustaka/editTaKpPa/" + idUpdate, {
        judul_pustaka: newJudulTA,
        subjek: newSubjek,
        klasifikasi: newKlasifikasi,
        tahun: newTahun,
        lokasi: newLokasi,
        jenis: newJenis,
        pembimbing: newPembimbing,
        pembimbing2: newPembimbing2,
        penguji: newPenguji,
        deskripsi: newDeskripsi
        
    }, {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response)=>{
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Behasil!",
          text: "Data ta/kp/pa telah di ubah",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Data ta/kp/pa gagal di ubah",
          type: "failed"
        });
      }
    });
    setNewJudulTA("")
    setNewTahun("")
    setNewKlasifikasi("")
    setNewSubjek("")
    setNewJenis("")
    setNewLokasi("")
    setNewPembimbing("")
    setNewPembimbing2("")
    setNewPenguji("")
    setNewDeskripsi("")
  };

  const hapusTA = (id) =>{
    console.log("ini : " + id);
    const newList = bukuList.filter((val) => val.id !== id);
    setBukuList(newList);
    Swal.fire({
      title: 'Apakah anda ingin menghapus data ini?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Ya`,
      denyButtonText: `Tidak`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const newList = bukuList.filter((val) => val.id !== id);
        setBukuList(newList);
        Axios.delete(`http://localhost:8082/api/v1/bahanPustaka/deleteBahanPustaka/${id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          Swal.fire('Berhasil menghapus data', '', 'success').then(
            window.location.reload()
          );
          
        });
        
      } else if (result.isDenied) {
        Swal.fire('Data tidak jadi di hapus', '', 'info')
      }
    })
  };

  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h1>Daftar TA/KP/PA</h1>
      <div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Jenis, Pembimbing" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Judul</StyledTableCell>
            <StyledTableCell>Jenis</StyledTableCell>
            <StyledTableCell >Pembimbing</StyledTableCell>
            <StyledTableCell >Tahun</StyledTableCell>
            <StyledTableCell >Status</StyledTableCell>
            <StyledTableCell >Alat</StyledTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {(rowsPerPage > 0
            ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : filteredData
          ).map((val) => (
            <StyledTableRow key={val.id}>
              <StyledTableCell component="th" scope="row">
                {val.judul_pustaka}
              </StyledTableCell>
              <StyledTableCell >{val.jenis}</StyledTableCell>
              <StyledTableCell >{val.pembimbing}</StyledTableCell>
              <StyledTableCell >{val.tahun}</StyledTableCell>
              <StyledTableCell >{val.status_pustaka}</StyledTableCell>
              <StyledTableCell>
                <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick={() => handleClickOpenEdit(val)}><FontAwesomeIcon icon={faPen} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                <Link to="#" onClick = {() => hapusTA(val.id)}><FontAwesomeIcon icon={faTrash} size="2x" className="faTrash" style={{color: 'red'}}/></Link>
              </StyledTableCell>
            </StyledTableRow>
          ))}
          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    <br />
    </div>
    <Dialog open={openEdit} onClose={handleClose}>
      <form>
        <div className="Edit-ta">
          <br/><center><h2>Edit Detail TA/KP/PA</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2">Judul Tugas</label>
              <input 
                type="text" 
                name="judul_pustaka"
                id="judul_pustaka" 
                class="form-control col-md-6"
                value={newJudulTA}
                onChange={(e)=> {
                  setNewJudulTA(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Klasifikasi</label>
              <input 
                type="text" 
                name="klasifikasi"
                id="klasifikasi" 
                value = {newKlasifikasi}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewKlasifikasi(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Tahun</label>
              <input 
                type="text" 
                name="tahun"
                id="tahun" 
                value={newTahun}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewTahun(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Subjek Tugas</label>
              <input 
                type="text" 
                name="subjek"
                id="subjek" 
                value={newSubjek}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewSubjek(e.target.value)}}
              />
            </div>
            <div class="form-row">
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  id="lokasi"
                  value={newLokasi}
                  onChange={(e)=> {
                    setNewLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  id="jenis"
                  value={newJenis}
                  onChange={(e)=> {
                    setNewJenis(e.target.value)}}
                >
                  <option></option>
                  <option>TA</option>
                  <option>KP</option>
                  <option>PA1</option>
                  <option>PA2</option>
                  <option>PA3</option>
                </select>
            </div>
            <div class="form-row">
              <label class="col-md-2">Penulis</label>
              <input 
                type="text" 
                name="penulis"
                id="penulis" 
                value={newPenulis}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPenulis(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Pembimbing 1</label>
              <input 
                type="text" 
                name="pembimbing"
                id="pembimbing" 
                value={newPembimbing}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPembimbing(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Pembimbing 2</label>
              <input 
                type="text" 
                name="pembimbing2"
                id="pembimbing2" 
                value={newPembimbing2}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPembimbing2(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Penguji</label>
              <input 
                type="text" 
                name="penguji"
                id="penguji" 
                value={newPenguji}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewPenguji(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Deskripsi</label>
              <input 
                type="text" 
                name="deskripsi"
                id="deskripsi" 
                value={newDeskripsi}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewDeskripsi(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <div class="col-md-2"></div>
              <div class="col-md-3">
                <Link to="/ta">
                  <Button 
                    variant="contained" 
                    color="success" 
                    className={classes.button}
                    onClick={editTA}
                  >
                    Simpan
                  </Button><br></br>
                </Link>
              </div>
              <div class="col-md-3">
                <Link to="/ta">
                    <Button variant="contained" className={classes.button}>Batal</Button>
                </Link>
              </div>
            </div>
          </div>
        </form>
      </Dialog>
      <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail TA/KP/PA</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">ID</label>
              <label class="col-md-9">: {detailId}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Judul</label>
              <label class="col-md-9">: {detailJudulTaKpPa}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jenis</label>
              <label class="col-md-9">: {detailJenis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Subjek</label>
              <label class="col-md-9">: {detailSubjek}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Abstrak</label>
              <label class="col-md-9">: {detailAbstrak}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penulis</label>
              <label class="col-md-9">: {detailPenulis}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tahun</label>
              <label class="col-md-9">: {detailTahun}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Lokasi</label>
              <label class="col-md-9">: {detailLokasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Penguji</label>
              <label class="col-md-9">: {detailPenguji}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 1</label>
              <label class="col-md-9">: {detailPembimbing}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Pembimbing 2</label>
              <label class="col-md-9">: {detailPembimbing2}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Klasifikasi</label>
              <label class="col-md-9">: {detailKlasifikasi}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Tingkat</label>
              <label class="col-md-9">: {detailTingkat}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Status</label>
              <label class="col-md-9">: {detailStatus}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <button class="col-md-2" onClick={handleClose}>Tutup</button>
            </div>
        </div>
      </form>
    </Dialog>
    <Footer />
    </>
  );
}

export default TA;