import Navbar from '../Components/Navbar/Navbar';
import Header from '../Components/Header/Header';
import Footer from '../Components/Footer/Footer';
import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import mainBanner from '../Components/Images/Background-Buku.png';


export default class BuatArtikel extends Component {
  
    state = {
        editorState: EditorState.createEmpty(),
    } 

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        })
        console.log(this.editorState);
    }
    render() {
      const {editorState} = this.state;
      return (
        <>
        <Header />
        <Navbar />
        <div style={{ backgroundImage: `url(${mainBanner})` }}>
          <br/><h1>Tambah Artikel Baru</h1>
          <form style={{
            maxWidth: 1200,
            marginLeft: 'auto',
            marginRight: 'auto',
            paddingLeft: '30px',
            paddingTop: '30px',
            border: '1px solid #c6c6c6',
            backgroundColor: 'white',
          }}>
            <div class="form-row">
                <label class="col-md-2">Judul Artikel</label>
                <input 
                  type="text" 
                  name="judul_buku" 
                  class="form-control col-md-9"
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">Deskripsi</label>
                <div 
                  className="Editor"
                  style={{width: '885px'}}
                >
                  <Editor
                    editorState={editorState}
                    toolbarClassName="toolbarClassName"
                    wrapperClassName="wrapperClassName"
                    editorClassName="editorClassName"
                    onEditorStateChange={this.onEditorStateChange}
                  />
                </div>
                {/* <div class="form-row">
                  <button type="submit"></button>
              </div> */}
            </div>
          </form>
        </div>
        <Footer />
    </>
  );
}
}