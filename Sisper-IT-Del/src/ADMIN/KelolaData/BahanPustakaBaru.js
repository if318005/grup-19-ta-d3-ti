import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Header from '../Components/Header/Header';
import Navbar from '../Components/Navbar/Navbar';
import './style.css';
import Axios from 'axios';
import Swal from 'sweetalert2';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../Components/Footer/Footer';
import mainBanner from '../Components/Images/Background-Buku.png';
//import * as Icons from 'images/icons';
//import { Editor } from 'react-draft-wysiwyg';


const useStyles = makeStyles({
  table: {
    marginLeft: '100px',
    marginRight: 'auto',
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  },
  input: {
    marginLeft: '30px',
  }
});
function BahanPustakaBaru() {

  const classes = useStyles();
  const [idCreate, setIdCreate] = useState('')
  const [toggleState, setToggleState] = useState(1);
  const idBuku = () => {
    var a = new Date();
    var year = a.getFullYear().toString().substr("-2");
    // var jumlah_id = jumlahId(year)
    // console.log("jumlah")
    Axios.get("http://localhost:8082/api/v1/bahanPustaka/getId/" + year, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      var jumlahLeadingZero = "000000000" + response.data.id;
      console.log("jumlah", response.data.id);
      var idCreate1 = year + "." + jumlahLeadingZero.substr(jumlahLeadingZero.length-5)
      console.log("id create", idCreate1)
      setIdCreate(idCreate1);
    })
    
  }


  // const jumlahId = (tahun) => {
  //   var jumlah1 = 0
    
  //   return jumlah1
  // }

  const toggleTab = (index) => {
    setToggleState(index);
  };

  //BUKU
  //const [id, setID] = useState('')
  const [judul_pustaka, setJudulPustaka] = useState('')
  const [bahasa, setBahasa] = useState('')
  const [subjek, setSubjek] = useState('')
  const [pengarang, setPengarang] = useState('')
  const [penerbit, setPenerbit] = useState('')
  const [deskripsi, setDeskripsi] = useState('')
  const [jenis, setJenis] = useState('')
  const [edisi, setEdisi] = useState('')
  const [isbn, setISBN] = useState('')
  const [klasifikasi, setKlasifikasi] = useState('')
  const [lokasi, setLokasi] = useState('')
  const [copyOriginal, setCopyOriginal] = useState('')
  const [tahun, setTahun] = useState('')
  //const [idMasterBuku, setIdMasterBuku] = useState('')
  const [tersedia, setTersedia] = useState('')
  const [foto, setFoto] = useState('')
  const [file, setFile] = useState('')

  //CD/DVD
  const [sumber, setSumber] = useState('')
  //const [idMasterCD, setIdMasterCD] = useState('')

  //TA/KP/PA
  const [penguji, setPenguji] = useState('')
  const [penguji2, setPenguji2] = useState('')
  const [pembimbing, setPembimbing] = useState('')
  const [pembimbing2, setPembimbing2] = useState('')
  const [prodi, setProdi] = useState('')
  const [tingkat, setTingkat] = useState('')

  //ARTIKEL
  const getBase64 = file => {
    return new Promise(resolve => {
      let baseURL = "";
      // Make new FileReader
      let reader = new FileReader();

      // Convert the file to base64 text
      reader.readAsDataURL(file);

      // on reader load somthing...
      reader.onload = () => {
        // Make a fileInfo Object
        // console.log("Called", reader);
        baseURL = reader.result;
        // console.log(baseURL);
        resolve(baseURL);
      };
      // console.log(fileInfo);
    });
  };
  const submitBuku = (e) => {
    e.preventDefault();
    if(foto){
      getBase64(foto)
      .then(result => {
        setFile(result);
      });
    }
    console.log("file =    " , file);
        Axios.post("http://localhost:8082/api/v1/bahanPustaka/addBuku", {
          id: idCreate,
          bahasa: bahasa,
          subjek: subjek, 
          edisi: edisi, 
          isbn: isbn, 
          judul_pustaka: judul_pustaka, 
          pengarang: pengarang,
          penerbit: penerbit, 
          klasifikasi: klasifikasi,
          tahun : tahun,
          jenis : jenis,
          lokasi : lokasi,  
          tersedia : tersedia,
          deskripsi : deskripsi,
          copyOriginal: copyOriginal, 
          gambar : file
        }, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          if(response.data.success){
            Swal.fire(
            {
                title: "Berhasil",
                text: "Data Buku berhasil ditambahkan",
                timer: 2000,
                icon: "success",
            })
            window.location.reload()
          }
        }
        );
  };
  const submitTa = (e) => {
    e.preventDefault()
      Axios.post("http://localhost:8082/api/v1/bahanPustaka/addTaKpPa", {
        id: idCreate,
        subjek: subjek, 
        judul_pustaka: judul_pustaka, 
        pengarang: pengarang,
        tahun : tahun,
        klasifikasi: klasifikasi,
        prodi : prodi,
        jenis : jenis,
        lokasi : lokasi,
        deskripsi : deskripsi,
        penguji : penguji,
        penguji2 : penguji2,
        tingkat : tingkat,
        pembimbing : pembimbing,
        pembimbing2 : pembimbing2
      }, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then((response) => {
        if(response.data.success){
          Swal.fire(
          {
              title: "Berhasil",
              text: "Data TA/KP/PA berhasil ditambahkan",
              timer: 2000,
              icon: "success",
          })
          window.location.reload()
        }
      }
      );
  };
  const submitCD = (e) => {
      e.preventDefault()
          Axios.post("http://localhost:8082/api/v1/bahanPustaka/addCdDvd", {
            id: idCreate,
            subjek: subjek, 
            judul_pustaka: judul_pustaka,
            klasifikasi: klasifikasi,
            isbn : isbn,
            tahun : tahun,
            jenis : jenis,
            prodi : prodi,
            lokasi : lokasi,
            deskripsi : deskripsi,
            copyOriginal : copyOriginal,
            sumber : sumber
          }).then((response) => {
            if(response.data.success){
              Swal.fire(
              {
                  title: "Berhasil",
                  text: "Data CD/DVD berhasil ditambahkan",
                  timer: 2000,
                  icon: "success",
              })
              window.location.reload()
            }
          }
          );
  };

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
    <br/> <br /> <h1>Bahan Pustaka Baru</h1>
    <div className="pustaka-baru">
      <div className="bloc-tabs1">
        <button
          className={toggleState === 1 ? "tabs1 active-tabs" : "tabs1"}
          onClick={() => {toggleTab(1); idBuku();}}
        >
          Tambah Buku
        </button>
        <button
          className={toggleState === 2 ? "tabs1 active-tabs" : "tabs1"}
          onClick={() => {toggleTab(2); idBuku();}}
        >
          Tambah TA/KP/PA
        </button>
        <button
          className={toggleState === 3 ? "tabs1 active-tabs" : "tabs1"}
          onClick={() => {toggleTab(3); idBuku();}}
        >
          Tambah CD/DVD
        </button>
        {/* <button
          className={toggleState === 4 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(4)}
        >
          Tambah Artikel
        </button> */}
        </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          
          <form onSubmit={submitBuku}>
          <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2" >Id</label>
                <input 
                  type="text" 
                  name="ID" 
                  class="form-control col-md-6"
                  value={idCreate}
                  disabled
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2" >Judul Buku</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setJudulPustaka(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Bahasa</label>
                <input 
                  type="text" 
                  name="bahasa" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setBahasa(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Subjek</label>
                <input 
                  type="text" 
                  name="subjek" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setSubjek(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Pengarang</label>
                <input 
                  type="text" 
                  name="pengarang" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPengarang(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Penerbit</label>
                <input 
                  type="text" 
                  name="penerbit" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPenerbit(e.target.value)}}
                />            
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Edisi</label>
                <input 
                  type="text" 
                  name="edisi" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setEdisi(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">ISBN</label>
                <input 
                  type="text" 
                  name="isbn" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setISBN(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Klasifikasi</label>
                <input 
                  type="text" 
                  name="klasifikasi" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setKlasifikasi(e.target.value)}}
                />            
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Tahun</label>
                <input 
                  type="text" 
                  name="tahun" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setTahun(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Copy/Original</label>
                <select 
                  class="form-control col-md-6"
                  name="copyOriginal" 
                  onChange={(e)=> {setCopyOriginal(e.target.value)}}
                >
                  <option></option>
                  <option>Copy</option>
                  <option>Original</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  onChange={(e)=> {setJenis(e.target.value)}}
                >
                  <option></option>
                  <option>Buku Bacaan</option>
                  <option>Tandon</option>
                </select>
            </div>
             <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  onChange={(e)=> {setLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Stok</label>
                <input 
                  type="number" 
                  name="tersedia" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setTersedia(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Deskripsi</label>
                <textarea 
                  class="form-control col-md-6"
                  name="deskripsi" 
                  onChange={(e)=> {setDeskripsi(e.target.value)}} 
                />
            </div> 
            <div class="form-row">
                <label class="col-md-2"/>
              <label class="col-md-2">Foto</label>
              <input
              type="file"
              name="foto"
              class="form-control col-md-6"
              onChange={(e)=> {setFoto(e.target.files[0])}}
              />
            </div>
            <div class="form-row">
                <text class="col-md-4"/>
                <button class="col-md-1" type="submit" className={classes.button}>Tambah</button>
                <button class="col-md-1" className={classes.button}>Batal</button>
            </div>
          </form>
          </div>
        
        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
          <form onSubmit={submitTa}>
          <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2" >Id</label>
                <input 
                  type="text" 
                  name="ID" 
                  class="form-control col-md-6"
                  value={idCreate}
                  disabled
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Judul Tugas</label><input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setJudulPustaka(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Subjek Tugas</label>
                <input 
                  type="text" 
                  name="subjek" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setSubjek(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Klasifikasi</label>
                <input 
                  type="text" 
                  name="klasifikasi" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setKlasifikasi(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Stok</label>
                <input 
                  type="number" 
                  name="tersedia" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setTersedia(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Tahun</label>
                <input 
                  type="text" 
                  name="tahun" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setTahun(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Prodi</label>
                <select 
                  class="form-control col-md-6"
                  name="prodi" 
                  onChange={(e)=> {setProdi(e.target.value)}}
                  >
                  <option></option>
                  <option>D3 Teknologi Informasi</option>
                  <option>D3 Teknologi Komputer</option>
                  <option>D4 Teknologi Rekayasa Perangkat Lunak</option>
                  <option>S1 Informatika</option>
                  <option>S1 Sistem Informasi</option>
                  <option>S1 Manajemen Rekayasa</option>
                  <option>S1 Teknik Elektro</option>
                  <option>S1 Bioproses</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Pembimbing 1</label>
                <input 
                  type="text" 
                  name="pembimbing" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPembimbing(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Pembimbing 2</label>
                <input 
                  type="text" 
                  name="pembimbing2" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPembimbing2(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Penguji</label>
                <input 
                  type="text" 
                  name="penguji" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPenguji(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Penguji 2</label>
                <input 
                  type="text" 
                  name="penguji2" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPenguji2(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  onChange={(e)=> {setJenis(e.target.value)}}
                  >
                  <option></option>
                  <option>TA</option>
                  <option>KP</option>
                  <option>PA1</option>
                  <option>PA2</option>
                  <option>PA3</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Tingkat</label>
                <select 
                  class="form-control col-md-6"
                  name="tingkat"
                  onChange={(e)=> {setTingkat(e.target.value)}}
                >
                  <option></option>
                  <option>Tingkat 1</option>
                  <option>Tingkat 2</option>
                  <option>Tingkat 3</option>
                  <option>Tingkat 4</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  onChange={(e)=> {setLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>
            {/* <div class="form-row">
                <label class="col-md-2">Dokumen</label>
                <input type="file" class="form-control col-md-6"/>
            </div> */}
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Abstrak</label>
                <textarea 
                  class="form-control col-md-6"
                  name="deskripsi" 
                  onChange={(e)=> {setDeskripsi(e.target.value)}}
                  />
            </div>
            <div class="form-row">
                <text class="col-md-4"/>
                <button class="col-md-1" type="submit" className={classes.button}>Tambah</button>
                <button class="col-md-1" type="submit" className={classes.button}>Batal</button>
            </div>
          </form>
        </div>
        
        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          <form onSubmit={submitCD}>
          <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2" >Id</label>
                <input 
                  type="text" 
                  name="ID" 
                  class="form-control col-md-6"
                  value={idCreate}
                  disabled
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Judul</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setJudulPustaka(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Subjek</label>
                <input 
                  type="text" 
                  name="subjek" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setSubjek(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Klasifikasi</label>
                <input 
                  type="text" 
                  name="klasifikasi" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setKlasifikasi(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">ISBN</label>
                <input 
                  type="text" 
                  name="isbn" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setISBN(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Tahun</label>
                <input 
                  type="text" 
                  name="tahun" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setTahun(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Jenis</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  onChange={(e)=> {setJenis(e.target.value)}}
                  >
                  <option></option>
                  <option>CD</option>
                  <option>DVD</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Prodi</label>
                <select 
                  class="form-control col-md-6"
                  name="prodi" 
                  onChange={(e)=> {setProdi(e.target.value)}}
                  >
                  <option></option>
                  <option>D3 Teknologi Informasi</option>
                  <option>D3 Teknologi Komputer</option>
                  <option>D4 Teknologi Rekayasa Perangkat Lunak</option>
                  <option>S1 Informatika</option>
                  <option>S1 Sistem Informasi</option>
                  <option>S1 Manajemen Rekayasa</option>
                  <option>S1 Teknik Elektro</option>
                  <option>S1 Bioproses</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Lokasi</label>
                <select 
                  class="form-control col-md-6"
                  name="lokasi" 
                  onChange={(e)=> {setLokasi(e.target.value)}}
                >
                  <option></option>
                  <option>Lantai 1</option>
                  <option>Lantai 2</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Sumber</label>
                <input 
                  type="text" 
                  name="sumber" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setSumber(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Copy/Original</label>
                <select 
                  class="form-control col-md-6"
                  name="copyOriginal" 
                  onChange={(e)=> {setCopyOriginal(e.target.value)}}
                >
                  <option></option>
                  <option>Copy</option>
                  <option>Original</option>
                </select>
            </div>
            {/* <div class="form-row">
                <label class="col-md-2">Gambar</label>
                <input type="file" class="form-control col-md-6"/>
            </div> */}
            <div class="form-row">
                <label class="col-md-2"/>
                <label class="col-md-2">Deskripsi</label>
                <textarea 
                  class="form-control col-md-6"
                  name="deskripsi"  
                  onChange={(e)=> {setDeskripsi(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <text class="col-md-4"/>
                <button class="col-md-1" type="submit" className={classes.button}>Tambah</button>
                <button class="col-md-1" type="submit" className={classes.button}>Batal</button>
            </div>
          </form>
          </div>
        
         <div
          className={toggleState === 4 ? "content  active-content" : "content"}
        >
          <form>
            <div class="form-row">
                <label class="col-md-2">Judul Artikel</label>
                <input type="text" class="form-control col-md-6"/>
            </div>
            <div class="form-row">
                <label class="col-md-2">Deskripsi</label>
                <textarea class="form-control col-md-6"/>
            </div>
            <div class="form-row">
                <text class="col-md-2"/>
                <button class="col-md-1" type="submit" className={classes.button}>Tambah</button>
                <button class="col-md-1" type="submit" className={classes.button}>Batal</button>
            </div>
          </form>
          </div>
        {/*
        <div
          className={toggleState === 6 ? "content  active-content" : "content"}
        >
          <form>
            <div class="form-row">
                <label class="col-md-2">Judul Pustaka</label>
                <input type="text" class="form-control col-md-6"/>
            </div>
            <div class="form-row">
                <label class="col-md-2">No</label>
                <input type="text" class="form-control col-md-6"/>
            </div>
            <div class="form-row">
                <text class="col-md-2"/>
                <button class="col-md-2" type="submit" className={classes.button}>Generate Barcode</button>
            </div>
          </form>
          </div> */}
      </div>
    </div>
    </div>
    <Footer />
    </>
  );
}

export default BahanPustakaBaru;
