import React from 'react';
import Pdf from "react-to-pdf";

//import { Button} from '@material-ui/core';

const ref = React.createRef();

const PemesananPDF = (props) => {
    // c
    // setListPemesanan(props.data)
    // console.log(listPemesanan)
    // const {pemesanan} = props.data
    console.log(Array.from(props.data))
  return (
    <>
      <div className="Laporan" ref={ref}>
        {/* <h1>{props.title}</h1>
        <img src={props.image} alt={props.title} /> */}
        {/* <p>{props.data}</p> */}
        {/* {pemesanan} */}
        <table border="1" cellPadding="5">
            <thead>
                <th>Nama</th>
                <th>Email</th>
                <th>Judul</th>
                <th>Jabatan</th>
                <th>Jurusan</th>
            </thead>
            <tbody>
                {Array.from(props.data).map((val) => (
                    <tr key={val.id}>
                        <td>{val.nama}</td>
                        <td>{val.email}</td>
                        <td>{val.judul_buku}</td>
                        <td>{val.jabatan}</td>
                        <td>{val.prodi}</td>
                    </tr>
                ))}
                
            </tbody>
        </table>
      </div>
      <Pdf targetRef={ref} filename="post.pdf" >
      {({ toPdf }) => <button onClick={toPdf}>Capture as PDF</button>}
      </Pdf>
    </>
  ); 
}

export default PemesananPDF;