import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Navbar from '../Components/Navbar/Navbar';
import { Button} from '@material-ui/core';
import './style.css';
import {Form, Row, Col} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import Axios from 'axios';
import PemesananPDF from './PemesananPDF'

// import {Link} from 'react-router-dom';


const useStyles = makeStyles({
  table: {
    marginLeft: '100px',
    marginRight: 'auto',
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

function Laporan() {
  const classes = useStyles();
  const [toggleState, setToggleState] = useState(1);
  const [bulanAwal, setBulanAwal] = useState('');
  const [tahunAwal, setTahunAwal] = useState('');
  const [bulanAkhir, setBulanAkhir] = useState('');
  const [tahunAkhir, setTahunAkhir] = useState('');
  //const [pengadaanSubmit, setPengadaanSubmit] = useState(false)
  const [pemesananSubmit, setPemesananSubmit] = useState(false)
  const [peminjamanSubmit, setPeminjamanSubmit] = useState(false)
  const [dendaSubmit, setDendaSubmit] = useState(false)
  const [listPemesanan, setListPemesanan] = useState('')
  const [listPeminjaman, setListPeminjaman] = useState('')
  const [listDenda, setListDenda] = useState('')

  const submitPeminjaman = (event) =>{
    event.preventDefault();
    setPeminjamanSubmit(true)
    event.preventDefault();
    // setPemesananSubmit(true);
    setBulanAwal(event.target[0].value)
    setTahunAwal(event.target[1].value)
    setBulanAkhir(event.target[2].value)
    setTahunAkhir(event.target[3].value)
    console.log(bulanAwal)
    Axios.post('http://localhost:8082/api/v1/transaksiBuku/laporanPeminjaman', {
      bulan_awal: bulanAwal,
      tahun_awal: tahunAwal,
      bulan_akhir: bulanAkhir,
      tahun_akhir: tahunAkhir
    },{
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then(response => {
      setListPeminjaman(response.data.data)
    })
  }

  const submitPemesanan = (event) =>{
    event.preventDefault();
    setPemesananSubmit(true);
    setBulanAwal(event.target[0].value)
    setTahunAwal(event.target[1].value)
    setBulanAkhir(event.target[2].value)
    setTahunAkhir(event.target[3].value)
    Axios.post('http://localhost:8082/api/v1/transaksiBuku/laporanPemesanan', {
      bulan_awal: bulanAwal,
      tahun_awal: tahunAwal,
      bulan_akhir: bulanAkhir,
      tahun_akhir: tahunAkhir
    },{
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then(response => {
      setListPemesanan(response.data.data)
    })
    
  }

  // const submitPengadaan = (event) =>{
  //   event.preventDefault();
  //   setPengadaanSubmit(true)
    
  // }

  const submitDenda = (event) =>{
    event.preventDefault();
    setDendaSubmit(true);
    event.preventDefault();
    // setPemesananSubmit(true);
    setBulanAwal(event.target[0].value)
    setTahunAwal(event.target[1].value)
    setBulanAkhir(event.target[2].value)
    setTahunAkhir(event.target[3].value)
    Axios.post('http://localhost:8082/api/v1/transaksiBuku/laporanDenda', {
      bulan_awal: bulanAwal,
      tahun_awal: tahunAwal,
      bulan_akhir: bulanAkhir,
      tahun_akhir: tahunAkhir
    },{
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then(response => {
      setListDenda(response.data.data)
    })
  }

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <>
    <Header />
    <Navbar />    
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br/> <br /><h1>Laporan</h1>
      <div className="laporan">
      <div className="bloc-tabs">
          <button
            className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(1)}
          >
            Pemesanan
          </button>
          {/* <button
            className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(2)}
          >
            Pengadaan
          </button> */}
          <button
            className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(3)}
          >
            Peminjaman
          </button>
          <button
            className={toggleState === 4 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(4)}
          >
            Denda
          </button>
          {/* <button
            className={toggleState === 5 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(5)}
          >
            Log Pengunjung
          </button>
          <button
            className={toggleState === 6 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(6)}
          >
            Bahan Pustaka
          </button> */}
        </div>

        <div className="content-tabs">
          <div
            className={toggleState === 1 ? "content  active-content" : "content"}
          >
          <form onSubmit={submitPemesanan}>
            <div class="form-row">
                <label class="col-md-2">Bulan Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAwal" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2">Tahun Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAwal" 
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
            </div>

            <div class="form-row">
                <label class="col-md-2">Bulan Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAkhir" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-2">Tahun Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAkhir"
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
            </div>

            <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
          </form>
          {pemesananSubmit ?
          (<PemesananPDF data={listPemesanan} />)
          :
          (
          <div>
          </div>
          )
        }
          </div>
          <div
            className={toggleState === 2 ? "content  active-content" : "content"}
          >
            <form>
              <div class="form-row">
                <label class="col-md-2">Bulan Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAwal" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAwal" 
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <div class="form-row">
                <label class="col-md-2">Bulan Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAkhir" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAkhir"
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Cetak Laporan</Button>
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
            </form>
          </div>

          <div
            className={toggleState === 3 ? "content  active-content" : "content"}
          >
            <form onSubmit={submitPeminjaman}>
              <div class="form-row">
                <label class="col-md-2">Bulan Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAwal" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAwal" 
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <div class="form-row">
                <label class="col-md-2">Bulan Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAkhir" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAkhir"
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Cetak Laporan</Button>
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
            </form>
            {peminjamanSubmit ?
          (<PemesananPDF data={listPeminjaman} />)
          :
          (
          <div>
          </div>
          )
        }
            
          </div>

          <div
            className={toggleState === 4 ? "content  active-content" : "content"}
          >
            <form onSubmit={submitDenda}>
              <div class="form-row">
                <label class="col-md-2">Bulan Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAwal" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAwal" 
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Bulan Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAkhir" 
                >
                  <option></option>
                  <option value="1">Januari</option>
                  <option value="2">Februari</option>
                  <option value="3">Maret</option>
                  <option value="4">April</option>
                  <option value="5">Mei</option>
                  <option value="6">Juni</option>
                  <option value="7">Juli</option>
                  <option value="8">Agustus</option>
                  <option value="9">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAkhir"
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Cetak Laporan</Button>
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
            </form>
            {dendaSubmit ?
          (<PemesananPDF data={listDenda} />)
          :
          (
          <div>
          </div>
          )
        }
          </div>

          <div
            className={toggleState === 5 ? "content  active-content" : "content"}
          >
            <form>
              <div class="form-row">
                <label class="col-md-2">Tanggal Awal</label>
                <select 
                  class="form-control col-md-6"
                  name="tanggalAwal" 
                >
                  <option></option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                  <option>11</option>
                  <option>12</option>
                  <option>13</option>
                  <option>14</option>
                  <option>15</option>
                  <option>16</option>
                  <option>17</option>
                  <option>18</option>
                  <option>19</option>
                  <option>20</option>
                  <option>21</option>
                  <option>22</option>
                  <option>23</option>
                  <option>24</option>
                  <option>25</option>
                  <option>26</option>
                  <option>27</option>
                  <option>28</option>
                  <option>29</option>
                  <option>30</option>
                  <option>31</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tanggal Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tanggalAkhir" 
                >
                  <option></option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                  <option>11</option>
                  <option>12</option>
                  <option>13</option>
                  <option>14</option>
                  <option>15</option>
                  <option>16</option>
                  <option>17</option>
                  <option>18</option>
                  <option>19</option>
                  <option>20</option>
                  <option>21</option>
                  <option>22</option>
                  <option>23</option>
                  <option>24</option>
                  <option>25</option>
                  <option>26</option>
                  <option>27</option>
                  <option>28</option>
                  <option>29</option>
                  <option>30</option>
                  <option>31</option>
                </select>
              </div>

              <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Cetak Laporan</Button>
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
            </form>
          </div>
          <div
            className={toggleState === 6 ? "content  active-content" : "content"}
          >
            <form>
              <div class="form-row">
                <label class="col-md-2">Jenis Bahan Pustaka</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAwal" 
                >
                  <option></option>
                  <option>Buku</option>
                  <option>CD</option>
                  <option>DVD</option>
                  <option>TA</option>
                  <option>KP</option>
                  <option>PA</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tanggal masuk</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAwal" 
                >
                  <option></option>
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                  <option>11</option>
                  <option>12</option>
                  <option>13</option>
                  <option>14</option>
                  <option>15</option>
                  <option>16</option>
                  <option>17</option>
                  <option>18</option>
                  <option>19</option>
                  <option>20</option>
                  <option>21</option>
                  <option>22</option>
                  <option>23</option>
                  <option>24</option>
                  <option>25</option>
                  <option>26</option>
                  <option>27</option>
                  <option>28</option>
                  <option>29</option>
                  <option>30</option>
                  <option>31</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Bulan Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="bulanAkhir" 
                >
                  <option></option>
                  <option>Januari</option>
                  <option>Februari</option>
                  <option>Maret</option>
                  <option>April</option>
                  <option>Mei</option>
                  <option>Juni</option>
                  <option>Juli</option>
                  <option>Agustus</option>
                  <option>September</option>
                  <option>Oktober</option>
                  <option>November</option>
                  <option>Desember</option>
                </select>
              </div>
              <div class="form-row">
                <label class="col-md-2">Tahun Akhir</label>
                <select 
                  class="form-control col-md-6"
                  name="tahunAkhir"
                >
                  <option></option>
                  <option>2010</option>
                  <option>2011</option>
                  <option>2012</option>
                  <option>2013</option>
                  <option>2014</option>
                  <option>2015</option>
                  <option>2016</option>
                  <option>2017</option>
                  <option>2018</option>
                  <option>2019</option>
                  <option>2020</option>
                  <option>2021</option>
                </select>
              </div>

              <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Cetak Laporan</Button>
                  <Button type="submit" className={classes.button}>Lihat Laporan</Button>
                </Col>
              </Form.Group>
            </form>
          </div>
        </div>
      </div>
    </div>
    <Footer />
    </>
  );
}

export default Laporan;
