import React, {useState, useEffect} from 'react';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import { Button, Dialog} from '@material-ui/core';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Header from '../Components/Header/Header';
import Navbar from '../Components/Navbar/Navbar';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Footer from '../Components/Footer/Footer';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faPen, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Axios from 'axios';
import Swal from 'sweetalert2';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  formTambah: {
    display: 'flex',
    flexDirection: 'column',
    alignItem: 'center',
    justifyContent: 'center',
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginBottom: '30px',
    marginTop: '50px',

  },
  form: {
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

function DataPengguna() {
  const classes = useStyles();
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

const [nama, setNama] = useState('')
const [username, setUsername] = useState('')
const [email, setEmail] = useState('')
const [alamat, setAlamat] = useState('')
const [prodi, setProdi] = useState('')
const [noHP, setNoHP] = useState('')
const [jabatan, setJabatan] = useState('')
const [nomorInduk, setNomorInduk] = useState('')
const [password, setPassword] = useState('')

const submitPengguna = (e) => {
  
  e.preventDefault()
    Axios.post("http://localhost:8082/api/v1/user/register", {
      name: nama, 
      username: username, 
      alamat: alamat, 
      nomorInduk: nomorInduk,
      prodi: prodi,
      noHP: noHP,
      jabatan: jabatan,
      email: email,
      password : password,
      id_role: jabatan
    }, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success){
        Swal.fire(
          {
              title: "Berhasil",
              text: "Data User berhasil ditambahkan",
              timer: 5000,
              icon: "success",
          })
          window.location.reload()
      }else{
        Swal.fire(
          {
              title: "Gagal",
              text: "Data User gagal ditambahkan",
              timer: 2000,
              icon: "failed",
          })
      }
    }
    );
  };

  // const submitPenggunaAdmin = (e) => {
  //   e.preventDefault()
  //     Axios.post("http://localhost:8082/api/v1/user/register", {
  //       name: nama, 
  //       username: username, 
  //       email: email,
  //       password : password,
  //       id_role: 1
  //     }, {
  //       headers: {
  //         Authorization: 'Bearer ' + localStorage.getItem("token")
  //       }
  //     }).then(Swal.fire(
  //       {
  //           title: "Berhasil",
  //           text: "Data Admin berhasil ditambahkan",
  //           timer: 2000,
  //           icon: "success",
  //       })
  //     );
  //   };
  const [openEdit, setOpenEdit] = React.useState(false);
  const [openDetail, setOpenDetail] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.id.search(value) !== -1;
  });

  setFilteredData(result);
  }
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getAnggota = () => {
      Axios.get('http://localhost:8082/api/v1/user/getAllUser', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          console.log(filteredData);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    getAnggota();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps
  const handleClickOpenEdit = (val) => {
    console.log(val);
    setNewNama(val.name)
    setNewEmail(val.email)
    setNewJurusan(val.Prodi)
    setNewAlamat(val.alamat)
    setNewNoHP(val.noHP)
    setNewJabatan(val.id_role)
    console.log(val.id_role)
    setIdUpdate(val.id)
    console.log("id adalah" ,idUpdate)
    setOpenEdit(true);
  };

  const handleClose = () => {
    setOpenEdit(false);
    setOpenDetail(false);
  };
  const hapusAnggota = (id) =>{
    // console.log("ini : " + id);
    Swal.fire({
      title: 'Apakah anda ingin menghapus data ini?',
      showCancelButton: true,
      confirmButtonText: `Ya`,
      denyButtonText: `Tidak`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const newList = bukuList.filter((val) => val.id !== id);
        setBukuList(newList);
        Axios.get(`http://localhost:8082/api/v1/user/hapusUser/${id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          Swal.fire('Berhasil menghapus data', '', 'success').then(
            window.location.reload()
          );
          
        });
        
      } else if (result.isDenied) {
        Swal.fire('Data tidak jadi di hapus', '', 'info')
      }
    })
    
  };

  //UPDATE Anggota
  //const [newNomorInduk, setNewNomorInduk] = useState('')
  const [newNama, setNewNama] = useState('')
  const [newEmail, setNewEmail] = useState('')
  const [newJurusan, setNewJurusan] = useState('')
  const [newAlamat, setNewAlamat] = useState('')
  const [newNoHP, setNewNoHP] = useState('')
  const [newJabatan, setNewJabatan] = useState('')
  const [idUpdate, setIdUpdate] = useState('')
  
  function editAnggota() {
    var updateurl = "http://localhost:8082/api/v1/user/editUser/"+ idUpdate;
    console.log(newNoHP)
    Axios.put(updateurl, {
        nama: newNama,
        email: newEmail,
        jurusan: newJurusan,
        alamat: newAlamat,
        noHP: newNoHP,
        jabatan: newJabatan        
    },
    {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Behasil!",
          text: "Data user telah di ubah",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Data user gagal di ubah",
          type: "failed"
        });
      }
    });
    setNewNama("")
    setNewEmail("")
    setNewJurusan("")
    setNewAlamat("")
    setNewNoHP("")
    setNewJabatan("")
};

//Detail Buku
const [detailNama, setDetailNama] = useState('')
const [detailEmail, setDetailEmail] = useState('')
const [detailJurusan, setDetailJurusan] = useState('')
const [detailAlamat, setDetailAlamat] = useState('')
const [detailJabatan, setDetailJabatan] = useState('')
const [detailNoHP, setDetailNoHP] = useState('')

const handleClickOpenDetail = (val) => {
  setDetailNama(val.name)
  setDetailEmail(val.email)
  setDetailJurusan(val.Prodi)
  setDetailAlamat(val.alamat)
  if(val.id_role === 1){
    setDetailJabatan("Administrator")
  }else if(val.id_role === 2){
    setDetailJabatan("Kaprodi")
  }else if(val.id_role === 3){
    setDetailJabatan("Dosen/Staff")
  }else{
    setDetailJabatan("Mahasiswa")
  }
  setDetailNoHP(val.noHP)
  setOpenDetail(true);
}

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
    <br/> <br /><h1>Akun User</h1>
    <div className="data-pengguna">
      <div className="bloc-tabs">
          <button
            className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(1)}
          >
            Tambah User
          </button>
          <button
            className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
            onClick={() => toggleTab(2)}
          >
            Daftar Akun User
          </button>
        </div>

        <div className="content-tabs">
          <div
            className={toggleState === 1 ? "content  active-content" : "content"}
          >
          <form onSubmit={submitPengguna}>
            <div class="form-row">
                <label class="col-md-4">Nomor Induk</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setNomorInduk(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">Nama</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setNama(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">Email</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setEmail(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">Jurusan</label>
                <select 
                  class="form-control col-md-6"
                  name="jenis" 
                  onChange={(e)=> {setProdi(e.target.value)}}
                >
                  <option></option>
                  <option>D3 Teknologi Informasi</option>
                  <option>D3 Teknologi Komputer</option>
                  <option>D4 Teknologi Rekayasa Perangkat Lunak</option>
                  <option>S1 Informasi</option>
                  <option>S1 Sistem Informasi</option>
                  <option>S1 Manajemen Rekayasa</option>
                  <option>S1 Teknik Elektro</option>
                  <option>S1 Bioproses</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-4">Alamat</label>
                <textarea 
                  class="form-control col-md-6"
                  name="deskripsi" 
                  onChange={(e)=> {setAlamat(e.target.value)}} 
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">No. HP</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setNoHP(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">Jabatan</label>
                <select 
                  class="form-control col-md-6"
                  name="jurusan" 
                  id="jurusan"
                  onChange={(e)=> {
                  setJabatan(e.target.value)}}
                >
                  <option></option>
                  <option value="1">Administrator</option>
                  <option value="2">Kaprodi</option>
                  <option value="3">Dosen/Staff</option>
                  <option value="4">Mahasiswa</option>
                </select>
            </div>
            <div class="form-row">
                <label class="col-md-4">Username</label>
                <input 
                  type="text" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setUsername(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-4">Password</label>
                <input 
                  type="password" 
                  name="judul_pustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setPassword(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <text class="col-md-4"/>
                <button class="col-md-1" type="submit" className={classes.button}>Tambah</button>
                {/* <button class="col-md-1" className={classes.button} onClick={()=> window.location.reload()}>Batal</button> */}
            </div>
          </form>
          </div>

          <div
            className={toggleState === 2 ? "content  active-content" : "content"}
          >
            <div>
              <div>
                <div className={classes.cari}>
                  <input 
                    class="search_search" 
                    type="search" 
                    placeholder="Nama, judul, jurusan" 
                    onChange={(event) =>handleSearch(event)}
                  />        
                  <input 
                    class="search_button" 
                    type="submit" 
                    value="Cari"
                  />
                </div>
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                      <TableRow>
                        <StyledTableCell>ID</StyledTableCell>
                        <StyledTableCell>Nama</StyledTableCell>
                        <StyledTableCell>Username</StyledTableCell>
                        <StyledTableCell>Email</StyledTableCell>
                        <StyledTableCell>Jabatan</StyledTableCell>
                        <StyledTableCell>Alat</StyledTableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {(rowsPerPage > 0
                        ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : filteredData
                      ).map((val) => (
                        <StyledTableRow key={val.id}>
                        <StyledTableCell>{val.nomorInduk}</StyledTableCell>
                          <StyledTableCell>{val.name}</StyledTableCell>
                          <StyledTableCell >{val.username}</StyledTableCell>
                          <StyledTableCell >{val.email}</StyledTableCell>
                          <StyledTableCell >
                            {val.id_role === 1 ? ("Administrator") : (val.id_role ===2) ? ("Kaprodi") : (val.id_role === 3) ? ("Dosen/Staff") : ("Mahasiswa")}
                            </StyledTableCell>
                          <StyledTableCell >
                            <Link to="#" onClick={() => handleClickOpenDetail(val)}><FontAwesomeIcon icon={faInfoCircle} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                            <Link to="#" onClick={() => handleClickOpenEdit(val)}><FontAwesomeIcon icon={faPen} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                            <Link to="#" onClick = {() => hapusAnggota(val.id)}><FontAwesomeIcon icon={faTrashAlt} size="2x" className="faTrash" style={{color: 'red'}}/></Link>
                        </StyledTableCell>
                        </StyledTableRow>
                      ))}
                      {emptyRows > 0 && (
                      <TableRow style={{ height: 53 * emptyRows }}>
                        <TableCell colSpan={6} />
                      </TableRow>
                    )}
                    </TableBody>
                  <TableFooter>
                    <TableRow>
                      <TablePagination
                        rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                        colSpan={7}
                        count={filteredData.length}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        SelectProps={{
                          inputProps: { 'aria-label': 'rows per page' },
                          native: true,
                        }}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                        ActionsComponent={TablePaginationActions}
                      />
                    </TableRow>
                  </TableFooter>
                  </Table>
                <br />
              </div>
            </div>
          </div>
            {/* <Form>
              <Form.Group as={Row} controlId="formHorizontalEmail">
                <Form.Label column sm={2}>
                  Nama Admin
                </Form.Label>
                <Col sm={10}>
                  <Form.Control type="text"/>
                </Col>
              </Form.Group> */}

              {/* <Form.Group as={Row} controlId="formHorizontalCheck">
                <Col sm={{ span: 10, offset: 2 }}>
                  <Form.Check label="Tambah Buku" />
                  </Col>
                  <Col sm={{ span: 10, offset: 2 }}>
                  <Form.Check label="Tambah CD/DVD" />
                  </Col>
                  <Col sm={{ span: 10, offset: 2 }}>
                  <Form.Check label="Tambah Anggota" />
                  </Col>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Form.Check label="Edit Buku" />
                  <Form.Check label="Edit CD/DVD" />
                  <Form.Check label="Edit Anggota" />
                </Col><Col sm={{ span: 10, offset: 2 }}>
                  <Form.Check label="Hapus Buku" />
                  <Form.Check label="Hapus CD/DVD" />
                  <Form.Check label="Hapus Anggota" />
                </Col>
              </Form.Group> */}

              {/* <Form.Group as={Row}>
                <Col sm={{ span: 10, offset: 2 }}>
                  <Button type="submit" className={classes.button}>Tambah</Button>
                </Col>
              </Form.Group>
            </Form> */}

          {/* <div
            className={toggleState === 3 ? "content  active-content" : "content"}
          >
            <Form>
              <Form.Group as={Row} controlId="formHorizontalPassword">
                  <Form.Label column sm={2}>
                    Awalan NIM/ID
                  </Form.Label>
                  <Col sm={10}>
                  <Form.Control type="text"/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalPassword">
                  <Form.Label column sm={2}>
                    NIM Awal
                  </Form.Label>
                  <Col sm={10}>
                  <Form.Control type="text"/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row} controlId="formHorizontalPassword">
                  <Form.Label column sm={2}>
                    NIM Akhir
                  </Form.Label>
                  <Col sm={10}>
                  <Form.Control type="text"/>
                  </Col>
                </Form.Group>

                <Form.Group as={Row}>
                  <Col sm={{ span: 10, offset: 2 }}>
                    <Button type="submit" className={classes.button}>Generate Data</Button>
                  </Col>
                </Form.Group>
              </Form>
          </div> */}
        </div>
        
        <Dialog open={openEdit} onClose={handleClose}>
              <form>
                  <div className="Edit-buku">
                    <br/><center><h2>Edit Detail Anggota</h2></center><br/>
                      <div class="form-row">
                        <label class="col-md-2">Nama</label>
                        <input 
                          type="text" 
                          name="nama"
                          id="nama" 
                          value={newNama}
                          class="form-control col-md-6"
                          onChange={(e)=> {
                            setNewNama(e.target.value)}}
                        />
                      </div>
                      <div class="form-row">
                        <label class="col-md-2">Email</label>
                        <input 
                          type="text" 
                          name="email"
                          id="email"
                          value={newEmail}
                          class="form-control col-md-6"
                          onChange={(e)=> {
                            setNewEmail(e.target.value)}}
                        />
                      </div>
                      <div class="form-row">
                          <label class="col-md-2">Jurusan</label>
                          <select 
                            class="form-control col-md-6"
                            name="jurusan" 
                            id="jurusan"
                            value={newJurusan}
                            onChange={(e)=> {
                              setNewJurusan(e.target.value)}}
                          >
                            <option></option>
                            <option>D3 Teknologi Informasi</option>
                            <option>D3 Teknologi Komputer</option>
                            <option>D4 Teknologi Rekayasa Perangkat Lunak</option>
                            <option>S1 Informatika</option>
                            <option>S1 Sistem Informasi</option>
                            <option>S1 Manajemen Rekayasa</option>
                            <option>S1 Teknik Elektro</option>
                            <option>S1 Bioproses</option>
                          </select>
                      </div>
                      <div class="form-row">
                        <label class="col-md-2">Alamat</label>
                        <textarea 
                            class="form-control col-md-6"
                            name="alamat"  
                            id="alamat"
                            value={newAlamat}
                            onChange={(e)=> {
                              setNewAlamat(e.target.value)}}
                          />
                      </div>
                      <div class="form-row">
                        <label class="col-md-2">No. HP</label>
                        <input 
                          type="text" 
                          name="noHP"
                          id="noHP" 
                          value={newNoHP}
                          class="form-control col-md-6"
                          onChange={(e)=> {
                            setNewNoHP(e.target.value)}}
                        />
                      </div>
                      <div class="form-row">
                        <label class="col-md-2">Jabatan</label>
                          <select 
                            class="form-control col-md-6"
                            name="jurusan" 
                            id="jurusan"
                            value={newJabatan}
                            onChange={(e)=> {
                              setNewJabatan(e.target.value)}}
                          >
                            <option></option>
                            <option value="1">Administrator</option>
                            <option value="2">Kaprodi</option>
                            <option value="3">Dosen/Staff</option>
                            <option value="4">Mahasiswa</option>
                          </select>
                      </div>
                      <div class="form-row">
                        <div class="col-md-2"></div>
                        <div class="col-md-3">
                          <Link to="/data-pengguna">
                            <Button 
                              variant="contained" 
                              color="success" 
                              className={classes.button}
                              type="submit"
                              onClick={editAnggota}
                            >
                              Simpan
                            </Button>
                          </Link>
                        </div>
                        <div class="col-md-3">
                        <Link to="/data-pengguna">
                            <Button variant="contained" onClick={handleClose} className={classes.button}>Batal</Button>
                        </Link>
                        </div>
                      </div>
                    </div>
                  </form>
                </Dialog>
        <Dialog open={openDetail} onClose={handleClose}>
      <form>
        <div className="Edit-buku">
          <br/><center><h2>Detail User</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Nama</label>
              <label class="col-md-4">: {detailNama}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Email</label>
              <label class="col-md-4">: {detailEmail}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jurusan</label>
              <label class="col-md-4">: {detailJurusan}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Alamat</label>
              <label class="col-md-4">: {detailAlamat}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">No. HP</label>
              <label class="col-md-4">: {detailNoHP}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2 font-weight-bold">Jabatan</label>
              <label class="col-md-4">: {detailJabatan}</label>
            </div>
            <div class="form-row">
              <label class="col-md-2"></label>
              <Button variant="contained" onClick={handleClose} className={classes.button}>Tutup</Button>
            </div>
        </div>
      </form>
    </Dialog>
      </div>
      </div>
      <Footer />
    </>
  );
}

export default DataPengguna;
