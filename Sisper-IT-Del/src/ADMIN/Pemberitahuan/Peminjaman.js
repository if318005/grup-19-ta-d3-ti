import React, {useState, useEffect, Fragment} from 'react';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import { faClipboardCheck, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import TableFooter from '@material-ui/core/TableFooter';
import LastPageIcon from '@material-ui/icons/LastPage';
import IconButton from '@material-ui/core/IconButton';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import Table from '@material-ui/core/Table';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import Axios from 'axios';
import Swal from 'sweetalert2';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  formTambah: {
    display: 'flex',
    flexDirection: 'column',
    alignItem: 'center',
    justifyContent: 'center',
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  form: {
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#061D47',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  },
  buttonPeminjaman: {
    backgroundColor: '#229644;',
    color: 'white',
    width:'250px',
    height: '50px',
    borderRadius: '10px',
    marginRight: '75px',
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const Peminjaman=() => {

  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.no_identitas.search(value) !== -1;
  });

  setFilteredData(result);
  }

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getPeminjaman = () => {
      Axios.get('http://localhost:8082/api/v1/transaksiBuku/getListPeminjaman', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    getPeminjaman();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const pengembalianBuku = (val) =>{
    var denda = 0
    var pengembalian = new Date(val.tanggal_pengembalian);
    var now = new Date(Date.now())
    var selisih = Math.ceil((now - pengembalian) / (1000 * 60 * 60 * 24))
    if(now > pengembalian){
      denda = (selisih - 1) * 2000
    }
    Swal.fire({
      title: 'Pengembalian buku dengan denda ' + denda,
      showDenyButton: true,
      confirmButtonText: `Ya`,
      denyButtonText: `Tidak`,
    }).then((result) => {
      if(result.isConfirmed){
        Axios.put(`http://localhost:8082/api/v1/transaksiBuku/pengembalianBuku/${val.id}`, {
          denda:denda
        },{
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          if(response.data.success){
            Swal.fire('Buku telah dikembalikan', '', 'success').then(
              window.location.reload()
            );
          }
        });
      }
    })
  }

  const perpanjanganPeminjaman = (id) =>{
    Swal.fire({
      title: 'Perpanjangan waktu peminjaman?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Ya`,
      denyButtonText: `Tidak`,
    }).then((result) => {
      if(result.isConfirmed){
        Axios.get(`http://localhost:8082/api/v1/transaksiBuku/perpanjanganPeminjaman/${id}`,{
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          if(response.data.success){
            Swal.fire('Waktu peminjaman di perpanjang', '', 'success').then(
              window.location.reload()
            );
          }
        });
      }
    })
  }


  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <br/><h1>Peminjaman</h1>
      <div class="form-row">
        <div class="col-md-6">
          <div className={classes.cari}>
            <input 
              class="search_search" 
              type="search" 
              placeholder=" ID peminjam" 
              onChange={(event) =>handleSearch(event)}
            />        
            <input 
              class="search_button" 
              type="submit" 
              value="Cari"
            />
          </div>
        </div>
        <div class="col-md-6" align="right">
          <Link to="/tambah-peminjaman"><button className={classes.buttonPeminjaman}>Tambah Peminjaman</button></Link>
        </div>
      </div>
      <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
            <StyledTableCell>No Induk</StyledTableCell>
              <StyledTableCell>Peminjam</StyledTableCell>
              <StyledTableCell>ID Bahan Pustaka</StyledTableCell>
              <StyledTableCell>Judul Bahan Pustaka</StyledTableCell>
              <StyledTableCell>Tanggal Pengembalian</StyledTableCell>
              <StyledTableCell>Alat</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : filteredData
            ).map((val) => (
              <StyledTableRow key={val.id}>
                <StyledTableCell>{val.no_identitas}</StyledTableCell>
                <StyledTableCell>{val.nama}</StyledTableCell>
                <StyledTableCell>{val.id_buku}</StyledTableCell>
                <StyledTableCell>{val.judul_buku}</StyledTableCell>
                <StyledTableCell>{val.tanggal_pengembalian}</StyledTableCell>
                <StyledTableCell>
                {val.status === 2? (<Link to="#">
                    <FontAwesomeIcon icon={faPlusSquare} onClick={() => perpanjanganPeminjaman(val.id)} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} />
                    </Link>) :
                    (<p></p>)
                  }
                  <Link to="#" onClick={() => {pengembalianBuku(val)}}><FontAwesomeIcon icon={faClipboardCheck} size="2x" className="faTrash" style={{color: 'green'}}/></Link>
              </StyledTableCell>
              </StyledTableRow>
            ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={6}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
        </Table>
    <br />
    </div>
    <Footer />
    </>
  );
}

export default Peminjaman;