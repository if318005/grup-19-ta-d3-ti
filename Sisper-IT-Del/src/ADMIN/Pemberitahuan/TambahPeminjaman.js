import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Header from '../Components/Header/Header';
import Navbar from '../Components/Navbar/Navbar';
import Axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import mainBanner from '../Components/Images/Background-Buku.png';
import Footer from '../Components/Footer/Footer';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';


const useStyles = makeStyles({
  table: {
    width: 1150,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: '50px',
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  },
  input: {
    marginLeft: '30px',
  }
});

function TambahPeminjaman() {
  const classes = useStyles();
  const [userList, setUserList] = useState('')
  const [bukuList, setBukuList] = useState('')
  const [kosong, setKosong] = useState(true)

  useEffect(()=> {
    const getAnggota = () => {
      Axios.get('http://localhost:8082/api/v1/user/getAllUser', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
          setUserList(response.data)
        }).catch(error => {
          console.log('Error getting fake data: ' + error);
        })
    };

    const getBuku = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data)
      }).catch(error => {
        console.log('Error getting fake data: ' + error);
      })
    }
    // console.log(typeof(bukuList))
    getBuku();
    getAnggota();
    setKosong(false)
  }, []);
  
  //PEMINJAM
  const [nomorAnggota, setNomorAnggota] = useState('')
  const [idBahanPustaka1, setIdBahanPustaka1] = useState('')
  const [idBahanPustaka2, setIdBahanPustaka2] = useState('')
  const [idBahanPustaka3, setIdBahanPustaka3] = useState('')
  const [judulBahanPustaka1, setJudulBahanPustaka1] = useState('')
  const [judulBahanPustaka2, setJudulBahanPustaka2] = useState('')
  const [judulBahanPustaka3, setJudulBahanPustaka3] = useState('')
  const [idBuku1, setIdBuku1] = useState('')
  const [idBuku2, setIdBuku2] = useState('')
  const [idBuku3, setIdBuku3] = useState('')
  const [errorSubmit, setErrorSubmit] = useState('')

  const submitPeminjaman = (e) => {
    e.preventDefault()
    // console.log(bukuList)
    // console.log("coba", bukuList.some(e => e.id_create === "123"))
    setErrorSubmit('')
    if(!kosong){
      if(nomorAnggota !== ''){
        if(!userList.some(e => e.nomorInduk === nomorAnggota)){
          setErrorSubmit("Nomor anggota tidak valid")
        }
      }else{
        setErrorSubmit("Nomor anggota tidak boleh kosong")
      }
      if(idBahanPustaka1 !== ''){
        if(!bukuList.some(e => e.id_create === idBahanPustaka1)){
          setErrorSubmit("ID Bahan Pustaka 1 tidak valid")
        }else{
          if(bukuList.find(buku => buku.id_create ===idBahanPustaka1).tersedia < 1){
            setErrorSubmit("ID " + bukuList.find(buku => buku.id_create === idBahanPustaka1).id_create + " tidak tersedia")
          }else{
            setJudulBahanPustaka1(bukuList.find(buku => buku.id_create === idBahanPustaka1).judul_pustaka)
            setIdBuku1(bukuList.find(buku => buku.id_create === idBahanPustaka1).id)
          }
        }
      }else{
        setErrorSubmit("Isi bahan pustaka 1");
      }
      if(idBahanPustaka2 !== ''){
        if(!bukuList.some(e => e.id_create === idBahanPustaka2)){
          setErrorSubmit("ID Bahan Pustaka 2 tidak valid")
        }else{
          if(bukuList.find(buku => buku.tersedia <= 0)){
            setErrorSubmit("ID " + bukuList.find(buku => buku.id_create === idBahanPustaka2).id_create + " tidak tersedia")
          }else{
            setJudulBahanPustaka2(bukuList.find(buku => buku.id_create === idBahanPustaka2).judul_pustaka)
            setIdBuku2(bukuList.find(buku => buku.id_create === idBahanPustaka2).id)
          }
        }
      }
      if(idBahanPustaka3 !== ''){
        if(!bukuList.some(e => e.id_create === idBahanPustaka3)){
          setErrorSubmit("ID Bahan Pustaka 3 tidak valid")
        }else{
          if(bukuList.find(buku => buku.tersedia <= 0)){
            setErrorSubmit("ID " + bukuList.find(buku => buku.id_create === idBahanPustaka3).id_create + " tidak tersedia")
          }else{
            setJudulBahanPustaka3(bukuList.find(buku => buku.id_create === idBahanPustaka3).judul_pustaka)
            setIdBuku3(bukuList.find(buku => buku.id_create === idBahanPustaka3).id)
          }
        }
      }
      if(errorSubmit !== ''){
        Swal.fire(
        {
            title: "Gagal",
            text: `${errorSubmit}`,
            icon: "warning"
        })
        setErrorSubmit('')
      }else{
        Axios.post('http://localhost:8082/api/v1/transaksiBuku/tambahPeminjamanAdmin', {
        nomorInduk: nomorAnggota,
        idBahanPustaka1: idBahanPustaka1, 
        idBahanPustaka2: idBahanPustaka2, 
        idBahanPustaka3: idBahanPustaka3,
        judulPustaka1: judulBahanPustaka1,
        judulPustaka2: judulBahanPustaka2,
        judulPustaka3: judulBahanPustaka3,
        id_buku1: idBuku1,
        id_buku2: idBuku2,
        id_buku3: idBuku3
      },{
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      })
      .then((response) => {
        Swal.fire(
          {
              title: "Berhasil",
              text: "Data peminjam berhasil dibuat",
              timer: 2000,
              icon: "success",
          })
          window.location.reload()
      }
        );
      }
    }
    
    
    //   console.log('Bearer ' + localStorage.getItem("token"));

    };

  return (
    <>
    <Header />
    <Navbar />
        <div style={{ backgroundImage: `url(${mainBanner})` }}>
            <br />
            <center><h3>Data Peminjaman Baru</h3></center>
          <form className={classes.table} onSubmit={submitPeminjaman}>
            <div class="form-row">
                <label class="col-md-2">Nomor Anggota</label>
                <input 
                  type="text" 
                  name="judul_buku" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setNomorAnggota(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">ID Bahan Pustaka 1</label>
                <input 
                  type="text" 
                  name="idBahanPustaka" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setIdBahanPustaka1(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">ID Bahan Pustaka 2</label>
                <input 
                  type="text" 
                  name="idBahanPustaka2" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setIdBahanPustaka2(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <label class="col-md-2">ID Bahan Pustaka 3</label>
                <input 
                  type="text" 
                  name="idBahanPustaka3" 
                  class="form-control col-md-6"
                  onChange={(e)=> {setIdBahanPustaka3(e.target.value)}}
                />
            </div>
            <div class="form-row">
                <text class="col-md-2"/>
                <button class="col-md-1" className={classes.button} type="submit">Tambah</button>
                <Link to="/peminjaman"><button class="col-md-1" className={classes.button}>Batal</button></Link>
            </div>
          </form>
        </div>
      <Footer />
    </>
  );
}

export default TambahPeminjaman;
