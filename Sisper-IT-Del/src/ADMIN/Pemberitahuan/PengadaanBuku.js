import React, {useState, useEffect, Fragment} from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle, faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Swal from 'sweetalert2';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  formTambah: {
    display: 'flex',
    flexDirection: 'column',
    alignItem: 'center',
    justifyContent: 'center',
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  form: {
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});


const PengadaanBuku=() => {
  
  const [open, setOpen] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_buku.search(value) !== -1;
  });

  setFilteredData(result);
  }

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])
  // const [pengadaan, setPengadaan] = useState([])

  useEffect(()=> {
    // const getPengadaanBukuId = () => {
    //   Axios.get('http://localhost:8082/api/v1/pengadaanBuku/getPengadaanBuku/$id', {
    //     headers: {
    //       Authorization: 'Bearer ' + localStorage.getItem("token")
    //     }
    //   }).then((response)=> {
    //     setPengadaan(response.data);
    //     console.log(pengadaan);
    //   }, [])
    // };
    const getPengadaanBuku = () => {
      Axios.get('http://localhost:8082/api/v1/pengadaanBuku/getPengadaanBuku', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    getPengadaanBuku();
    // getPengadaanBukuId();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps

  const tolakPengadaan = (id) =>{
    // console.log("ini : " + id);
    Swal.fire({
      title: 'Apakah anda ingin menolak pengadaaan  buku ini?',
      showCancelButton: true,
      confirmButtonText: `Ya`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const newList = bukuList.filter((val) => val.id !== id);
        setBukuList(newList);
        Axios.get(`http://localhost:8082/api/v1/pengadaanBuku/setujuPengadaan/${id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          if(response.data.success){
            Swal.fire('Pengadaan ditolak', '', 'success').then(
              window.location.reload()
            );
          }
        });
        
      } else if (result.isDenied) {
        
      }
    })
    
  };

  const setujuPengadaan = (id) =>{
    // console.log("ini : " + id);
    Swal.fire({
      title: 'Apakah anda menyetujui pengadaaan  buku ini?',
      showCancelButton: true,
      confirmButtonText: `Ya`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const newList = bukuList.filter((val) => val.id !== id);
        setBukuList(newList);
        Axios.get(`http://localhost:8082/api/v1/pengadaanBuku/setujuPengadaan/${id}`,{
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          if(response.data.success){
            Swal.fire('Pengadaan disetujui', '', 'success').then(
              window.location.reload()
            );
          }
        });
        
      } else if (result.isDenied) {
        
      }
    })
    
  };

  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <h1>Pengadaan Buku</h1>
      <div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder=" ID, Judul, Pengarang, Penerbit" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell></StyledTableCell>
              <StyledTableCell align="right">Jenis</StyledTableCell>
              <StyledTableCell align="right">Judul Pustaka</StyledTableCell>
              <StyledTableCell align="right">Pengarang</StyledTableCell>
              <StyledTableCell align="right">Tanggal Pengajuan</StyledTableCell>
              <StyledTableCell align="right">Aksi</StyledTableCell>
            </TableRow>
          </TableHead>
          {filteredData.map((val) => (
          <TableBody>
              <StyledTableRow key={val.id}>
                <StyledTableCell>
                  <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                    {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                  </IconButton>
                  </StyledTableCell>
                  <StyledTableCell align="right">
                    {val.status === 0 ? (<div>Menunggu persetujuan</div>) : (val.status === 3) ? (<div>Ditolak</div>) : (<div>Diterima</div>) }
                  </StyledTableCell>
                  <StyledTableCell align="right">{val.judul_buku}</StyledTableCell>
                  <StyledTableCell align="right">{val.pengarang}</StyledTableCell>
                  <StyledTableCell align="right">{val.updated_at}</StyledTableCell>
                  <StyledTableCell align="right">
                    {val.status === 0 ? (<div><Link to="#" onClick={() => setujuPengadaan(val.id)}><FontAwesomeIcon icon={faCheckCircle} size="2x" className="faPen" style={{color: 'green', marginRight: '20px'}} /></Link>
                    <Link to="#" onClick={() => tolakPengadaan(val.id)}><FontAwesomeIcon icon={faTimesCircle} size="2x" className="faTrash" style={{color: 'red'}}/></Link></div>) : <div></div>}
                </StyledTableCell>
              </StyledTableRow>
              <StyledTableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                  <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box margin={1}>
                      <Typography variant="h6" gutterBottom component="div">
                        Detail Pengajuan Buku
                      </Typography>
                      <Table size="small" aria-label="purchases">
                        <TableHead>
                          <TableRow key={val.id}>
                            <TableCell>Pengarang</TableCell>
                            <TableCell>{val.pengarang}</TableCell>
                          </TableRow>
                          <TableRow key={val.id}>
                            <TableCell>Judul Buku</TableCell>
                            <TableCell>{val.judul_buku}</TableCell>
                          </TableRow>
                          <TableRow key={val.id}>
                            <TableCell>Pengarang</TableCell>
                            <TableCell>{val.pengarang}</TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell>Tanggal Pengajuan</TableCell>
                            <TableCell>{val.updated_at}</TableCell>
                          </TableRow>
                        </TableHead>
                      </Table>
                    </Box>
                  </Collapse>
                </TableCell>
              </StyledTableRow>
          </TableBody>
          ))}
        </Table>
    <br />
    </div>
    <Footer />
    </>
  );
}

export default PengadaanBuku;