import React, {useState, useEffect, Fragment} from 'react';
import { withStyles, makeStyles, useTheme} from '@material-ui/core/styles';
import { Button, Dialog} from '@material-ui/core';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Axios from 'axios';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faPen } from "@fortawesome/free-solid-svg-icons";
import Swal from 'sweetalert2';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';


const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#061D47',
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  formTambah: {
    display: 'flex',
    flexDirection: 'column',
    alignItem: 'center',
    justifyContent: 'center',
  }
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    maxWidth: 1200,
    marginLeft: 'auto',
    marginRight: 'auto',
    border: '1px solid #c6c6c6',
    backgroundColor: 'white',
  },
  cari: {
    marginLeft: '80px',
    marginBottom: '30px',

  },
  form: {
    width: '600px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  button: {
    backgroundColor: '#061D47',
    marginRight: '10px',
    color: 'white',
    width:'150px',
    height: '35px',
    borderRadius: '50px',
  }
});

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const Anggota=() => {

  const [openEdit, setOpenEdit] = React.useState(false);
  const [allData,setAllData] = useState([]);
  const [filteredData,setFilteredData] = useState(allData);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filteredData.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleSearch = (event) => {
    let value = event.target.value.toLowerCase();
    let result = [];
    console.log(value);
    result = allData.filter((data) => {
    return data.judul_pustaka.search(value) !== -1;
  });

  setFilteredData(result);
  }

  const classes = useStyles();
  const [bukuList, setBukuList] = useState([])

  useEffect(()=> {
    const getAnggota = () => {
      Axios.get('http://localhost:8082/api/v1/bahanPustaka/getBahanPustaka', {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem("token")
        }
      }).then(response => {
        setBukuList(response.data);
        console.log(bukuList);
          setAllData(response.data);
          setFilteredData(response.data);
          })
          .catch(error => {
              console.log('Error getting fake data: ' + error);
              })
    };
    getAnggota();
  }, []);// eslint-disable-line react-hooks/exhaustive-deps
  const handleClickOpenEdit = (val) => {
    console.log(val);
    setNewNama(val.nama)
    setNewEmail(val.email)
    setNewJurusan(val.jurusan)
    setNewAlamat(val.alamat)
    setNewNoHP(val.noHP)
    setNewBatasBuku(val.batasBuku)
    setNewJabatan(val.jabatan)
    setIdUpdate(val.id)
    // console.log("adalah ",newDeskripsi)
    console.log("id adalah" ,idUpdate)
    setOpenEdit(true);
  };

  const handleClose = () => {
    setOpenEdit(false);
  };
  const hapusAnggota = (id) =>{
    // console.log("ini : " + id);
    Swal.fire({
      title: 'Apakah anda ingin menghapus data ini?',
      showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Ya`,
      denyButtonText: `Tidak`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        const newList = bukuList.filter((val) => val.id !== id);
        setBukuList(newList);
        Axios.delete(`http://localhost:8082/api/v1/bahanPustaka/deleteBahanPustaka/${id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem("token")
          }
        }).then((response) => {
          Swal.fire('Berhasil menghapus data', '', 'success').then(
            window.location.reload()
          );
          
        });
        
      } else if (result.isDenied) {
        Swal.fire('Data tidak jadi di hapus', '', 'info')
      }
    })
    
  };

  //UPDATE Anggota
  //const [newNomorInduk, setNewNomorInduk] = useState('')
  const [newNama, setNewNama] = useState('')
  const [newEmail, setNewEmail] = useState('')
  const [newJurusan, setNewJurusan] = useState('')
  const [newAlamat, setNewAlamat] = useState('')
  const [newNoHP, setNewNoHP] = useState('')
  const [newBatasBuku, setNewBatasBuku] = useState('')
  const [newJabatan, setNewJabatan] = useState('')
  const [idUpdate, setIdUpdate] = useState('')
  
  function editAnggota() {
    var updateurl = "http://localhost:8082/api/v1/bahanPustaka/editBuku/"+ idUpdate;
    console.log("Asdf   : ", updateurl);
    Axios.put(updateurl, {
        nama: newNama,
        email: newEmail,
        jurusan: newJurusan,
        alamat: newAlamat,
        noHP: newNoHP,
        batasBuku: newBatasBuku,
        jabatan: newJabatan        
    },
    {
      headers : {
        Authorization: 'Bearer ' + localStorage.getItem("token")
      }
    }).then((response) => {
      if(response.data.success === true){
        handleClose();
        Swal.fire({
          title: "Behasil!",
          text: "Data buku telah di ubah",
          type: "success"
        }).then(function() {
            window.location.reload();
        });
      }else{
        handleClose();
        Swal.fire({
          title: "Gagal",
          text: "Data buku gagal di ubah",
          type: "failed"
        });
      }
    });
    setNewNama("")
    setNewEmail("")
    setNewJurusan("")
    setNewAlamat("")
    setNewNoHP("")
    setNewBatasBuku("")
    setNewJabatan("")
};

  return (
    <>
    <Header />
    <Navbar />
    <div className="section-cari" style={{ backgroundImage: `url(${mainBanner})` }}>
      <h1>Anggota</h1>
      <div className={classes.cari}>
        <input 
          class="search_search" 
          type="search" 
          placeholder="Nama, judul, jurusan" 
          onChange={(event) =>handleSearch(event)}
        />        
        <input 
          class="search_button" 
          type="submit" 
          value="Cari"
        />
      </div>
      <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Nama</StyledTableCell>
              <StyledTableCell >Status</StyledTableCell>
              <StyledTableCell >Email</StyledTableCell>
              <StyledTableCell >Batas Buku</StyledTableCell>
              <StyledTableCell >Jabatan</StyledTableCell>
              <StyledTableCell >Jurusan</StyledTableCell>
              <StyledTableCell >Alat</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? filteredData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : filteredData
            ).map((val) => (
              <StyledTableRow key={val.id}>
                <StyledTableCell></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell ></StyledTableCell>
                <StyledTableCell >
                  <Link to="#" onClick={() => handleClickOpenEdit(val)}><FontAwesomeIcon icon={faPen} size="2x" className="faPen" style={{color: 'blue', marginRight: '20px' }} /></Link>
                  <Link to="#" onClick = {() => hapusAnggota(val.id)}><FontAwesomeIcon icon={faTrashAlt} size="2x" className="faTrash" style={{color: 'red'}}/></Link>
              </StyledTableCell>
              </StyledTableRow>
            ))}
            {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
          </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={7}
              count={filteredData.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
        </Table>
    <br />
    </div>
    <Dialog open={openEdit} onClose={handleClose}>
    <form>
        <div className="Edit-buku">
          <br/><center><h2>Edit Detail Anggota</h2></center><br/>
            <div class="form-row">
              <label class="col-md-2">Nama</label>
              <input 
                type="text" 
                name="nama"
                id="nama" 
                value={newNama}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewNama(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Email</label>
              <input 
                type="text" 
                name="email"
                id="email"
                value={newEmail}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewEmail(e.target.value)}}
              />
            </div>
            <div class="form-row">
                <label class="col-md-2">Jurusan</label>
                <select 
                  class="form-control col-md-6"
                  name="jurusan" 
                  id="jurusan"
                  value={newJurusan}
                  onChange={(e)=> {
                    setNewJurusan(e.target.value)}}
                >
                  <option></option>
                  <option>D3 Teknologi Informasi</option>
                  <option>D3 Teknologi Komputer</option>
                  <option>D4 Teknologi Rekayasa Perangkat Lunak</option>
                  <option>S1 Informatika</option>
                  <option>S1 Sistem Informasi</option>
                  <option>S1 Manajemen Rekayasa</option>
                  <option>S1 Teknik Elektro</option>
                  <option>S1 Bioproses</option>
                </select>
            </div>
            <div class="form-row">
              <label class="col-md-2">Alamat</label>
              <textarea 
                  class="form-control col-md-6"
                  name="alamat"  
                  id="alamat"
                  value={newAlamat}
                  onChange={(e)=> {
                    setNewAlamat(e.target.value)}}
                />
            </div>
            <div class="form-row">
              <label class="col-md-2">No. HP</label>
              <input 
                type="text" 
                name="noHP"
                id="noHP" 
                value={newNoHP}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewNoHP(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Batas Buku</label>
              <input 
                type="text" 
                name="batasBuku"
                id="batasBuku" 
                value={newBatasBuku}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewBatasBuku(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <label class="col-md-2">Jabatan</label>
              <input 
                type="text" 
                name="jabatan"
                id="jabatan" 
                value={newJabatan}
                class="form-control col-md-6"
                onChange={(e)=> {
                  setNewJabatan(e.target.value)}}
              />
            </div>
            <div class="form-row">
              <div class="col-md-2"></div>
              <div class="col-md-3">
              <Link to="/anggota">
                <Button 
                  variant="contained" 
                  color="success" 
                  className={classes.button}
                  onClick={editAnggota}
                  type="submit"
                >
                  Simpan
                </Button>
              </Link>
              </div>
              <div class="col-md-3">
              <Link to="/anggota">
                  <Button variant="contained" className={classes.button}>Batal</Button>
              </Link>
              </div>
            </div>
          </div>
        </form>
    </Dialog>
    <Footer />
    </>
  );
}

export default Anggota;
