import React from 'react';
import './Footer.css';

function Footer() {
  return (
    <footer>
      <div class="container pt-5 border-bottom">
        <div class="row">
          <div class="col-md-3 col-sm-12 mb-3">
          <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <img src="images/Logo-Sisper.png" alt="." width="40%"/>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
              <p style={{color: "white"}}>Sistem Perpustakaan Institut Teknologi Del (SisPer)</p>
              </li>
            </ul>
          </div>
          <div class="col-md-9 col-sm-12">

            <div class="col-md-4 col-sm-6 col-6 p-0 float-left mb-4">
              <h5 class="mb-4 font-weight-bold text-uppercase">JAM BUKA</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a href="/" style={{color: 'white'}}>Senin - Jumat :</a></li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a href="/" style={{color: 'white'}}>08.00 - 21.00</a></li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a href="/" style={{color: 'white'}}>Sabtu :</a></li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2"><a href="/" style={{color: 'white'}}>08.00 - 12.00</a></li>
            </ul>
            </div>

            <div class="col-md-4 col-sm-6 col-6 p-0 mb-4 float-left">
              <h5 class="mb-4 font-weight-bold text-uppercase">WEB LINKS</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="http://akademik.del.ac.id/" style={{color: 'white'}}>Web Akademik</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="https://mail.del.ac.id/" style={{color: 'white'}}>Web Mail</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="https://www.del.ac.id/" style={{color: 'white'}}>Web IT Del</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="http://students.del.ac.id/" style={{color: 'white'}}>Web Students</a>
              </li>
            </ul>
            </div>

            <div class="col-md-4 col-sm-6 col-6 mb-4 p-0 float-left">
              <h5 class="mb-4 font-weight-bold text-uppercase">HUBUNGI KAMI</h5>
            <ul class="list-group">
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="tel:+62 632 331234" style={{color: 'white'}}><i/> Panggilan Telepon</a>
              </li>
              <li class="list-group-item bg-transparent border-0 p-0 mb-2">
                <a href="mailto:info@del.ac.id" style={{color: 'white'}}><i/> Email</a>
              </li>
            </ul>
            </div>
          </div>
        </div>
      </div>
            <div class="col-md-12" id="copyright">
              <div class="py-4 d-flex justify-content-center align-items-center">
              <p id="copyright">Copyright SDI-IT Del © 2020 Institut Teknologi Del. All Rights Reserved</p>
              </div>
            </div>
    </footer>
  );
}

export default Footer;
