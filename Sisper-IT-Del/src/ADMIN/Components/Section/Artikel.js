import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';

const useStyles = makeStyles({
  card: {
    display: 'flex',
    background: '#061D47',
    color: 'white',

  },cardright: {
    display: 'flex',
    background: 'white',
    color: 'black',

  },
  cardDetails: {
    flex: 1,
  },
});

export default function Artikel(props) {
  const classes = useStyles();

  return (
    <div>
        <center><h3>ARTIKEL</h3></center>
        <hr />
        <Grid xs={12}>
        <CardActionArea class="col-md-6">
            <Card className={classes.card}>
            <div className={classes.cardDetails}>
                <CardContent>
                <Typography component="h2" variant="h5">
                FINALIS IMAINE CUP 2014
                </Typography><br />
                <Typography variant="subtitle1" paragraph>
                Saat ini, Imagine Cup chapter Indonesia telah memasuki babak final. Microsoft telah mengumumkan nama peserta yang berhasil mencapai final dari kategori World Citizenship, Innovation, dan Games...
                </Typography><br />
                <Typography variant="subtitle1" color="primary" href="#">
                    Continue reading...
                </Typography>
                </CardContent>
            </div>
            </Card>
        </CardActionArea>
        <CardActionArea class="col-md-6">
            <Card className={classes.cardright}>
            <div className={classes.cardDetails}>
                <CardContent>
                <Typography component="h2" variant="h5">
                19 OKTOBER DITETAPKAN APPLE UNTUK PERINGATAN STEVE JOBS
                </Typography><br />
                <Typography variant="subtitle1" paragraph>
                Siapa saja yang bekerja untuk perusahaan besar Apple pasti merasa sangat kehilangan atas kematian salah satu orang nomor satu dunia...
                </Typography><br />
                <Typography variant="subtitle1" color="primary" href="#">
                    Continue reading...
                </Typography>
                </CardContent>
            </div>
            </Card>
        </CardActionArea>
        </Grid>
    </div>
  );
}

Artikel.propTypes = {
  post: PropTypes.object,
};