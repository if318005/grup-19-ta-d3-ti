import React from 'react';
import './Cards.css';
import CardItem from './CardItem';
import mainBanner from '../Images/Background-Buku.png';

function Cards() {

  return (
    <div className='cards' style={{ backgroundImage: `url(${mainBanner})` }}>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
                src='images/Buku.png'
                text='Kelola Bahan Pustaka'
                path='/bahan-pustaka-baru'
            />
            <CardItem
                  src='images/Data.png'
                  text='Kelola Pengguna'
                  path='/data-pengguna'
            />
            <CardItem
                src='images/Laporan.png'
                text='Kelola Laporan'
                path='/laporan'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
                src='images/Peminjaman.png'
                text='Kelola Peminjaman'
                path='/peminjaman'
            />
            <CardItem
                  src='images/Pemesanan.png'
                  text='Kelola Pemesanan'
                  path='/pemesanan'
            />
            <CardItem
                src='images/Pengadaan.png'
                text='Kelola Pengadaan'
                path='/pengadaan'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;