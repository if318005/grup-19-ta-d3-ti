import Navbar from '../Navbar/Navbar';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import React, { Component } from "react";
import { Editor } from "react-draft-wysiwyg";
import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

export default class BuatArtikel extends Component {
    state = {
        editorState: EditorState.createEmpty(),
    } 

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        })
    }
    render() {
      const {editorState} = this.state;
      return (
        <>
        <Header />
        <Navbar />
        <h1>Artikel</h1>
        <div 
          className="Editor"
          style={{width: '900px', marginLeft: '20%', marginTop: '50px'}}
        >
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            onEditorStateChange={this.onEditorStateChange}
          />;
        </div>
        <Footer />
    </>
  );
}
}