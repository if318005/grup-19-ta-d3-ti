import React from 'react';
import { Link } from 'react-router-dom';

function CardItem(props) {
  return (
    <>
      <li className='cards__item'>
        <div className='cards__item__link'>
          <figure className='cards__item__pic-wrap' data-category={props.label}>
            <div><img src={props.src} alt='sembarang' className='cards__item__img'/></div>
          </figure>
          <div className='cards__item__info'>
            <h5 className='cards__item__text'>{props.text}</h5>
          </div>
          <div className='cards__item__keterangan'>
            <h6 className='cards__item__catatan'>{props.h6}</h6>
          </div>
          <div className='cards__item__action'>
            <Link to={props.path}><button className='cards__item__button'>Lihat</button></Link>
          </div>
          </div>
      </li>
    </>
  );
}

export default CardItem;