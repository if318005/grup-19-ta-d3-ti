export const BahanPustaka = [
  {
    title: 'Buku',
    path: '/buku',
    cName: 'dropdown-link'
  },
  {
    title: 'CD/DVD',
    path: '/cd',
    cName: 'dropdown-link'
  },
  {
    title: 'TA/KP/PA',
    path: '/ta',
    cName: 'dropdown-link'
  }
];
export const KelolaData = [
  {
    title: 'Bahan Pustaka Baru',
    path: 'bahan-pustaka-baru',
    cName: 'dropdown-link'
  },
  // {
  //   title: 'Artikel Baru',
  //   path: '/artikelBaru',
  //   cName: 'dropdown-link'
  // },
  {
    title: 'Akun User',
    path: '/data-pengguna',
    cName: 'dropdown-link'
  },
  {
    title: 'Laporan',
    path: '/laporan',
    cName: 'dropdown-link'
  }
];
export const TentangPerpus = [
  {
    title: 'Tentang Perpustakaan',
    path: '/sejarah-perpustakaan',
    cName: 'dropdown-link'
  },
  {
    title: 'Panduan Penggunaan',
    path: '/panduan-pengguna',
    cName: 'dropdown-link'
  },
  {
    title: 'Panduan Pesan Pinjam',
    path: '/panduan-pesan-pinjam',
    cName: 'dropdown-link'
  },
  {
    title: 'Peraturan Perpustakaan',
    path: '/peraturan-perpustakaan',
    cName: 'dropdown-link'
  },
  {
    title: 'Galeri',
    path: '/galeri',
    cName: 'dropdown-link'
  }
];
export const Pemberitahuan = [
  {
    title: localStorage.getItem("username"),
    path: '/beranda',
    cName: 'dropdown-link'
  },
  {
    title: 'Peminjaman',
    path: '/peminjaman',
    cName: 'dropdown-link'
  },
  {
    title: 'Pemesanan',
    path: '/pemesanan',
    cName: 'dropdown-link'
  },
  {
    title: 'Pengadaan',
    path: '/pengadaan',
    cName: 'dropdown-link'
  },
  // {
  //   title: 'Sejarah Peminjaman',
  //   path: '/sejarah-peminjaman',
  //   cName: 'dropdown-link'
  // },
  // {
  //   title: 'Rating Buku',
  //   path: '/rating-buku',
  //   cName: 'dropdown-link'
  // }
];