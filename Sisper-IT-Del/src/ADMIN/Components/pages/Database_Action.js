import { API, setAuthToken } from 'app/config/api';

export const GET_BUKU_KAS = "GET_BUKU_KAS";
export const ADD_BUKU_KAS = "ADD_BUKU_KAS";
export const DEL_BUKU_KAS = "DEL_BUKU_KAS";
export const UPD_BUKU_KAS = "UPD_BUKU_KAS";
export const DETAIL_BUKU_KAS = "DETAIL_BUKU_KAS";

export const getBukuKas = () => {
	return async (dispatch) => {
		const token = await localStorage.getItem("jwt_token");
		setAuthToken(token);
		const res = await API.get("/user/bukukas/list").catch((err) => {
			return Promise.reject(err);
		});
		if (res.data.code === 0) {
			dispatch({
				type: GET_BUKU_KAS,
				payload: res.data.data,
			});
		} else {
			dispatch({
				type: GET_BUKU_KAS,
				payload: [],
			});
		}
	};
};

export const addBukuKas = (params) => {
	return async (dispatch) => {
		const token = await localStorage.getItem("jwt_token");
		setAuthToken(token);
		const res = await API.post("/user/bukukas/create", params).catch((err) => {
			return Promise.reject(err);
		});
		return res.data;
	};
};

export const delBukuKas = (params) => {
	return async (dispatch) => {
		const token = await localStorage.getItem("jwt_token");
		setAuthToken(token);
		const res = await API.post(`/user/bukukas/delete`, params).catch((err) => {
			return Promise.reject(err);
		});
		if (res.data.code === 0) {
			dispatch({
				type: DEL_BUKU_KAS,
				payload: params,
			});
		}
		return res.data;
	};
};

export const updBukuKas = (params) => {
	return async (dispatch) => {
		const token = await localStorage.getItem("jwt_token");
		setAuthToken(token);
		const res = await API.post(`/user/bukukas/edit`, params).catch((err) => {
			return Promise.reject(err);
		});
		if (res.data.code === 0) {
			dispatch({
				type: UPD_BUKU_KAS,
				payload: params,
			});
		}
		return res.data;
	};
};

export const getDetailBukuKas = (id) => {
	return async (dispatch) => {
		const token = await localStorage.getItem("jwt_token");
		setAuthToken(token);
		const res = await API.get(`/user/bukukas/${id}/detail`).catch((err) => {
			return Promise.reject(err);
		});
		if (res.data.code === 0) {
			dispatch({
				type: DETAIL_BUKU_KAS,
				payload: res.data.data,
			});
		}
		return res.data;
	};
};

export const getOptionBukuKas = async () => {
	const token = await localStorage.getItem("jwt_token");
	setAuthToken(token);
	const res = await API.get(`/user/bukukas/all`).catch((err) => {
		return Promise.reject(err);
	});
	return res.data.data;
};
