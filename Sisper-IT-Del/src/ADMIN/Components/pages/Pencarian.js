import React, {useState} from 'react';
import './Pencarian.css';

function Tabs() {
  const [toggleState, setToggleState] = useState(1);

  const toggleTab = (index) => {
    setToggleState(index);
  };

  return (
    <div className="container_pencarian">
      <div className="bloc-tabs">
        <button
          className={toggleState === 1 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(1)}
        >
          BUKU
        </button>
        <button
          className={toggleState === 2 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(2)}
        >
          TA/KP
        </button>
        <button
          className={toggleState === 3 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(3)}
        >
          CD/DVD
        </button>
        <button
          className={toggleState === 4 ? "tabs active-tabs" : "tabs"}
          onClick={() => toggleTab(4)}
        >
          BAHAN PUSTAKA BARU
        </button>
      </div>

      <div className="content-tabs">
        <div
          className={toggleState === 1 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input class="search_search" type="search" placeholder=" Judul, Pengarang, ID, Penerbit" />        
            <input class="search_button" type="submit" value="Cari"/>
          </div>
        </div>

        <div
          className={toggleState === 2 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input class="search_search" type="search" placeholder=" Judul, Pengarang, ID, Penerbit" />        
            <input class="search_button" type="submit" value="Cari"/>
          </div>
        </div>

        <div
          className={toggleState === 3 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input class="search_search" type="search" placeholder=" Judul, Pengarang, ID, Penerbit" />        
            <input class="search_button" type="submit" value="Cari"/>
          </div>
        </div>

        <div
          className={toggleState === 4 ? "content  active-content" : "content"}
        >
          <div className="search">
            <input class="search_search" type="search" placeholder=" Judul, Pengarang, ID, Penerbit" />        
            <input class="search_button" type="submit" value="Cari"/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Tabs;