import React from 'react';
import Navbar from '../Components/Navbar/Navbar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Footer from '../Components/Footer/Footer';
import Header from '../Components/Header/Header';
import mainBanner from '../Components/Images/Background-Buku.png';


const useStyles = makeStyles({
  Details: {
    flex: 1,
    background: 'white',
    margin: '10px 60px 10px 60px',
    padding: '20px',
  },
});

function PeraturanPerpustakaan() {
  const classes = useStyles();

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h3><center>Peraturan Perpustakaan</center></h3> <br/>
      <Grid item xs={12} md={12}>
              <div className={classes.Details}>
                  <Typography variant="subtitle1" paragraph>
                  <text>
                      Berikut ini garis-garis besar tata tertib di Perpustakaan IT Del, yaitu:<br />

                      Anggota perpustakaan IT Del terdiri dari:<br />
                      1. Mahasiswa IT Del<br />
                      2. Dosen dan Staf IT Del<br />
                      3. Karyawan Del<br />
                      4. Keluarga Dosen, Staf, dan Karyawan Del<br /><br />

                      <b>A. Waktu operasional perpustakaan</b><br/>
                        - Senin-Jumat: 08:00-21:00 WIB<br/>
                        - Sabtu: 08:00-12:00 WIB<br/><br/>

                        <b>B. Kunjungan</b><br/>
                        1. Untuk alasan keamanan, pengunjung tidak diperbolehkan membawa tas, folder atau jaket ke dalam perpustakaan. 
                        Barang-barang tersebut harus disimpan dalam loker yang disediakan. 
                        Setiap kerusakan atau kehilangan barang-barang pribadi menjadi risiko pemiliknya.<br/>

                        2. Perpustakaan IT Del pada dasarnya adalah tempat untuk belajar. 
                        Untuk alasan ini, semua pengunjung harus tetap tenang sepanjang waktu di dalam perpustakaan dan menghormati orang lain yang sedang belajar. 
                        Selain itu, perpustakaan harus tetap dijaga kebersihan dan kerapiannya oleh semua pengunjungnya.<br/><br/>


                        <b>C. Aturan meminjam buku dari perpustakaan</b><br/>
                        1. Prosedur:<br/>
                        a. Anggota harus membawa dan memberikan kartu keanggotaan mereka kepada petugas perpustakaan saat akan melakukan transaksi peminjaman buku. 
                        Kartu anggota akan dikembalikan sesaat setelah proses transaksi selesai dilakukan.<br/>
                        b. Setelah seseorang diberikan hak meminjam buku, maka setiap kerusakan atau kehilangan buku akan menjadi tanggungjawab si peminjam. 
                        Peminjam akan dikenai denda apabila mengembalikan buku melewati tanggal pengembalian yang seharusnya.<br/>
                        c.Sistem informasi akan me-reminder peminjam sehari sebelum tanggal pengembalian buku<br/><br/>
                        

                        2. Jumlah buku yang dapat dipinjam:<br/>
                            a. Untuk mahasiswa tingkat 1 dan 2 : maksimal 4 buku<br/>
                            b. Mahasiswa tingkat III, mahasiswa yang sedang mengerjakan Tugas Akhir dan staf : maksimal 5 buku.<br/>
                            c. Peminjaman buku oleh dosen, maksimal 7 buku.<br/><br/>

                        3. Jangka waktu peminjaman:<br/>
                            a) Mahasiswa : 7 hari. Jika tidak ada reservasi untuk buku yang dipinjam, maka si peminjam dapat memperpanjang pinjaman untuk 7 hari berikutnya<br/>
                            b) Dosen dan Staf:14 hari. Dengan perpanjangan peminjaman untuk 14 hari selanjutnya<br/>
                            c) Untuk buku referensi utama (Bahasa Inggris), masa peminjaman oleh mahasiswa dan dosen bidang bahasa inggris adalah 1 semester.<br/>
                            d) Untuk referensi utama bidang IT, hanya dosen IT (sesuai bidang mata kuliah yang diajarkannya) dapat meminjam buku selama 1 semester.<br/><br/>

                        4. Bahan yang tidak dapat dibawa keluar dari perpustakaan: 
                        Buku-buku yang diperlukan untuk Proyek Akhir, Tugas Akhir, Laporan Kerja Praktek, majalah, jurnal, kamus, ensiklopedi dan peta tidak dapat dibawa keluar dari perpustakaan. 
                        Bahan-bahan pustaka ini tidak dapat dibawa keluar dari perpustakaan kapanpun, namun dapat dipergunakan di dalam perpustakaan atau untuk dicopy.<br/><br/>

                        5. Pengunjung diluar IT Del tidak diizinkan untuk meminjam buku dengan kondisi apapun. Namun, mereka dapat menggunakan buku-buku di dalam perpustakaan dan menggunakan komputer untuk mengakses internet di perpustakaan apabila tidak diperlukan oleh civitas IT Del.<br/><br/>

                        6. Semua buku yang telah dibawa keluar dari perpustakaan harus dikembalikan sebelum masa libur, selama masa libur, buku-buku tidak dapat dipinjam dari perpustakaan.<br/>
                        </text>
                  </Typography>
              </div>
        </Grid>
      </div>
      <Footer />
    </>
  );
}

export default PeraturanPerpustakaan;
