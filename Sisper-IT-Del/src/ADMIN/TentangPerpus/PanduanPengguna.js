import React from 'react';
import Navbar from '../Components/Navbar/Navbar';
import Footer from '../Components/Footer/Footer';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Header from '../Components/Header/Header';
import Step1 from '../Components/Images/Step1.jpg';
import Step2 from '../Components/Images/Step2.jpg';
import Step3 from '../Components/Images/Step3.jpg';
import Step5 from '../Components/Images/Step5.jpg';
import Step6 from '../Components/Images/Step6.jpg';
import Step7 from '../Components/Images/Step7.jpg';
import Step8 from '../Components/Images/Step8.jpg';
import Step9 from '../Components/Images/Step9.jpg';
import Step10 from '../Components/Images/Step10.jpg';
import Step11 from '../Components/Images/Step11.jpg';
import Step12 from '../Components/Images/Step12.jpg';
import Step13 from '../Components/Images/Step13.jpg';
import Step14 from '../Components/Images/Step14.jpg';
import Step15 from '../Components/Images/Step15.jpg';
import mainBanner from '../Components/Images/Background-Buku.png';

const useStyles = makeStyles({
  panduanDetails: {
    flex: 1,
    background: 'white',
    margin: '10px 60px 10px 60px',
    padding: '20px',
  },
});

function PanduanPengguna() {
  const classes = useStyles();

  return (
    <>
    <Header />
    <Navbar />
    <div style={{ backgroundImage: `url(${mainBanner})` }}>
      <br /><h3><center>Panduan Pengguna</center></h3>
      <Grid item xs={12} md={12}>
              <div className={classes.panduanDetails}>
                  <Typography variant="subtitle1" paragraph>
                  <text>
                        Sistem Informasi Perpustakaan IT Del ini dibangun untuk mempermudah peminjaman barang perpustakaan. 
                        Ada beberapa langkah yang harus diikuti dalam menjalankan Sistem Informasi Perpustakaan IT Del ini.
                        Cara penggunaan Sistem Informasi Perpustakaan IT Del ini adalah sebagai berikut:<br/><br/>

                        1. Pertama kali anda membuka Sistem Informasi Perpustakaan IT Del ini yang muncul adalah halaman login<br/>
                        
                        <center><img src={Step15} alt="Step1" width="600px" height="200px"/></center><br /><br />

                        2. Tampilan halaman Beranda pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step1} alt="Step2" width="600px" height="200px"/></center><br /><br />

                        3. Pengunjung dapat melihat pengumuman dengan klik menu pemberitahuan kemudian pilih Bahan Pustaka Baru<br/>

                        <center><img src={Step2} alt="Step3" width="600px" height="200px"/></center><br /><br />

                        4. Halaman Buku pada Sistem Informasi Perpustakaan IT Del ini dapat melakukan pencarian buku berdasarkan Judul buku<br/>

                        <center><img src={Step3} alt="Step7" width="600px" height="200px"/></center><br /><br />

                        5. Halaman CD/DVD pada Sistem Informasi Perpustakaan IT Del ini dapat melakukan pencarian CD/DVD berdasarkan Judul CD<br/>

                        <center><img src={Step5} alt="Step8" width="600px" height="200px"/></center><br /><br />

                        6. Halaman TA/KP/PA pada Sistem Informasi Perpustakaan IT Del ini dapat melakukan pencarian TA/KP/PA berdasarkan Judul TA/KP/PA<br/>

                        <center><img src={Step6} alt="Step9" width="600px" height="200px"/></center><br /><br />

                        7. Halaman Tentang Perpus: Sejarah Perpustakaan pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step7} alt="Step10" width="600px" height="200px"/></center><br /><br />

                        8. Halaman Tentang Perpus: Layanan Perpustakaan pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step8} alt="Step11" width="600px" height="200px"/></center><br /><br />

                        9. Halaman Tentang Perpus: Pencapaian Perpustakaan pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step9} alt="Step12" width="600px" height="200px"/></center><br /><br />

                        10. Halaman Tentang Perpus: Peraturan Perpustakaan pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step10} alt="Step13" width="600px" height="200px"/></center><br /><br />

                        11. Halaman Tentang Perpus: Galeri Perpustakaan pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step11} alt="Step14" width="600px" height="200px"/></center><br /><br />

                        12. Halaman Informasi Akun Anda: Informasi Bahan Pustaka pada Sistem Informasi Perpustakaan IT Del<br/>
                      
                        <center><img src={Step12} alt="Step15" width="300px" height="150px"/></center><br /><br />

                        13. Halaman Informasi Akun Anda: Notifikasi Admin pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step13} alt="Step15" width="300px" height="150px"/></center><br /><br />

                        14. Halaman Informasi Akun Anda: Ganti Password pada Sistem Informasi Perpustakaan IT Del<br/>

                        <center><img src={Step14} alt="Step15" width="300px" height="150px"/></center><br /><br />

                      </text>
                  </Typography>
              </div>
        </Grid>
      </div>
      <Footer />
    </>
  );
}

export default PanduanPengguna;
