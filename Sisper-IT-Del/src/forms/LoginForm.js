import React, {useState} from 'react';
import { useHistory } from "react-router-dom";
import backgroundForm from "../USERS/Components/Images/BgLogin.png";
import './style.css'
import Axios from 'axios';
import Footer from '../ADMIN/Components/Footer/Footer';

export default function LoginForm() {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')

    const handleuseStateChangeUsername = (event) => (
        setUsername(event.target.value)
    );
    const handleuseStateChangePassword = (event) => (
        setPassword(event.target.value)
    );
    const history = useHistory();
    const handleuseStateSubmit = (event) => {
        event.preventDefault();
        Axios.post("http://localhost:8082/authenticate", {
            username : username,
            password : password
        }).then(response => {
            if(response.data.token){
                localStorage.setItem("token", response.data.token);
                console.log(response.data);
                localStorage.setItem("login", true);
                localStorage.setItem("role", response.data.role)
                localStorage.setItem("username", response.data.username)
                if(response.data.role.includes("ROLE_ADMIN")){
                    history.push("/beranda")
                }else if(response.data.role.includes("ROLE_USER")){
                    history.push("/beranda-user")
                }
            }
        })
    };
    
  return (
    <>
    <div className="main">
      <div className="row justify-content-center">
            <img src="images/BannerLogin.png" alt="." width="absolute" height="100px"/>
            <div style={{width:'1366px', height: '80px', backgroundColor: '#061D47', color: 'white'}}>
                <center><h3 style={{marginTop: '25px'}}>Sistem Informasi Perpustakaan IT Del (SISPER)</h3></center>
            </div>
            <div className="col-md-6" style={{backgroundImage: `url(${backgroundForm})`, marginTop: '50px', marginBottom: '50px'}}>
            <h4 style={{color: 'white', textAlign: 'center', marginTop: '10px'}}>Login</h4>
                <form onSubmit={handleuseStateSubmit}>
                    <div className="form-group">
                        <label for="exampleInputEmail1" style={{color: 'white', marginTop:'10px'}}>Username </label>
                         <input type="text" className="form-control" onChange={handleuseStateChangeUsername} required/>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword1" style={{color: 'white'}}>Password</label>
                        <input type="password" className="form-control" onChange={handleuseStateChangePassword}/>
                    </div>
                    <div class="form-check" style={{marginBottom:'30px'}}>
                        <button type="submit" class="btn btn-primary btn-md">Login</button>   
                    </div>
                </form>
            </div>
      </div>
  </div> 
  <Footer />
  </>
  );
}