import React, {useState} from 'react';
import backgroundForm from "../USERS/Components/Images/BgGantiSandi.png";
import backgroundGantiSandi from "../USERS/Components/Images/Bg-GantiSandi.png"
import './style.css'
import Axios from 'axios';
import Footer from '../ADMIN/Components/Footer/Footer';
import Swal from 'sweetalert2';

export default function GantiSandi() {
    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')

    const handleuseStateChangePassword = (event) => (
        setOldPassword(event.target.value)
    );
    const handleuseStateChangeNewPassword = (event) => (
        setNewPassword(event.target.value)
    );
    const handleuseStateChangeConfirmNewPassword = (event) => (
        setConfirmPassword(event.target.value)
    );
    const handleuseStateSubmit = (event) => {
        event.preventDefault();
        if(newPassword === confirmPassword){
            Axios.put("http://localhost:8082/api/v1/user/gantiPassword/" + localStorage.getItem("username"), {
                password : oldPassword,
                newPassword : newPassword
            },{
                headers : {
                    Authorization: 'Bearer ' + localStorage.getItem("token")
                }
            }).then(response => {
                if(response.data.success){
                    Swal.fire({
                        title: "Berhasil",
                        text: "Kata sandi telah di ubah",
                        type: "success"
                      });
                      window.location.reload();
                }else{
                    Swal.fire({
                        title: "Gagal",
                        text: response.data.message,
                        type: "failed"
                      });
                }
            })
        }else{
            Swal.fire({
                title: "Gagal",
                text: "Kata sandi baru tidak sama!",
                type: "failed"
              });
        }
        
    };
  return (
    <>
    <div className="main" style={{backgroundImage: `url(${backgroundGantiSandi})`}}>
      <div className="row justify-content-center">
            <div className="col-md-6" style={{backgroundImage: `url(${backgroundForm})`, marginTop: '50px', marginBottom: '50px'}}>
            <h4 style={{color: 'white', textAlign: 'center', marginTop: '10px'}}>Ganti Password</h4>
                <form onSubmit={handleuseStateSubmit}>
                    <div className="form-group">
                        <label for="exampleInputPassword2" style={{color: 'white', marginTop:'10px'}}>Password saat ini</label>
                         <input type="password" className="form-control" onChange={handleuseStateChangePassword} required/>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword3" style={{color: 'white'}}>Password yang baru</label>
                        <input type="password" className="form-control" onChange={handleuseStateChangeNewPassword}/>
                    </div>
                    <div className="form-group">
                        <label for="exampleInputPassword3" style={{color: 'white'}}>Konfirmasi password yang baru</label>
                        <input type="password" className="form-control" onChange={handleuseStateChangeConfirmNewPassword}/>
                    </div>
                    <div class="form-submit" style={{marginBottom:'30px'}}>
                        <button type="submit" class="btn btn-primary btn-md" style={{backgroundColor: "#229644"}}>Simpan perubahan</button>   
                    </div>
                </form>
            </div>
      </div>
  </div> 
  <Footer />
  </>
  );
}