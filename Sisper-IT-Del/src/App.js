import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './ADMIN/Home';
import BerandaUser from './USERS/BerandaUser';
import Buku from './ADMIN/BahanPustaka/Buku';
import CD from './ADMIN/BahanPustaka/CD';
import TA from './ADMIN/BahanPustaka/TA';
import BahanPustakaBaru from './ADMIN/KelolaData/BahanPustakaBaru';
import DataPengguna from './ADMIN/KelolaData/DataPengguna';
import Laporan from './ADMIN/KelolaData/Laporan';
import PanduanPengguna from './ADMIN/TentangPerpus/PanduanPengguna';
import PanduanPesanPinjam from './ADMIN/TentangPerpus/PanduanPesanPinjam';
import PeraturanPerpustakaan from './ADMIN/TentangPerpus/PeraturanPerpustakaan';
import Pencarian from './ADMIN/Components/pages/Pencarian';
import Peminjaman from './ADMIN/Pemberitahuan/Peminjaman';
import Pemesanan from './ADMIN/Pemberitahuan/Pemesanan';
import SejarahPeminjaman from './ADMIN/Pemberitahuan/SejarahPeminjaman';
import Anggota from './ADMIN/Pemberitahuan/Anggota';
import RatingBuku from './ADMIN/Pemberitahuan/RatingBuku';
import BukuUser from './USERS/BahanPustaka/BukuUser';
import CDUser from './USERS/BahanPustaka/CDUser';
import TAUser from './USERS/BahanPustaka/TAUser';
import BahanPustakaBaruUser from './USERS/Pemberitahuan/BahanPustakaBaru';
import Artikel from './USERS/Pemberitahuan/Artikel';
import RatingBukuUser from './USERS/Pemberitahuan/RatingBuku';
import PanduanPenggunaUser from './USERS/TentangPerpus/PanduanPengguna';
import PanduanPesanPinjamUser from './USERS/TentangPerpus/PanduanPesanPinjam';
import PeraturanPerpustakaanUser from './USERS/TentangPerpus/PeraturanPerpustakaan';
import GantiPassword from './USERS/InformasiAkun/GantiPassword';
import InformasiBahanPustaka from './USERS/InformasiAkun/InformasiBahanPustaka';
import NotifikasiAdmin from './USERS/InformasiAkun/NotifikasiAdmin';
import PencarianUser from './USERS/Components/pages/Pencarian';
import DetailBuku from './ADMIN/BahanPustaka/Detail_Buku';
import DetailCD from './USERS/BahanPustaka/DetailCD';
import LoginForm from './forms/LoginForm';
import GantiSandi from './forms/GantiSandi';
import SejarahPerpus from './ADMIN/TentangPerpus/SejarahPerpus';
import SejarahPerpusUser from './USERS/TentangPerpus/SejarahPerpus';
import Galeri from './ADMIN/TentangPerpus/Galeri';
import GaleriUser from './USERS/TentangPerpus/Galeri';
import Request from './USERS/BahanPustaka/Request';
import DetailTA from './ADMIN/BahanPustaka/DetailTA';
import PengadaanBuku from './ADMIN/Pemberitahuan/PengadaanBuku';
import BuatArtikel from './ADMIN/KelolaData/BuatArtikel';
import TambahPeminjaman from './ADMIN/Pemberitahuan/TambahPeminjaman';

function App() {

  return (
    <Router>
      <Switch>
        <Route 
          path='/beranda' 
          component={Home} 
        />
        <Route 
          path='/login' 
          component={LoginForm} 
        />
        <Route 
          path='/ganti-sandi' 
          component={GantiSandi} 
        />
        <Route 
          path='/beranda-user' 
          component={BerandaUser} 
        />
        <Route 
          path='/pencarian' 
          component={Pencarian} 
        />
        <Route 
          path='/buku' 
          component={Buku} 
        />
        <Route 
          path='/cd' 
          component={CD} 
        />
        <Route 
          path='/ta' 
          component={TA} 
        />
        <Route 
          path='/detail-buku/:id' 
          component={DetailBuku} 
        />
        <Route
          path='/bahan-pustaka-baru' 
          component={BahanPustakaBaru} 
        />
        <Route 
          path='/data-pengguna' 
          component={DataPengguna} 
        />
        <Route 
          path='/laporan' 
          component={Laporan} 
        />
        <Route 
          path='/panduan-pengguna' 
          component={PanduanPengguna} 
        />
        <Route 
          path='/panduan-pesan-pinjam' 
          component={PanduanPesanPinjam} 
        />
        <Route 
          path='/peraturan-perpustakaan' 
          component={PeraturanPerpustakaan} 
        />
        <Route 
          path='/sejarah-perpustakaan' 
          component={SejarahPerpus} 
        />
        <Route 
          path='/sejarah-perpustakaan-user' 
          component={SejarahPerpusUser} 
        />
        <Route 
          path='/galeri' 
          component={Galeri} 
        />
        <Route 
          path='/galeri-user' 
          component={GaleriUser} 
        />
        <Route 
          path='/peminjaman' 
          component={Peminjaman} 
        />
        <Route 
          path='/pemesanan' 
          component={Pemesanan} 
        />
        <Route 
          path='/pengadaan' 
          component={PengadaanBuku} 
        />
        <Route 
          path='/sejarah-peminjaman' 
          component={SejarahPeminjaman} 
        />
        <Route 
          path='/anggota' 
          component={Anggota} 
        />
        <Route 
          path='/rating-buku' 
          component={RatingBuku} 
        />
        <Route 
          path='/pencarian' 
          component={PencarianUser} 
        />
        <Route 
          path='/buku-user' 
          component={BukuUser} 
        />
        <Route 
          path='/cd-user' 
          component={CDUser} 
        />
        <Route 
          path='/ta-user' 
          component={TAUser} 
        />
        <Route 
          path='/bahan-pustaka-baru-user' 
          component={BahanPustakaBaruUser} 
        />
        <Route 
          path='/artikel-user' 
          component={Artikel} 
        />
        <Route 
          path='/rating-buku-user' 
          component={RatingBukuUser} 
        />
        <Route 
          path='/panduan-pengguna-user' 
          component={PanduanPenggunaUser} 
        />
        <Route 
          path='/panduan-pesan-pinjam-user' 
          component={PanduanPesanPinjamUser} 
        />
        <Route 
          path='/peraturan-perpustakaan-user' 
          component={PeraturanPerpustakaanUser} 
        />
        <Route 
          path='/ganti-password' 
          component={GantiPassword} 
        />
        <Route 
          path='/informasi-bahan-pustaka' 
          component={InformasiBahanPustaka} 
        />
        <Route 
          path='/notifikasi-admin' 
          component={NotifikasiAdmin} 
        />
        <Route 
          path='/request' 
          component={Request} 
        />
        <Route 
          path='/detail-cd' 
          component={DetailCD} 
        />
        <Route 
          path='/detail-ta' 
          component={DetailTA} 
        />
        <Route 
          path='/artikelBaru' 
          component={BuatArtikel} 
        />
        <Route
          path='/tambah-peminjaman'
          component={TambahPeminjaman}
        />
      </Switch>
    </Router>
  )
}

export default App;