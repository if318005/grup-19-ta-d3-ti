<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('coba', 'Api\KoleksiBahanPustakaController@coba');
Route::delete('hapusBahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@deleteBahanPustaka');
Route::get('listBahanPustaka', 'Api\KoleksiBahanPustakaController@getListBahanPustaka');
Route::get('bahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@getBahanPustaka');
Route::post('tambahBahanPustaka', 'Api\KoleksiBahanPustakaController@tambahBahanPustaka');
Route::put('editBahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@editBahanPustaka');
Route::put('editCdDvd/{id}', 'Api\KoleksiBahanPustakaController@editCdDvd');
Route::put('editBuku/{id}', 'Api\KoleksiBahanPustakaController@editBuku');
Route::put('pinjamBahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@pinjamBahanPustaka');
Route::put('pengembalianBahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@pengembalianBahanPustaka');
Route::get('listBuku', 'Api\KoleksiBahanPustakaController@getListBuku');
Route::get('listTaKpPa', 'Api\KoleksiBahanPustakaController@getListTaKpPa');
Route::get('listCdDvd', 'Api\KoleksiBahanPustakaController@getListCdDvd');
Route::post('tambahBuku', 'Api\KoleksiBahanPustakaController@tambahBuku');
Route::post('tambahTaKpPa', 'Api\KoleksiBahanPustakaController@tambahTaKpPa');
Route::post('tambahCdDvd', 'Api\KoleksiBahanPustakaController@tambahCdDvd');
Route::get('getBukuBaru', 'Api\KoleksiBahanPustakaController@getBukuBaru');
Route::get('getCdBaru', 'Api\KoleksiBahanPustakaController@getCdBaru');
Route::get('getTaBaru', 'Api\KoleksiBahanPustakaController@getTaBaru');
Route::get('getId/{tahun}', 'Api\IdPustakaController@getId');
Route::get('bahanPustaka/{id}', 'Api\KoleksiBahanPustakaController@getBahanPustaka');
Route::get('getBahanPustakaIdCreate/{id}', 'Api\KoleksiBahanPustakaController@getBahanPustakaIdCreate');
Route::get('tambahIdBahanPustaka', 'Api\IdPustakaController@tambahKoleksiBahanPustaka');
Route::post('tambahArtikel', 'Api/ArtikelController@tambahArtikel');
Route::put('editArtikel/{id}', 'Api\ArtikelController@editArtikel');
Route::delete('deleteArtikel/{id}', 'Api\ArtikelController@deleteArtikel');
