<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBahanPustakaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_pustaka', function (Blueprint $table) {
            $table->id();
            $table->string("jenis_pustaka");
            $table->string("klasifikasi");
            $table->string("status_pustaka");
            $table->string("lokasi");
            $table->string("subjek");
            $table->string("deskripsi");
            $table->string("judul_pustaka");
            $table->integer("tersedia")->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_pustaka');
    }
}
