<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableBahanPustaka extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bahan_pustaka', function (Blueprint $table) {
            $table->enum('kategori', ['ta/kp/pa', 'buku', 'cd/dvd']);
            $table->string('jenis');
            $table->string('pembimbing')->nullable();
            $table->year('tahun');
            $table->string('pengarang')->nullable();
            $table->string('bahasa')->nullable();
            $table->string('penerbit')->nullable();
            $table->string('prodi')->nullable();;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
