<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'artikel';
     protected $fillable = [
        "judul",
        "status_pustaka",
}
