<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdPustaka extends Model
{
    use HasFactory;
    protected $table = 'id_pustaka';
    public $timestamps = false;
    protected $fillable = [
        "tahun",
        "jumlah"
    ];
}
