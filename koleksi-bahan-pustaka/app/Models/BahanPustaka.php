<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BahanPustaka extends Model
{
    use HasFactory;
    protected $table = 'bahan_pustaka';
    protected $fillable = [
        "klasifikasi",
        "status_pustaka",
        "lokasi",
        "subjek",
        "edisi",
        "isbn",
        "deskripsi",
        "judul_pustaka",
        "tersedia",
        "jenis",
        "kategori",
        "pembimbing",
        "tahun",
        "pengarang",
        "bahasa",
        "penerbit",
        "penguji",
        "penguji2",
        "pembimbing2",
        "sumber",
        "copyOriginal",
        "gambar",
        "prodi",
        "id_create",
    ];
}
