<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BahanPustaka;
use Illuminate\Support\Facades\Validator;

class KoleksiBahanPustakaController extends Controller
{
    public function getListBahanPustaka(){
        $data = BahanPustaka::all();
        return json_encode($data);
    }

    public function getListBuku(){
        $data = BahanPustaka::where('kategori', '=', 'buku')->get();
        return json_encode($data);
    }

    public function getListTaKpPa(){
        $data = BahanPustaka::where('kategori', '=', 'ta/kp/pa')->get();
        return json_encode($data);
    }

    public function getListCdDvd(){
        $data = BahanPustaka::where('kategori', '=', 'cd/dvd')->get();
        return json_encode($data);
    }

    public function getBukuBaru(){
        $data = BahanPustaka::where('kategori', '=', 'buku')->latest('created_at')->take(3)->get();
        return json_encode($data);
    }

    public function getCdBaru(){
        $data = BahanPustaka::where('kategori', '=', 'cd/dvd')->latest('created_at')->take(3)->get();
        return json_encode($data);
    }

    public function getTaBaru(){
        $data = BahanPustaka::where('kategori', '=', 'ta/kp/pa')->latest('created_at')->take(3)->get();
        return json_encode($data);
    }

    public function tambahBuku(Request $request){
        $post = BahanPustaka::create([
            "pengarang" => $request->input("pengarang"),
            "bahasa" => $request->input("bahasa"),
            "klasifikasi" => $request->input("klasifikasi"),
            "penerbit" => $request->input("penerbit"),
            "lokasi" => $request->input("lokasi"),
            "tahun" => $request->input("tahun"),
            "subjek" => $request->input("subjek"),
            "edisi" => $request->input("edisi"),
            "isbn" => $request->input("isbn"),
            "judul_pustaka" => $request->input("judul_pustaka"),
            "jenis" => $request->input("jenis"),
            "deskripsi" => $request->input("deskripsi"),
            "status_pustaka" => "tersedia",
            "tersedia" => $request->input("tersedia"),
            "kategori" => "buku",
            "copyOriginal" => $request->input("copyOriginal"),
            "gambar" => $request->input("gambar"),
            "id_create" => $request->input("id")
        ]);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
        // return $request->input("gambar");
    }

    public function tambahTaKpPa(Request $request){
        $post = BahanPustaka::create([
            "judul_pustaka" => $request->input("judul_pustaka"),
            "jenis" => $request->input("jenis"),
            "klasifikasi" => $request->input("klasifikasi"),
            "status_pustaka" => "tersedia",
            "subjek" => $request->input("subjek"),
            "pengarang" => $request->input("pengarang"),
            "prodi" => $request->input("prodi"),
            "tersedia" => 1,
            "pembimbing" => $request->input("pembimbing"),
            "pembimbing2" => $request->input("pembimbing2"),
            "tahun" => $request->input("tahun"),
            "lokasi" => $request->input("lokasi"),
            "deskripsi" => $request->input("deskripsi"),
            "penguji" => $request->input("penguji"),
            "penguji2" => $request->input("penguji2"),
            "tingkat" => $request->input("tingkat"),
            "kategori" => "ta/kp/pa",
            "id_create" => $request->input("id")
        ]);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function tambahCdDvd(Request $request){
        $post = BahanPustaka::create([
            "judul_pustaka" => $request->input("judul_pustaka"),
            "jenis" => $request->input("jenis"),
            "status_pustaka" => "tersedia",
            "klasifikasi" => $request->input("klasifikasi"),
            "subjek" => $request->input("subjek"),
            "isbn" => $request->input("isbn"),
            "tersedia" => 1,
            "tahun" => $request->input("tahun"),
            "lokasi" => $request->input("lokasi"),
            "sumber" => $request->input("sumber"),
            "copyOriginal" => $request->input("copyOriginal"),
            "prodi" => $request->input("prodi"),
            "deskripsi" => $request->input("deskripsi"),
            "kategori" => "cd/dvd",
            "gambar" => $request->input("gambar"),
            "id_create" => $request->input("id")
        ]);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function editCdDvd(Request $request, int $id){
        $post = BahanPustaka::find($id)->update([
            "judul_pustaka" => $request->input("judul_pustaka"),
            "jenis" => $request->input("jenis"),
            "klasifikasi" => $request->input("klasifikasi"),
            "subjek" => $request->input("subjek"),
            "tahun" => $request->input("tahun"),
            "lokasi" => $request->input("lokasi"),
            "isbn" => $request->input("isbn"),
            "copyOriginal" => $request->input("copyOriginal"),
            "deskripsi" => $request->input("deskripsi"),
            "prodi" => $request->input("prodi"),
            "sumber" => $request->input("sumber"),
            "gambar" => $request->input("gambar")
        ]);

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 400);
        }

    }

    public function editBuku(Request $request, int $id){
        $post = BahanPustaka::find($id)->update([
            "judul_pustaka" => $request->input("judul_pustaka"),
            "pengarang" => $request->input("pengarang"),
            "bahasa" => $request->input("bahasa"),
            "klasifikasi" => $request->input("klasifikasi"),
            "penerbit" => $request->input("penerbit"),
            "lokasi" => $request->input("lokasi"),
            "tahun" => $request->input("tahun"),
            "subjek" => $request->input("subjek"),
            "edisi" => $request->input("edisi"),
            "isbn" => $request->input("isbn"),
            "jenis" => $request->input("jenis"),
            "copyOriginal" => $request->input("copyOriginal"),
            "deskripsi" => $request->input("deskripsi"),
            "tersedia" => $request->input("tersedia")
        ]);
        // $post = false;

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 200);
        }

    }

    public function getBahanPustaka(int $id){
        $data = BahanPustaka::find($id);
        return json_encode($data);
    }

    public function getBahanPustakaIdCreate($id){
        $data = BahanPustaka::where('id_create', $id)->first();
        return json_encode($data);
    }

    public function coba(Request $request){
        return response()->json([
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
                'var1' => $request->input("var1"),
                'a' => $request->input("a")
            ], 200);
    }

    public function editBahanPustaka(Request $request, int $id){
        $post = BahanPustaka::find($id)->update([
            "jenis_pustaka" => $request->input("jenis_pustaka"),
            "klasifikasi" => $request->input("klasifikasi"),
            "status_pustaka" => $request->input("status_pustaka"),
            "lokasi" => $request->input("lokasi"),
            "subjek" => $request->input("subjek"),
            "deskripsi" => $request->input("deskripsi"),
            "judul_pustaka" => $request->input("judul_pustaka"),
            "tersedia" => $request->input("tersedia")
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 400);
        }
    }

    public function tambahBahanPustaka(Request $request){
        $post = BahanPustaka::create([
            "jenis" => $request->input("jenis_pustaka"),
            "klasifikasi" => $request->input("klasifikasi"),
            "status_pustaka" => $request->input("status_pustaka"),
            "lokasi" => $request->input("lokasi"),
            "subjek" => $request->input("subjek"),
            "deskripsi" => $request->input("deskripsi"),
            "judul_pustaka" => $request->input("judul_pustaka"),
            "tersedia" => $request->input("tersedia")
        ]);
        
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
    }

    public function deleteBahanPustaka($id){
        $post = BahanPustaka::find($id)->delete();
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di hapus!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data berhasil di hapus!',
            ], 400);
        }
    }

    public function pinjamBahanPustaka($id){
        // $bahanPustaka = BahanPustaka::where('id', $id)->first();
        // $stok = $bahanPustaka->tersedia;
        // $post = true;
        $post = BahanPustaka::find($id)->decrement('tersedia');

        if ($post) {
            return response()->json([
                // 'asdf' => $bahanPustaka->tersedia,
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 200);
        }
    }

    public function pengembalianBahanPustaka($id){
        // $bahanPustaka = BahanPustaka::where('id', $id)->first();
        // $stok = $bahanPustaka->tersedia;
        // $post = true;
        $post = BahanPustaka::where("id_create", $id)->get()->first()->increment('tersedia');

        if ($post) {
            return response()->json([
                // 'asdf' => $bahanPustaka->tersedia,
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 200);
        }
    }
}
