<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IdPustaka;

class IdPustakaController extends Controller
{
    function getId($tahun){
    	if(IdPustaka::where('tahun', $tahun)->exists()){
    		return response()->json([
    			'id' => IdPustaka::where('tahun', $tahun)->first()->jumlah + 1,
                'success' => true,
            ], 200);
    	}else{
    		$post = IdPustaka::create([
    			"tahun" => $tahun
    		]);
    		if ($post) {
    			return response()->json([
	    			'id' => 1,
	                'success' => true,
	            ], 200);
    		}
    	}
    }

    function tambahKoleksiBahanPustaka(){
        $post = IdPustaka::where('tahun', date("y"))->first()->increment('jumlah');
        if ($post) {
            return response()->json([
                'success' => true 
            ], 200);
        }else {
            return response()->json([
                'success' => true
            ], 200);
        }
    }
}
