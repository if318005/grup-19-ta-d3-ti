<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Artikel;
use Illuminate\Http\Request;

class ArtikelController extends Controller
{
    //
    public function getListArtikel(){
        $data = Artikel::->all();
        return json_encode($data);
    }

    public function tambahArtikel(Request $request){
        $post = Artikel::create([
            "judul" => $request->input("judul"),
            "deskripsi" => $request->input("deskripsi")
        ]);
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Post Berhasil Disimpan!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Post Gagal Disimpan!',
            ], 400);
        }
        // return $request->input("gambar");
    }

    public function editArtikel(Request $request, int $id){
        $post = Artikel::find($id)->update([
            "judul" => $request->input("judul"),
            "deskripsi" => $request->input("deskripsi")
        ]);
        // $post = false;

        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data Berhasil di ubah!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data Gagal di ubah!',
            ], 200);
        }

    }

    public function deleteArtikel($id){
        $post = Artikel::find($id)->delete();
        if ($post) {
            return response()->json([
                'success' => true,
                'message' => 'Data berhasil di hapus!',
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data berhasil di hapus!',
            ], 400);
        }
    }
}
