package com.sisper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SisperApplication {

	public static void main(String[] args) {
		SpringApplication.run(SisperApplication.class, args);
	}

}
