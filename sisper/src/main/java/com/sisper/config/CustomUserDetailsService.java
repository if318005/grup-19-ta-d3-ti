package com.sisper.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.json.*;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<SimpleGrantedAuthority> roles = null;
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username;
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
//		System.out.println(result.length());
		if(result.length() > 2) {
			try {
				JSONObject coba;
				coba = new JSONObject(result);
//				System.out.println(coba.getString("pass"));
				if(coba.getInt("id_role") == 1) {
					roles=Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
				}else {
					roles=Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
				}
				return new User(coba.getString("username"), coba.getString("pass"), roles);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
//		if(username.equals("if318005"))
//		{
//			roles=Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
////			$2a$10$4SrbQvsbI.hJPJwmn9/6AetODe4kaGURvwwFH.Zq0CGOR2d0CH6ky
//			return new User("if318005", "$2y$10$dBg0RyV88xR.Sp0kdESKNu1iQs0uDws7o2o9qxLcT95IQdLXpuDj2", roles);
//			
//		}
		throw new UsernameNotFoundException("User not found with the name "+ username);
	}

}
