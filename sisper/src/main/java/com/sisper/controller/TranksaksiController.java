package com.sisper.controller;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1/transaksiBuku")
public class TranksaksiController {
	@GetMapping("getListPeminjaman")
	public Object getPengadaanBuku() {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/listPeminjaman";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getListPemesanan")
	public Object getPemesanan() {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/listPemesanan";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("setujuPemesanan/{id}")
	public Object terimaPemesanan(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/setujuPemesanan/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@PostMapping("laporanPemesanan")
	public ResponseEntity<String> laporanPemesanan(@RequestBody String data){
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/laporanPemesanan";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@PostMapping("laporanPeminjaman")
	public ResponseEntity<String> laporanPeminjaman(@RequestBody String data){
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/laporanPeminjaman";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		System.out.println(data);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@PostMapping("laporanDenda")
	public ResponseEntity<String> laporanDenda(@RequestBody String data){
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/laporanDenda";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		System.out.println(data);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@GetMapping("bahanPustakaHarusAmbil/{username}")
	public Object bahanPustakaHarusAmbil(@PathVariable String username){
		RestTemplate restTemplate = new RestTemplate();
		JSONObject user = new JSONObject(restTemplate.getForObject("http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username, String.class));
		String nomorInduk = user.getString("nomorInduk");
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/getBahanPustakaYangHarusDiambil/" + nomorInduk;
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("bahanPustakaDiPesan/{username}")
	public Object bahanPustakaDiPesan(@PathVariable String username){
		RestTemplate restTemplate = new RestTemplate();
		JSONObject user = new JSONObject(restTemplate.getForObject("http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username, String.class));
		String nomorInduk = user.getString("nomorInduk");
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/getBahanPustakaYangDiPesan/" + nomorInduk;
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("bahanPustakaDiPinjam/{username}")
	public Object bahanPustakaDiPinjam(@PathVariable String username){
		RestTemplate restTemplate = new RestTemplate();
		JSONObject user = new JSONObject(restTemplate.getForObject("http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username, String.class));
		String nomorInduk = user.getString("nomorInduk");
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/getBahanPustakaYangDiPinjam/" + nomorInduk;
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getNotifikasi/{username}")
	public Object getNotifikasi(@PathVariable String username){
		RestTemplate restTemplate = new RestTemplate();
		JSONObject user = new JSONObject(restTemplate.getForObject("http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username, String.class));
		String nomorInduk = user.getString("nomorInduk");
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/getNotifikasi/" + nomorInduk;
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	@GetMapping("tolakPemesanan/{id}")
	public Object tolakPemesanan(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/tolakPemesanan/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@PutMapping(value = "pengembalianBuku/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Object pengembalianBuku(@RequestBody String data ,@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/pengembalianBuku/" + id;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		JSONObject peminjaman = new JSONObject(restTemplate.exchange("http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/getTransaksiById/" + id, HttpMethod.GET, entity, String.class).getBody().toString());
		String idCreate = peminjaman.getString("id_buku");
		restTemplate.exchange("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/pengembalianBahanPustaka/" + idCreate, HttpMethod.PUT, entity, String.class);
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@GetMapping("perpanjanganPeminjaman/{id}")
	public Object perpanjanganPeminjaman(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/perpanjanganPeminjaman/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("ambilBuku/{id}")
	public Object ambilBuku(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/ambilBuku/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}

	@PostMapping(value = "tambahPeminjaman", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addBuku(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/tambahPeminjaman";
		RestTemplate restTemplate = new RestTemplate();
//
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);
//		
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
//		return response;
		JSONObject coba = new JSONObject(data);
		String username = coba.get("username").toString();
		JSONObject user = new JSONObject(restTemplate.exchange("http://localhost:8081/TA/Newfolder/service-user/public/api/getUser/" + username, HttpMethod.GET, entity, String.class).getBody().toString());
		String nama = user.getString("name");
		String id = user.getString("nomorInduk");
//		System.out.println(id);
		coba.put("no_identitas", id);
		coba.put("nama", nama);
		coba.put("email", user.getString("email"));
		coba.put("prodi", user.get("Prodi").toString());
		int role = user.getInt("id_role");
		if(role == 1) {
			coba.put("jabatan", "Admin");
		}else if(role == 2) {
			coba.put("jabatan", "Kaprodi");
		}else if(role == 3) {
			coba.put("jabatan", "Dosen/Staff");
		}else {
			coba.put("jabatan", "Mahasiswa");
		}
		System.out.println(user.get("Prodi").toString());
		restTemplate.exchange("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/pinjamBahanPustaka/" + coba.getInt("id_buku"), HttpMethod.PUT, entity, String.class);
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<String>(coba.toString(), headers), String.class).getBody(), HttpStatus.OK);
	}
	
	@PostMapping(value = "tambahPeminjamanAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addPeminjamanAdmin(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/transaksi_buku/public/api/tambahPeminjamanAdmin";
		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);

		JSONObject coba = new JSONObject(data);
		String nomorInduk = coba.get("nomorInduk").toString();
		JSONObject user = new JSONObject(restTemplate.exchange("http://localhost:8081/TA/Newfolder/service-user/public/api/getUserByNomorInduk/" + nomorInduk, HttpMethod.GET, entity, String.class).getBody().toString());
		String nama = user.getString("name");
		String id = user.getString("nomorInduk");
		coba.put("no_identitas", id);
		coba.put("nama", nama);
		coba.put("email", user.getString("email"));
		coba.put("prodi", user.get("Prodi").toString());
		int role = user.getInt("id_role");
		if(role == 1) {
			coba.put("jabatan", "Admin");
		}else if(role == 2) {
			coba.put("jabatan", "Kaprodi");
		}else if(role == 3) {
			coba.put("jabatan", "Dosen/Staff");
		}else {
			coba.put("jabatan", "Mahasiswa");
		}
//		System.out.println(user.get("Prodi").toString());
		restTemplate.exchange("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/pinjamBahanPustaka/" + coba.getInt("id_buku1"), HttpMethod.PUT, entity, String.class);
		if(coba.getString("idBahanPustaka2") != "") {
			restTemplate.exchange("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/pinjamBahanPustaka/" + coba.getInt("id_buku2"), HttpMethod.PUT, entity, String.class);
		}
		if(coba.getString("idBahanPustaka3") != "") {
			restTemplate.exchange("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/pinjamBahanPustaka/" + coba.getInt("id_buku3"), HttpMethod.PUT, entity, String.class);
		}
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, new HttpEntity<String>(coba.toString(), headers), String.class).getBody(), HttpStatus.OK);
//		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
}
