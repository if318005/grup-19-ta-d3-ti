package com.sisper.controller;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
//@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/v1/user")
public class UserController {
	@PostMapping(value = "login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> login(@RequestBody String user)
	{
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/login";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(user, headers);
		
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		return response;
	}
	
	@GetMapping(value="getAllUser")
	public Object getAllUser() {
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/getAllUser";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping(value="hapusUser/{id}")
	public Object hapusUser(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/hapusUser/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@PostMapping(value= "register", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> regiser(@RequestBody String data)
	{
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/register";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	
	@PutMapping(value = "gantiPassword/{username}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editPassword(@RequestBody String data, @PathVariable String username){
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/gantiPassword/" + username;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@PutMapping(value = "editUser/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editUser(@RequestBody String data, @PathVariable Integer id){
		final String uri = "http://localhost:8081/TA/Newfolder/service-user/public/api/editUser/" + id;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody(), HttpStatus.OK);
	}
}
