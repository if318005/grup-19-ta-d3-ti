package com.sisper.controller;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
@RequestMapping("/api/v1/bahanPustaka")
public class BahanPustakaController {
	@GetMapping
	public String coba(){
		return "sdf";
	}
	
	@GetMapping("getBahanPustaka")
	public Object getBahanPustaka() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/listBahanPustaka";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getBuku")
	public Object getBuku() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/listBuku";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getTaKpPa")
	public Object getTaKpPa() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/listTaKpPa";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getCdDvd")
	public Object getCdDvd() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/listCdDvd";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getBukuBaru")
	public Object getBukuBaru() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/getBukuBaru";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getCdBaru")
	public Object getCdBaru() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/getCdBaru";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getTaBaru")
	public Object getTaBaru() {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/getTaBaru";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getBahanPustaka/{id}")
	public Object getBahanPustakaById(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/bahanPustaka/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getId/{tahun}")
	public Object getJumlahIdByTahun(@PathVariable Integer tahun) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/getId/" + tahun;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@PostMapping(value = "addBuku", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addBuku(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahBuku";
		RestTemplate restTemplate = new RestTemplate();
//		JsonObject coba2 = new Gson().fromJson(data, JsonObject.class);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);
		
		restTemplate.getForObject("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahIdBahanPustaka", String.class);
		
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);

		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@PostMapping(value = "addTaKpPa", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addTaKpPa(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahTaKpPa";
		RestTemplate restTemplate = new RestTemplate();

		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);
		
		restTemplate.getForObject("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahIdBahanPustaka", String.class);
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody() ,HttpStatus.OK);
	}
	
	@PostMapping(value = "addCdDvd", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addCdDvd(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahCdDvd";
		RestTemplate restTemplate = new RestTemplate();

		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);
		restTemplate.getForObject("http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/tambahIdBahanPustaka", String.class);
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.POST, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@DeleteMapping(value="deleteBahanPustaka/{id}")
	public ResponseEntity<String> deleteBahanPustaka(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/hapusBahanPustaka/" + id;
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
//		restTemplate.delete(uri);
		
		return new ResponseEntity<>(restTemplate.exchange(uri, HttpMethod.DELETE, entity, String.class).getBody(), HttpStatus.OK);
	}
	
//	@PutMapping(value = "editBahanPustaka/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
//	public ResponseEntity<String> editBahanPustaka(@RequestBody String data, @PathVariable int id){
//		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/editBahanPustaka/" + id;
//		RestTemplate restTemplate = new RestTemplate();
//		
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
//		
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
//		return response;
//	}
	

	@PutMapping(value = "editCdDvd/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editCdDvd(@RequestBody String data, @PathVariable int id){
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/editCdDvd/" + id;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody(), HttpStatus.OK);
	}
	
	@PutMapping(value = "editBuku/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> editBuku(@RequestBody String data, @PathVariable int id){
		final String uri = "http://localhost:8081/TA/Newfolder/koleksi-bahan-pustaka/public/api/editBuku/" + id;
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data, headers);
		
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
//		return response;
		return new ResponseEntity<String>(restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody(), HttpStatus.OK);
	}

}
