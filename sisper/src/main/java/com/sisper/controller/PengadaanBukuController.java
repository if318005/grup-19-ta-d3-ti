package com.sisper.controller;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/api/v1/pengadaanBuku")
public class PengadaanBukuController {
	@GetMapping("getPengadaanBuku")
	public Object getPengadaanBuku() {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/listPengajuan";
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@GetMapping("getPengadaan/{id}")
	public Object getPengadaanById(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/getPengajuan/" + id;
		RestTemplate restTemplate = new RestTemplate();
		Object result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	
	@PostMapping(value = "addPengajuan", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> addPengadaan(@RequestBody String data) {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/tambahPengajuan";
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(data.toString(), headers);
		
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
		return response;
	}
	
	@GetMapping("setujuPengadaan/{id}")
	public Object setujuPengadaan(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/setujuPengajuan/" + id;
		RestTemplate restTemplate = new RestTemplate();
		JSONObject coba = new JSONObject();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(coba.toString(), headers);
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
		return new ResponseEntity<String> (restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody().toString(), HttpStatus.OK);
	}
	
	@GetMapping("tolakPengadaan/{id}")
	public Object tolakPengadaan(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/tolakPengajuan/" + id;
		RestTemplate restTemplate = new RestTemplate();
		JSONObject coba = new JSONObject();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(coba.toString(), headers);
//		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
		return new ResponseEntity<String> (restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class).getBody().toString(), HttpStatus.OK);
	}
	
	@PutMapping("pengadaanBuku/{id}")
	public Object pengdaanBuku(@PathVariable Integer id) {
		final String uri = "http://localhost:8081/TA/Newfolder/pengadaan_buku/public/api/pengadaanBuku/" + id;
		RestTemplate restTemplate = new RestTemplate();
		JSONObject coba = new JSONObject();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(coba.toString(), headers);
		ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.PUT, entity, String.class);
		return response;
	}
}
