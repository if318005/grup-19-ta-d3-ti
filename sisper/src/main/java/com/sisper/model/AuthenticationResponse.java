package com.sisper.model;

public class AuthenticationResponse {

	private String token;
	private String role;
	private String username;
	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public AuthenticationResponse()
	{
		
	}
	
	public String getRole() {
		return role;
	}

	public void setNama(String role) {
		this.role = role;
	}


	public AuthenticationResponse(String token, String role, String username) {
		super();
		this.role = role;
		this.token = token;
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
